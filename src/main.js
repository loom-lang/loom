/**
 * Module containing the main entry functions for using Loom.
 */

// Dependencies
let ast = require('./parser/nodes');
let compileLoom = require('./compiler/compiler');
let fs = require('fs');
let Module = require('module');
let parser = require('./parser/parser');
let path = require('path');
let {transformFromAst} = require('babel-core');
let {extend, prettifyError} = require('./utils');
let {name: pkgName, version: loomVersion} = require('../package.json');
/**
 * Object rest/spread Babel plugin.
 * NOTE: This is only needed while this feature is a stage 3 proposal of ES;
 *       once it becomes stage 4 it will be included by the `babel-env` preset
 */
let objectRestSpread = require('babel-plugin-transform-object-rest-spread');

/**
 * Default options used by the `compile` function.
 */
const COMPILE_OPTIONS = {modulesPath: `${pkgName}/src/modules`,
  produceValue: false, jsAst: false, js: true, filename: '.', presets: [],
  plugins: []};

/**
 * Parses a string in Loom into its AST representation.
 * @param {string} input Input code
 * @param {Object} options Parsing options:
 *   - {string} filename: name of file being parsed (or something such as
 *       `'[repl]'`)
 * @returns {ASTNode} AST representation of provided input
 */
function parse(input, options = {}) {
  try {
    return parser.parse(input);
  } catch(e) {
    // The error thrown by PEG.js is messy, format it
    if (e instanceof parser.SyntaxError) {
      let eNew = new SyntaxError(`unexpected ${e.found || 'end of input'}`);
      eNew.location = e.location;
      options.filename && (eNew.filename = options.filename);
      eNew.input = input;
      prettifyError(eNew); // Make error pretty
      throw eNew;
    }
    throw e;
  }
}

/**
 * Compiles a given Loom AST.
 * @param {ASTNode} parsedAst Input Loom AST
 * @param {Object} options Compilation options:
 *   - {string} modulesPath: path to Loom's modules
 *   - {boolean} produceValue: whether to force the last statement of the
 *       program to produce a value (important for the REPL, for example)
 *   - {boolean} jsAst: whether to output the AST representation of the compiled
 *       JavaScript
 *   - {boolean} js: whether to output the compiled JavaScript string
 *   - {string} filename: name of file being compiled (or something such as
 *       `'[repl]'`)
 *   - {Array<any>} presets: array of Babel presets to use in the transformation
 *   - {Array<any>} plugins: array of Babel plugins to use in the transformation
 * @returns {Object} Object that may contain the following properties (depending
 *   on the provided options):
 *   - {Object} jsAst: JavaScript AST representation of the compiled code
 *   - {string} js: compiled JavaScript string
 */
function compile(parsedAst, options) {
  options = options ? extend(COMPILE_OPTIONS, options) : COMPILE_OPTIONS;
  // Options to pass to Babel
  let babelOptions = {filename: options.filename, code: options.js,
    ast: options.jsAst, presets: options.presets,
    plugins: [objectRestSpread, ...options.plugins]};
  let compiled = compileLoom(parsedAst, {modulesPath: options.modulesPath,
                                         produceValue: options.produceValue});
  let transformed = transformFromAst(compiled.jsAst, null, babelOptions);
  return {jsAst: transformed.ast, js: transformed.code};
}

/**
 * Run the compiled JavaScript code correctly, setting `__filename`,
 * `__dirname`, and relative `require()`.
 * @param {String} js JavaScript code to run
 * @param {Object} options Running options:
 *   - {string} filename: Name of file containing the code to run (defaults to
 *       `'.'`)
 * @returns {void}
 */
function run(js, options = {}) {
  let mainModule = require.main;
  // Set the filename
  mainModule.filename = process.argv[1] = options.filename || '.';
  // Clear module cache
  mainModule.moduleCache && (mainModule.moduleCache = {});
  // Assign paths for `node_modules` loading
  mainModule.paths = Module._nodeModulePaths(
    options.filename ? path.dirname(options.filename) : '.');
  // Compile
  mainModule._compile(js, mainModule.filename);
}

/**
 * Register Loom. This makes Node able to require Loom source files.
 * @returns {void}
 */
function register() {
  // Compile and run a file with the `.loom` extension when requiring it
  require.extensions['.loom'] = (module, filename) => {
    let input = fs.readFileSync(filename, 'utf8');
    if (input.charCodeAt(0) === 0xFEFF) { // Strip UTF BOM
      input = input.slice(1);
    }
    try { // Compile and run
      let js = compile(parse(input, {filename})).js;
      module._compile(js, filename);
    } catch(e) {
      throw e; // TODO: Prettify syntax errors in other files
    }
  };
}

// Exported values
module.exports = {VERSION: loomVersion, nodes: ast, parse, compile, run,
                  register};

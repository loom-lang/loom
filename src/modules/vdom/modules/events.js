/**
 * This module handles the updating of the event listeners of a DOM element.
 */

// Dependencies
let Reactive = require('../../reactive/reactive');
let {isEventListener, getEventListenerName, associateListener,
     disassociateListener} = require('./utils');

/**
 * Function called whenever an event is triggered. This supports multiple
 * formats for user defined listeners (e.g. function, reactive, array of
 * functions/reactives).
 * @param {string} attr Attribute representing event listener (e.g. `"onClick"`)
 * @param {any} listener User defined listener
 * @param {Event} e Triggered DOM event
 * @returns {void}
 */
function handleEvent(attr, listener, e) {
  if (listener instanceof Reactive) { // Reactive listener, call its value
    handleEvent(attr, listener.value, e);
  } else if (typeof listener === 'function') { // Normal listener, call it
    listener.call(null, e);
  } else if (Array.isArray(listener)) { // Array of listeners, call all elements
    for (let elem of listener) {
      handleEvent(attr, elem, e);
    }
  } else {
    console.warn(`${attr}: invalid listener: ${listener}`);
  }
}

/**
 * Adds an event listener to a given element.
 * @param {Element} elem DOM element where listener will be added
 * @param {string} attr Attribute representing the event listener to add
 * @param {Function} handler Event handler
 * @param {Object} api API to interact with a DOM element
 * @returns {void}
 */
function addEventListener(elem, attr, handler, api) {
  api.addEventListener(elem, getEventListenerName(attr), handler);
  associateListener(elem, 'event', attr, handler);
}

/**
 * Removes an event listener from a element.
 * @param {Element} elem DOM Element from which to remove event listener
 * @param {string} attr Attribute representing the event listener to remove
 * @param {Object} api API to interact with a DOM element
 * @returns {void}
 */
function removeEventListener(elem, attr, api) {
  let handler = disassociateListener(elem, 'event', attr);
  api.removeEventListener(elem, getEventListenerName(attr), handler);
}

/**
 * Updates the event listeners of a DOM element given the old and new attributes
 * associated with such element.
 * @param {Element} elem Element on which events will be applied
 * @param {Object} oldAttrs Old attributes of the element
 * @param {Object} attrs New attributes of the element
 * @param {Object} api API to interact with a DOM element
 * @returns {void}
 */
function updateEvents(elem, oldAttrs, attrs, api) {
  if (oldAttrs === attrs) { // Attributes are static
    return;
  }

  // Remove old attributes
  for (let key in oldAttrs) {
    if (isEventListener(key) && oldAttrs[key] !== attrs[key]) {
      // Handle removing/changing of event listeners and reactive values
      removeEventListener(elem, key, api);
    }
  }

  // Add/update new attributes
  for (let key in attrs) {
    let value = attrs[key];
    if (isEventListener(key) && oldAttrs[key] !== value) {
      // Add event listener
      addEventListener(elem, key, (e) => handleEvent(key, value, e), api);
    }
  }
}

// Exported module functions
module.exports = {
  create: (elem, attrs, api) => updateEvents(elem, {}, attrs, api),
  update: updateEvents,
  destroy: (elem, attrs, api) => updateEvents(elem, attrs, {}, api)
};

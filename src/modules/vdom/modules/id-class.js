/**
 * This module handles the updating of the `id` and `class` of the element. It
 * takes into account the static id and classes of the element, which have been
 * passed to the attributes in the `staticId` and `staticClasses` properties.
 * E.g.: given an element such as:
 *   `<div#x.a.b {id: "y", class: "c"} />`
 * The element's `staticId` is `'x'` and `staticClasses` is `['a', 'b']`;
 * however, this module will set the DOM element's `id` to `'y'` and `class` to
 * `['a', 'b', 'c']`.
 */

# Loom

## Installation

Loom is availabe as an [npm](https://www.npmjs.com/package/loom-lang) package
with the name `loom-lang`. It may be installed via the following command:
```sh
npm install -g loom-lang
```

## Playground

Play around with Loom at: https://loom-lang.gitlab.io/loom-playground

## Development

### Building

Build with:
```sh
npm run build
```

### Testing

Run tests with:
```sh
npm test
```

/**
 * API to interact with the DOM and DOM elements.
 */

// DOM manipulation ============================================================
exports.appendChild = (node, child) => node.appendChild(child);
exports.firstChild = (node) => node.firstChild;
exports.createElement = (tag) => document.createElement(tag);
exports.createElementNS = (ns, tag) => document.createElementNS(ns, tag);
exports.createTextNode = (text) => document.createTextNode(text);
exports.insertBefore = (parent, node, ref) => parent.insertBefore(node, ref);
exports.nextSibling = (node) => node.nextSibling;
exports.parentElement = (node) => node.parentElement;
exports.removeChild = (node, child) => node.removeChild(child);
exports.getTextContent = (node) => node.textContent;
exports.setTextContent = (node, text) => node.textContent = text;
exports.nodeType = (node) => node.nodeType;
exports.tagName = (node) => node.tagName;

// Element manipulation ========================================================
exports.getAttribute = (el, attr) => el.getAttribute(attr);
exports.setAttribute = (el, attr, val) => el.setAttribute(attr, val);
exports.removeAttribute = (el, attr) => el.removeAttribute(attr);
exports.getProperty = (el, prop) => el[prop];
exports.setProperty = (el, prop, val) => el[prop] = val;
exports.removeProperty = (el, prop) => delete el[prop];
exports.addEventListener = (el, e, fn) => el.addEventListener(e, fn);
exports.removeEventListener = (el, e, fn) => el.removeEventListener(e, fn);
exports.setStyleProperty = (el, prop, val) => el.style.setProperty(prop, val);
exports.removeStyleProperty = (el, prop) => el.style.removeProperty(prop);

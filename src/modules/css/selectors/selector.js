/**
 * Class that represents a generic CSS selector.
 */
class Selector {}

// Exported class
module.exports = Selector;

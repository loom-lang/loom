// Dependencies
let browserApi = require('../browser-api');
let {ELEMENT_NODE} = require('../node-types');
let {onCreate, onInsert, onUpdate, onDestroy,
     onRemove} = require('../patch-attributes');
let {patch} = require('../patch');
let {virtualize} = require('./utils');

/**
 * Virtual representation of a DOM node. All virtual nodes must extend this
 * abstract class.
 */
class VirtualNode {
  /**
   * Renders this virtual node in a given DOM element.
   * @param {Node} node Node where to render this node
   * @param {Object?} api API to interact with the DOM (browser API by default)
   * @returns {void}
   */
  renderTo(node, api = browserApi) {
    // Hooks to pass to the `patch` function; these hooks trigger events inside
    // the virtual nodes themselves (e.g. causing the virtual nodes to start
    // listening to changes in their reactive values) and handle the patching of
    // DOM element attributes by calling the appropriate functions in
    // `patch-attributes`
    let hooks = {
      // Hook called when a DOM node has been associated with a virtual node
      onCreate: (vNode) => {
        // Virtual node now has a DOM node associated, call `onRender`
        vNode.onRender && vNode.onRender(hooks, api);
        if (vNode.type() === ELEMENT_NODE) { // Only elements have attributes
          onCreate(vNode.getNode(), vNode.attributes(), api);
        }
      },
      // Hook called when the virtual node's associated DOM node has been
      // inserted in the DOM
      onInsert: (vNode) => {
        if (vNode.type() === ELEMENT_NODE) { // Only elements have attributes
          onInsert(vNode.getNode(), vNode.attributes(), api);
        }
      },
      // Hook called when a DOM node stopped being represented by a virtual node
      // to start being represented by a different virtual node
      onUpdate: (oldVNode, vNode) => {
        // Virtual node no longer has a DOM node associated, call `onUnrender`
        oldVNode.onUnrender && oldVNode.onUnrender(hooks, api);
        // Virtual node now has a DOM node associated, call `onRender`
        vNode.onRender && vNode.onRender(hooks, api);
        if (vNode.type() === ELEMENT_NODE) { // Only elements have attributes
          onUpdate(vNode.getNode(), oldVNode.attributes(), vNode.attributes(),
                   api);
        }
      },
      // Hook called when a DOM node is to be removed from the DOM, either
      // directly or indirectly because one of its ancestors was removed (e.g.
      // if we have a tree: `<i> [<b/>, <a/>]` and the element with tag `i` is
      // removed, `onDestroy` will be called for the elements with tag `i`, `b`,
      // and `a`)
      onDestroy: (vNode) => {
        // Virtual node no longer has a DOM node associated, call `onUnrender`
        vNode.onUnrender && vNode.onUnrender(hooks, api);
        if (vNode.type() === ELEMENT_NODE) { // Only elements have attributes
          onDestroy(vNode.getNode(), vNode.attributes(), api);
        }
      },
      // Hooks called when a DOM node is to be directly removed from the DOM
      // (e.g. if we have a tree: `<i> [<b/>, <a/>]` and the element with tag
      // `i` is removed, `onRemove` will only be called for element with tag
      // `i`); this hook's function may return a promise, in which case the DOM
      // node may only be effectively removed from the DOM upon resolution of
      // said promise
      onRemove: (vNode) => {
        if (vNode.type() === ELEMENT_NODE) { // Only elements have attributes
          return onRemove(vNode.getNode(), vNode.attributes(), api);
        }
      }
    };

    // The `patch` function only accepts virtual nodes, as such we need to
    // transform the DOM node into a virtual node first
    let vNode = virtualize(node, api);
    // Virtual nodes may define a `beforeRender` method that should run before
    // the patching begins; this allows virtual nodes to resolve any lazy values
    // they may have
    vNode.beforeRender && vNode.beforeRender();
    this.beforeRender && this.beforeRender();
    patch(vNode, this, hooks, api); // Patch this node against `vNode`
  }
}

// Exported class
module.exports = VirtualNode;

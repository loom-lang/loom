/**
 * Module containing compilation-specific utilitarian functions.
 */

// Dependencies
let ast = require('../parser/nodes');
let js = require('./js-nodes');
let {extend} = require('../utils');

/**
 * A safe way of representing `undefined` in JavaScript (AST representation of
 * `void 0`).
 */
const VOID_0 = js.unaryExpression('void', js.numericLiteral(0));

/**
 * Transforms the given options in options used to compile a node as an
 * expression.
 * @param {Object} o Compilation options
 * @returns {Object} Transformed options
 */
function expressionOptions(o) {
  // No need to clone the object when the current one already states that we are
  // compiling an expression
  return o.expression ? o : extend(o, {expression: true, mustEndInValue: false,
                                       mustReturn: false, pushTo: null});
}

/**
 * Transforms the given options in options used to compile a node as a
 * statement.
 * @param {Object} o Compilation options
 * @param {boolean} mustReturn Whether something must be returned from statement
 * @param {JSIdentifier} pushTo Whether something must be pushed on statement
 * @param {boolean} mustEndInValue Whether the statement must produce a value
 * @param {Object} others Additional options
 * @returns {Object} Transformed options
 */
function statementOptions(o, mustReturn = false, pushTo = null,
                          mustEndInValue = false, others) {
  let options = {expression: false, mustReturn, pushTo, mustEndInValue};
  if (others != null) {
    for (let prop in others) {
      options[prop] = others[prop];
    }
  }
  return extend(o, options);
}

/**
 * Returns given `expression` or a statement, depending on `o.expression`. The
 * statement is either an expression statement, a return statement, or a
 * statement pushing the value to an array, depending on `o.mustReturn` and
 * `o.pushTo`.
 * @param {Object} o Compilation options
 * @param {JSNode} exp Expression being considered
 * @returns {JSNode} Resulting expression or statement
 */
function statementize(o, exp) {
  if (o.expression) {
    return exp;
  }
  if (o.mustReturn) {
    return js.returnStatement(exp);
  }
  // Push expression to identifier declared in `o.pushTo` (e.g. if `o.pushTo`
  // has the id `x`, this compiles to `x.push(expression)`)
  if (o.pushTo != null) {
    exp = js.callExpression(js.memberExpression(o.pushTo, js.identifier('push'),
                                                false), [exp]);
  }
  return js.expressionStatement(exp);
}

/**
 * Turns a given expression into a block expression that outputs said
 * expression (e.g. `x` would turn into `do {x}`, `do {x}` would remain the
 * same)
 * @param {ASTNode} exp Expression to turn into a block expression
 * @returns {ASTNode} Block expression containing given expression
 */
function blockify(exp) {
  return exp instanceof ast.Block ? exp : new ast.Block(exp.location, [exp]);
}

/**
 * Creates an immediately-invoked function expression (IIFE), given the
 * JavaScript block that represents the function's body. Example: given (in JS)
 * `{let x; return x;}` we return `(() => {let x; return x;})()`
 * @param {JSNode} block Body of the function
 * @returns {JSNode} IIFE containing the given body
 */
function iife(block) {
  return js.callExpression(js.arrowFunctionExpression([], block, false), []);
}

/**
 * Returns the id used to access the module `moduleName`. If the module was
 * never used before, an id is created and associated to the module for future
 * accesses.
 * @param {string} moduleName Name of module to access
 * @param {string | Array} specifier If a string then the module is to be
 *   declared with a default specifier (e.g. `import _x from 'm'` given `'_m'`);
 *   if it is a pair of strings then it becomes an import specifier (e.g.
 *   `import {x as _x} from 'm'` given a pair with (`'x'`, `'_x'`))
 * @param {Object} o Compilation options
 * @returns {JSIdentifier} Id used to access the module
 */
function getModuleId(moduleName, specifier, o) {
  // Whether the module will be accessed with a default specifier
  let isDefaultSpecifier = typeof specifier === 'string';
  // Check if module with given name has already been used; we store the modules
  // in `o.auxiliaryModules` as either a simple `JSIdentifier` for default
  // specifiers or objects pairing the specifier name to its respective
  // `JSIdentifier`
  let module = o.auxiliaryModules.get(moduleName);
  if (module == null) { // Module never used before
    let id;
    if (isDefaultSpecifier) {
      module = id = js.identifier(specifier);
    } else {
      module = {[specifier[0]]: id = js.identifier(specifier[1])};
    }
    // Set-up structures to handle the import
    o.auxiliaryIds.push(id);
    o.auxiliaryModules.set(moduleName, module);
    o.modulesToImport.push(moduleName);
  } else if (module[specifier[0]] == null) { // Module used, but new specifier
    let id = js.identifier(specifier[1]);
    module[specifier[0]] = id;
    o.auxiliaryIds.push(id);
  }
  return isDefaultSpecifier ? module : module[specifier[0]];
}

/**
 * Prepends a list of statements with the declaration of the auxiliary ids that
 * were created when compiling such statement.
 * @param {Array<JSStatement>} body List of statements
 * @param {Array<[JSIdentifier, JSNode?]>} auxiliaryIds List of auxiliary ids to
 *   declare
 * @returns {void}
 */
function declareAuxiliaryIds(body, auxiliaryIds) {
  if (auxiliaryIds.length > 0) {
    let declarators = auxiliaryIds.map(
      ([id, value]) => js.variableDeclarator(id, value));
    // If the first statement of the body is a variable declaration, append the
    // new declarations to it
    if (body.length > 0 && body[0].type === 'VariableDeclaration' &&
        body[0].kind === 'var') {
      body[0].declarations.unshift(...declarators);
    } else { // Create a new declaration
      body.unshift(js.variableDeclaration('var', declarators));
    }
  }
}

/**
 * Compiles a loop taking into consideration when it is being used as an
 * expression or as a statement.
 * @param {Object} o Compilation options
 * @param {boolean} needsIndex Whether the compiled loop requires an auxiliary
 *   variable to keep track of the index
 * @param {Function} loopFun A function that, given the appropriate options to
 *   compile the body of the loop and (if requested) the id of the auxiliary
 *   index outputs the compiled loop
 * @returns {JSNode} Compiled loop
 */
function compileLoop(o, needsIndex, loopFun) {
  // When the loop requires an auxiliary variable to keep track of the index
  let indexId, indexToDeclare;
  if (needsIndex) {
    indexId = js.identifier('_i');
    indexToDeclare = [indexId, js.numericLiteral(0)];
    o.auxiliaryIds.push(indexId);
  }

  // When the loop is being used as a statement and its resulting value is not
  // required
  if (!o.expression && !o.mustReturn && o.pushTo == null && !o.mustEndInValue) {
    // Declare index in current block if necessary
    needsIndex && o.idsToDeclare.push(indexToDeclare);
    return loopFun(o, indexId);
  }

  // Otherwise we require an auxiliary variable where to store the result
  let resId = js.identifier('_res');
  let resToDeclare = [resId, js.arrayExpression([])]; // Initialize with `[]`
  o.auxiliaryIds.push(resId);

  // Compile the loop (forcing the body to push to the result)
  let loop = loopFun(statementOptions(o, false, resId), indexId);

  // When being compiled as an expression, the loop requires an IIFE
  if (o.expression) {
    let funBody = []; // Body of the IIFE
    // Declare the auxiliary ids in the IIFE's body
    needsIndex && declareAuxiliaryIds(funBody, [indexToDeclare]);
    declareAuxiliaryIds(funBody, [resToDeclare]);
    // Push loop (and possible additional statements) to the function's body
    loop instanceof Array ? funBody.push(...loop) : funBody.push(loop);
    // Return the result from the IIFE
    funBody.push(js.returnStatement(resId));
    return statementize(o, iife(js.blockStatement(funBody, [])));
  }

  // Declare the ids in the current block (possibly inside another loop)
  needsIndex && o.idsToDeclare.push(indexToDeclare);
  o.idsToDeclare.push(resToDeclare);
  // Return or push the result
  return [loop, statementize(o, resId)];
}

/**
 * Splits a declaration with multiple declarators into multiple declarations
 * with single declarators. Example: `let x = 1, y = 2;` becomes
 * [`let x = 1;`,`let y = 2;`].
 * @param {JSVariableDeclaration} declaration Variable declaration with
 *   multiple declarators
 * @returns {Array<JSVariableDeclaration>} Array of variable declarations
 */
function splitDeclaration(declaration) {
  let split = [];
  for (let declarator of declaration.declarations) {
    split.push(js.variableDeclaration(declaration.kind, [declarator]));
  }
  return split;
}

/**
 * Compiles the patterns in the parameters of a function.
 * @param {Array<ASTNode>} parameters Function parameters to compile
 * @param {Object} o Compilation options
 * @param {Function} c Compilation function
 * @returns {Array<JSNode>} Compiled parameters
 */
function compileParameters(parameters, o, c) {
  let oExp = expressionOptions(o);
  return parameters.map((parameter) => {
    let [pattern, names] = c(parameter.pattern, o);
    for (let name of names) { // Declare bound names
      o.declaredNames.add(name);
    }
    if (parameter.initializer != null) { // When parameter has a default value
      pattern = js.assignmentPattern(pattern,
                                     c(parameter.initializer, oExp));
    } else if (parameter.rest) { // Rest parameter
      pattern = js.restElement(pattern);
    }
    return pattern;
  });
}

/**
 * Caches complex accesses and properties in the left-hand side of an
 * assignment. Newly generated identifiers are added to the environment.
 * Examples:
 *   `x` returns pair (`x`, `x`)
 *   `x.y` returns pair (`x.y`, `x.y`)
 *   `x[y++]` returns pair (`x[prop = y++]`, `x[prop]`)
 *   `x.y.z` returns pair (`(base = x.y).z`, `base.z`)
 *   `x.y[z++]` returns pair (`(base = x.y)[prop = z++]`, `base[prop]`)
 * @param {ASTNode} node Left-hand side node to deconstruct/cache
 * @param {Object} o Compilation options
 * @param {Function} c Compilation function
 * @returns {[JSNode, JSNode]} Pair of generated nodes (the first to be used as
 *   assignee, the second in the assigned expression)
*/
function cacheUpdatable(node, o, c) {
  let left, leftRef;
  let oNew = expressionOptions(o);
  if (node instanceof ast.IdentifierPattern) { // Basic left side (e.g. `x`)
    left = leftRef = c(node, oNew)[0];
  } else { // `node instanceof ast.MemberPattern`
    let base, baseRef, prop, propRef;
    // Basic member object (e.g. `x.y`, `x[y++]`, no need to cache `x`)
    if (node.object instanceof ast.Identifier) {
      base = baseRef = c(node.object, oNew);
    } else { // Complex member object (e.g. `x.y.z`, need to cache `x.y`)
      baseRef = js.identifier('_base');
      o.auxiliaryIds.push(baseRef);
      o.idsToDeclare.push([baseRef, null]);
      base = js.assignmentExpression('=', baseRef, c(node.object, oNew));
    }
    // Basic member property (e.g. `x.y`, `x.y[0]`, no need to cache `y` or `0`)
    if (!node.computed || node.property instanceof ast.Identifier ||
        node.property instanceof ast.Literal) {
      prop = propRef = node.computed ? c(node.property, oNew) :
                                       js.identifier(node.property);
    } else { // Complex member property (e.g. `x[y++]`, need to cache `y++`)
      propRef = js.identifier('_name');
      o.auxiliaryIds.push(propRef);
      o.idsToDeclare.push([propRef, null]);
      prop = js.assignmentExpression('=', propRef, c(node.property, oNew));
    }
    left = js.memberExpression(base, prop, node.computed);
    leftRef = js.memberExpression(baseRef, propRef, node.computed);
  }
  return [left, leftRef];
}

/**
 * Transforms a given JavaScript value in its JavaScript AST representation.
 * Example (with simplified JS AST representation):
 *   `1` returns `JSNumeric(1)`
 *   `null` returns `JSNull()`
 *   `{a: "hi"}` becomes `JSObject([JSProperty(JSId('a'), JSString('hi'))])`
 * Values found that are instances of `ast.ASTNode` are compiled with the given
 * options and compilation function.
 * @param {Object} value Value to transform into its JS AST representation
 * @param {Object} o Compilation options
 * @param {Function} c Compilation function
 * @returns {JSNode} JavaScript AST representation of the given object
 */
function jsToAST(value, o, c) {
  if (value instanceof ast.ASTNode) { // Compile AST node
    return c(value, o);
  } else if (Array.isArray(value)) { // Transform into JS array AST
    return js.arrayExpression(value.map((elem) => jsToAST(elem, o, c)));
  } else {
    switch (typeof value) {
      case 'boolean':   return js.booleanLiteral(value);
      case 'number':    return js.numericLiteral(value);
      case 'string':    return js.stringLiteral(value);
      case 'undefined': return VOID_0;
      case 'object': {
        if (value === null) {
          return js.nullLiteral();
        }
        // Transform into (simplified) JS object AST, note that we assume that
        // keys will always be ids
        let props = [];
        for (let prop in value) {
          props.push(js.objectProperty(
            js.identifier(prop), jsToAST(value[prop], o, c), false, false, []));
        }
        return js.objectExpression(props);
      }
    }
  }
}

// Exported utilities
module.exports = {VOID_0, expressionOptions, statementOptions, statementize,
  blockify, iife, getModuleId, declareAuxiliaryIds, compileLoop,
  splitDeclaration, compileParameters, cacheUpdatable, jsToAST};

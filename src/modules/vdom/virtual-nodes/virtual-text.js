// Dependencies
let VirtualNode = require('./virtual-node');
let {TEXT_NODE} = require('../node-types');

/**
 * Virtual representation of a DOM text node.
 */
class VirtualText extends VirtualNode {
  /**
   * Virtual text node's constructor.
   * @param {string} text Node's text
   * @param {Text} node DOM text node this virtual text node represents
   */
  constructor(text, node) {
    super();
    // Text node's text
    this._text = text;
    // DOM text node this virtual text node is representing
    this._node = node;
  }

  /**
   * Type of DOM node (as defined in the HTML spec) this virtual node
   * represents. All instances of this class represent DOM text nodes, defined
   * in the DOM in `Node.TEXT_NODE` (number `3`).
   * @returns {number} `Node.TEXT_NODE`
   */
  type() {
    return TEXT_NODE;
  }

  /**
   * The node's text.
   * @returns {string} Node's text
   */
  text() {
    return this._text;
  }

  /**
   * Gets the DOM text node being represented by this virtual node.
   * @returns {Text} DOM text node being represented
   */
  getNode() {
    return this._node;
  }

  /**
   * Sets the DOM text node this virtual node will represent.
   * @param {Text} node DOM text node to represent
   * @returns {void}
   */
  setNode(node) {
    this._node = node;
  }
}

// Exported class
module.exports = VirtualText;

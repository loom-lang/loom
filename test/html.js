/**
 * Testing of HTML.
 */

let {testSyntax} = require('./utils');

suite('HTML');

// Syntax
test('syntax', function() {
  let pass = ['<i/>', '< i />', '<i>[]', '< i > [ ]', '<i{}/>', '< i { } />',
              '<i{}>[]', '< i { } > [ ]', '<i#a/>', '<i.a.b/>', '<i#a.b.c/>',
              '<i 2 > 1 />', '<i (2 > 1)> []', '<i/regex/> []', '<i(/regex/)/>'
              /* '<i 1, 2> []' */];
  let fail = ['<i>', '<i/ >', '<i/>[]', '< i /> [ ]', '<#a/>', '<.a/>',
              '<i.a#b/>', '<i #a.b/>', '<i/regex//>'];
  testSyntax(pass, fail);
});

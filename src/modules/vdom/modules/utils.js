// Dependencies
let Reactive = require('../../reactive/reactive');

/**
 * Attributes that shouldn't be added to a DOM element.
 */
const HOOKS = new Set(['oncreate', 'oninsert', 'ondestroy', 'onremove']);

/**
 * Boolean attributes: attributes whose value is not relevant; what's relevant
 * is their presence as an attribute of an element.
 */
const BOOLEAN_ATTRS = new Set(['allowfullscreen', 'async', 'autofocus',
  'autoplay', 'checked', 'compact', 'controls', 'declare', 'default',
  'defaultchecked', 'defaultmuted', 'defaultselected', 'defer', 'disabled',
  'enabled', 'formnovalidate', 'hidden', 'indeterminate', 'inert', 'ismap',
  'itemscope', 'loop', 'multiple', 'muted', 'nohref', 'noresize', 'noshade',
  'novalidate', 'nowrap', 'open', 'pauseonexit', 'readonly', 'required',
  'reversed', 'scoped', 'seamless', 'selected', 'sortable', 'translate',
  'truespeed', 'typemustmatch', 'visible']);

/**
 * Enumerated attributes that must have `"true"` or `"false"` set as values.
 */
const TRUE_FALSE_ATTRS = new Set(['contenteditable', 'draggable',
                                  'spellcheck']);

/**
 * Attributes that should be using properties for binding.
 */
const NEEDS_PROPERTY = new Set(['checked', 'muted', 'selected', 'value']);

/**
 * Attributes whose value should be a comma separated list of values.
 */
const COMMA_LIST = new Set(['accept', 'coords', 'rel', 'sizes', 'srcset']);

/**
 * Property to add to a DOM element to keep track of the listeners currently
 * associated to it.
 */
const ELEM_LISTENERS_PROPERTY = '_listeners';

/**
 * Whether a given attribute is a "normal" attribute, i.e. that should be set on
 * a DOM element with `setAttribute` or something similar.
 * @param {string} attr Attribute to check if is normal
 * @returns {boolean} Whether attribute is a "normal" attribute
 */
function isNormalAttribute(attr) {
  return attr !== 'css' && !HOOKS.has(attr) && !isEventListener(attr);
}

/**
 * Whether a given attribute is a "boolean" attribute.
 * @param {string} attr Attribute to check if is a boolean attribute
 * @returns {boolean} Whether attribute is a boolean attribute
 */
function isBooleanAttribute(attr) {
  return BOOLEAN_ATTRS.has(attr);
}

/**
 * Whether a given attribute should use a property for binding.
 * @param {string} attr Attribute to check if should be set through a property
 * @returns {boolean} Whether attribute should be set through a property
 */
function attributeNeedsProperty(attr) {
  return NEEDS_PROPERTY.has(attr);
}

/**
 * Whether a given attribute is an enumerated attributes that must have `"true"`
 * or `"false"` set as values.
 * @param {string} attr Attribute to check if is a "true/false" attribute
 * @returns {boolean} Whether attribute is a "true/false" attribute
 */
function isTrueFalseAttribute(attr) {
  return TRUE_FALSE_ATTRS.has(attr);
}

/**
 * Whether a given attribute expects a comma separated list as its value.
 * @param {string} attr Attribute to check if it expects a comma separated list
 * @returns {boolean} Whether attribute expects a comma-separated list
 */
function expectsCommaListValue(attr) {
  return COMMA_LIST.has(attr);
}

/**
 * Whether a given attribute represents an event listener.
 * @param {string} attr Attribute to check if it represents an event listener
 * @returns {boolean} Whether attribute is an event listener
 */
function isEventListener(attr) {
  return !HOOKS.has(attr) && /^on/.test(attr);
}

/**
 * Gets the name of the event listener (to pass to `addEventListener`) given the
 * attribute representing the event listener.
 * @param {string} attr Attribute with event listener name to get
 * @returns {string} Name of event listener
 */
function getEventListenerName(attr) {
  return attr.slice(2).toLowerCase();
}

/**
 * Whether a given attribute of given element can have two-way binding.
 * @param {Element} elem DOM element to check if can two-way bind
 * @param {Object} attrs Attributes associated with the DOM element to check
 * @param {string} attr Attribute to check
 * @param {Object} api API to interact with a DOM element
 * @returns {string?} Name of event to listen to for the two-way binding or
 *   `null` when it cannot two-way bind
 */
function canTwoWayBind(elem, attrs, attr, api) {
  let tag = api.tagName(elem).toLowerCase();
  if (tag === 'input') {
    // The `input` can two-way bind depending on its type
    let type = attrs.type;
    if (type instanceof Reactive) { // XXX: Handle this
      type = type.value;
    }
    // `input`s with nothing to bind
    if (type === 'button' || type === 'reset' || type === 'submit') {
      return null;
    }
    // `checkbox`es and `radio`s can two-way bind the `checked` attribute
    // `onChange`
    if ((type === 'checkbox' || type === 'radio') && attr === 'checked') {
      return 'change';
    }
    // Other `input`s can two-way bind their `value` attribute `onInput`
    if (attr === 'value') {
      return 'input';
    }
    return null;
  }
  // `textarea`s can two-way bind their `value` attribute `onInput`
  if (tag === 'textarea' && attr === 'value') {
    return 'input';
  }
  // `select`s can two-way bind their `value` attribute `onChange`
  if (tag === 'select' && attr === 'value') {
    return 'change';
  }
  return null;
}

/**
 * Gets value associated to attribute from a DOM element when it changes after
 * user-input.
 * @param {Element} elem DOM element from where to get value
 * @param {Object} attrs Attributes associated with the DOM element
 * @param {Object} api API to interact with the DOM element
 * @returns {any} Retrieved value
 */
function getTwoWayValue(elem, attrs, api) {
  // TODO: If the mutable has a string but the `input` has type `number`, should
  // the mutable be set with a string or should it become a number?
  // TODO: Missing date related input types.
  let tag = api.tagName(elem).toLowerCase();
  if (tag === 'input') {
    let type = attrs.type;
    if (type instanceof Reactive) { // XXX: Handle this
      type = type.value;
    }
    // `checkbox`es and `radio`s two-way bind with a boolean `checked` attribute
    if (type === 'checkbox' || type === 'radio') {
      return Boolean(api.getProperty(elem, 'checked'));
    }
    // `number`s and `range`s two-way bind with a number `value` attribute
    if (type === 'number' || type === 'range') {
      return Number(api.getProperty(elem, 'value'));
    }
  }
  // All the rest two-way bind with a string `value` attribute
  return api.getProperty(elem, 'value');
}

/**
 * Associates a listener with a DOM element.
 * @param {Element} elem DOM element with which to associate listener
 * @param {string} type Type of the listener, there can only be one listener of
 *   a given type per attribute in a single element
 * @param {string} attr Attribute to associate listener to
 * @param {any} listenerKey Key that represents the listener
 * @returns {void}
 */
function associateListener(elem, type, attr, listenerKey) {
  let listeners = elem[ELEM_LISTENERS_PROPERTY] ||
                  (elem[ELEM_LISTENERS_PROPERTY] = {});
  listeners[`${type}-${attr}`] = listenerKey;
}

/**
 * Disassociates a listener from a DOM element.
 * @param {Element} elem DOM element from which to disassociate listener
 * @param {string} type Type of the listener
 * @param {string} attr Attribute to associate listener to
 * @returns {any} Key that represents the removed listener
 */
function disassociateListener(elem, type, attr) {
  let listenerName = `${type}-${attr}`;
  let listenerKey = elem[ELEM_LISTENERS_PROPERTY][listenerName];
  delete elem[ELEM_LISTENERS_PROPERTY][listenerName];
  return listenerKey;
}

// Exported functions
module.exports = {isNormalAttribute, isBooleanAttribute, attributeNeedsProperty,
  isTrueFalseAttribute, expectsCommaListValue, isEventListener,
  getEventListenerName, canTwoWayBind, getTwoWayValue, associateListener,
  disassociateListener};

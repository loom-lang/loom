// Dependencies
let Reactive = require('./reactive');

/**
 * Class used to represent mutable values, i.e. values whose associated value
 * can be changed, causing its subscribers to invalidate.
 */
class Mutable extends Reactive {
  /**
   * Constructor of a mutable value.
   * @param {any} value Value associated with the mutable
   */
  constructor(value) {
    super();
    // Current value of the mutable
    this._value = value;
  }

  /**
   * Gets the mutable's current value.
   */
  get value() {
    return this._value;
  }

  /**
   * Sets the value while causing subscribed signals to become invalid.
   * @param {any} value Value to set on mutable
   */
  set value(value) {
     // No need to invalidate anything when nothing changed
    if (value === this._value) {
      return;
    }
    this._value = value;
    // Array that will contain reactives with `onInvalidate` functions to run
    let invalids = [];
    if (this._onInvalidate.size > 0) {
      invalids.push(this);
    }
    // Invalidate all subscribers (they will remove themselves from this set, so
    // no need to clear it)
    for (let subscriber of this._subscribers) {
      subscriber._invalidate(invalids);
    }
    // Execute all `onInvalidate` functions
    for (let reactive of invalids) {
      for (let fun of reactive._onInvalidate.values()) {
        fun(reactive);
      }
    }
  }
}

// Exported class
module.exports = Mutable;

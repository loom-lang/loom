/**
 * Calls a hook if it is defined in the element's attributes.
 * @param {Element} elem Element on which hook will be called
 * @param {Object} attrs Attributes of the element
 * @param {string} hookName Name of the hook to call
 * @returns {void}
 */
function callHook(elem, attrs, hookName) {
  let value = attrs[hookName];
  if (value != null) {
    if (Array.isArray(value)) {
      let promises = [];
      for (let hook of value) {
        let result = hook(elem);
        if (result instanceof Promise) {
          promises.push(result);
        }
      }
      if (promises.length > 0) {
        return Promise.all(promises);
      }
    } else {
      return value(elem);
    }
  }
}

// Exported module functions
module.exports = {
  create: (elem, attrs) => callHook(elem, attrs, 'oncreate'),
  insert: (elem, attrs) => callHook(elem, attrs, 'oninsert'),
  destroy: (elem, attrs) => callHook(elem, attrs, 'ondestroy'),
  remove: (elem, attrs) => callHook(elem, attrs, 'onremove')
};

/**
 * Constant used to help create a class for each of Loom's types.
 */
const TYPES = [
  ['Any',               []],
  ['Function',          ['params', 'return']],
  ['Boolean',           []],
  ['Number',            []],
  ['String',            []],
  ['Null',              []],
  ['Void',              []],
  ['RegularExpression', []],
  ['Array',             ['type']],
  ['Tuple',             ['elements']],
  ['Object',            ['properties']],
  ['Signal',            ['type']],
  ['HTML',              ['tag']]
];

/**
 * Class that represents a Loom type.
 */
class Type {}

// Export all types
exports.Type = Type;
for (let [type] of TYPES) {
  exports[type] = class extends Type {};
}

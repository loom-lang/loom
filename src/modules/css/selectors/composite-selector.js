// Dependencies
let Selector = require('./selector');

/**
 * Class that represents a composite selector used inside a CSS value.
 * Example: `a > b` would be a composite selector with combinator `'>'` with
 * both left and right sides being `SimpleSelector`s.
 */
class CompositeSelector extends Selector {
  /**
   * Constructor of a composite selector.
   * @param {string} combinator One of `' '`, `'>'`, `'+'`, `'~'`.
   * @param {SimpleSelector} left Left selector
   * @param {Selector} right Right selector
   */
  constructor(combinator, left, right) {
    super();
    this._combinator = combinator;
    this._left = left;
    this._right = right;
  }

  /**
   * Whether this selector references its parent.
   * @returns {boolean} Whether selector references its parent
   */
  referencesParent() {
    // NOTE: currently we only support parent references on the left side of a
    //   composite selector
    return this._left.referencesParent();
  }

  /**
   * String representation of the selector (as expected in CSS).
   * @param {Array<any>} ifValues Values appearing inside `:if()` qualifiers
   * @param {string} parent String representation of the parent element
   * @returns {string | boolean} String representation of the selector, `false`
   *   when the selector will never match
   */
  toString(ifValues, parent) {
    return this._left.toString(ifValues, parent) + this._combinator +
           this._right.toString(ifValues, parent);
  }
}

// Exported class
module.exports = CompositeSelector;

/**
 * This module handles the updating of the attributes of a DOM element.
 * The attributes patching algorithm supports reactive attributes.
 * Mutable values on an input element's value (e.g. `<input {type: "text",
 * value: m}/>` where `m` is a mutable) are bidirectional. This means that user
 * input changes the value of the mutable and changing the mutable
 * programmatically changes the value of the input.
 */

// Dependencies
let Mutable = require('../../reactive/mutable');
let Reactive = require('../../reactive/reactive');
let {isNormalAttribute, isBooleanAttribute, attributeNeedsProperty,
     isTrueFalseAttribute, expectsCommaListValue, canTwoWayBind, getTwoWayValue,
     associateListener, disassociateListener} = require('./utils');

/**
 * Sets an attribute on a DOM element taking into consideration the type of the
 * attribute being set (e.g. is a boolean attribute).
 * @param {Element} elem DOM element on which to set attribute
 * @param {string} key Name of attribute to set
 * @param {any} value Value to set on attribute
 * @param {Object} api API to interact with a DOM element
 * @returns {void}
 */
function setAttribute(elem, key, value, api) {
  // Transform array value into string
  if (Array.isArray(value)) {
    value = value.join(expectsCommaListValue(key) ? ',' : ' ');
  }

  if (attributeNeedsProperty(key)) {
    api.setProperty(elem, key, value);
  } else if (isBooleanAttribute(key)) { // Boolean attributes
    if (value == null || value === false) {
      api.removeAttribute(elem, key);
    } else {
      api.setAttribute(elem, key, key);
    }
  } else if (isTrueFalseAttribute(key)) { // True/false enumerated attributes
    api.setAttribute(elem, key,
      value == null || value === false || (typeof value === 'string' &&
        value.toLowerCase() === 'false') ? 'false' : 'true');
  } else if (value == null) {
    api.removeAttribute(elem, key);
  } else {
    api.setAttribute(elem, key, value);
  }
}

/**
 * Removes an attribute from a DOM element.
 * @param {Element} elem DOM element from which to remove attribute
 * @param {string} key Name of attribute to remove
 * @param {Object} api API to interact with a DOM element
 * @returns {void}
 */
function removeAttribute(elem, key, api) {
  if (attributeNeedsProperty(key)) {
    api.removeProperty(elem, key);
  } else { // Normal attribute
    api.removeAttribute(elem, key);
  }
}

/**
 * Adds a listener to a given reactive that represents the value of some
 * attribute. This listener is associated to the DOM element that contains said
 * attribute. If the node and the attribute supports two-way binding, then an
 * event listener is also added to the element in order to update the reactive
 * (if a mutable) when appropriate.
 * @param {Element} elem DOM element where listener will be stored
 * @param {Object} attrs Attributes associated with the DOM element
 * @param {string} attr Attribute to associate listener with
 * @param {Reactive} reactive Reactive value to listen to
 * @param {Object} api API to interact with a DOM element
 * @returns {void}
 */
function addReactiveListener(elem, attrs, attr, reactive, api) {
  let sym = reactive.onInvalidate(
    () => setAttribute(elem, attr, reactive.value, api));
  associateListener(elem, 'attribute', attr, sym);

  // Build a two-way binding if necessary
  let eventName = canTwoWayBind(elem, attrs, attr, api);
  if (eventName != null && reactive instanceof Mutable) {
    let eventHandler = () => reactive.value = getTwoWayValue(elem, attrs, api);
    api.addEventListener(elem, eventName, eventHandler);
    associateListener(elem, 'two-way', attr, eventHandler);
  }
}

/**
 * Removes the listener of a reactive value previously associated to some
 * attribute. It further removes any event listener that may have been added
 * thanks to an attribute that supports two-way binding.
 * @param {Element} elem Element where listener was stored
 * @param {Object} attrs Old attributes associated with the element
 * @param {string} attr Attribute to disassociate listener from
 * @param {Reactive} reactive Reactive value to stop listening to
 * @param {Object} api API to interact with a DOM element
 * @returns {void}
 */
function removeReactiveListener(elem, attrs, attr, reactive, api) {
  let sym = disassociateListener(elem, 'attribute', attr);
  reactive.removeListener(sym);

  let eventName = canTwoWayBind(elem, attrs, attr, api);
  if (eventName != null && reactive instanceof Mutable) {
    let handler = disassociateListener(elem, 'two-way', attr);
    api.removeEventListener(elem, eventName, handler);
  }
}

/**
 * Updates the attributes of a DOM element given its old and new attributes.
 * @param {Element} elem Element on which attributes will be applied
 * @param {Object} oldAttrs Old attributes of the element
 * @param {Object} attrs New attributes of the element
 * @param {Object} api API to interact with a DOM element
 * @returns {void}
 */
function updateAttributes(elem, oldAttrs, attrs, api) {
  if (oldAttrs === attrs) { // Attributes are static
    return;
  }

  // Remove old attributes
  for (let key in oldAttrs) {
    if (isNormalAttribute(key)) { // Ignore reserved attributes
      let oldValue = oldAttrs[key];
      // Handle removing/changing of event listeners and reactive values
      if (oldValue !== attrs[key]) { // Properties are static
        if (oldValue instanceof Reactive) { // Add reactive listener
          removeReactiveListener(elem, oldAttrs, key, oldValue, api);
        }
        // Remove attributes no longer present in the current node (if they
        // are present then they'll be overwritten)
        if (!(key in attrs)) {
          removeAttribute(elem, key, api);
        }
      }
    }
  }

  // Add/update new attributes
  for (let key in attrs) {
    if (isNormalAttribute(key)) { // Ignore reserved attributes
      let value = attrs[key];
      if (oldAttrs[key] !== value) { // Properties are static
        if (value instanceof Reactive) { // Add reactive listener
          addReactiveListener(elem, attrs, key, value, api);
          value = value.value;
        }
        setAttribute(elem, key, value, api);
      }
    }
  }
}

// Exported module functions
module.exports = {
  create: (elem, attrs, api) => updateAttributes(elem, {}, attrs, api),
  update: updateAttributes,
  destroy: (elem, attrs, api) => updateAttributes(elem, attrs, {}, api)
};

/**
 * Module responsible for extending the JavaScript classes that represent some
 * of Loom's native values such as mutables, reactives, and HTML values with an
 * `inspect` method used by the REPL to pretty-print such values.
 * These methods are not added directly in the class definitions to keep them
 * from being added to bundles together with Node's `util` module.
 */

// Dependencies
let {extend} = require('../utils');
let {inspect} = require('util');

// Classes to extend
let CompositeSelector = require('../modules/css/selectors/composite-selector');
let CSS = require('../modules/css/css');
let Mutable = require('../modules/reactive/mutable');
let Signal = require('../modules/reactive/signal');
let SimpleSelector = require('../modules/css/selectors/simple-selector');
let VirtualElement = require('../modules/vdom/virtual-nodes/virtual-element');

// Add new styles to the node inspect function =================================
let NEW_STYLES = {html: 'yellow', qualifierId: 'green',
                  qualifierClass: 'cyan', qualifierPseudo: 'magenta'};
for (let style in NEW_STYLES) {
  inspect.styles[style] = NEW_STYLES[style];
}

// Extend language classes =====================================================
// e.g. `mut 1` will print `Mutable { 1 }`
Mutable.prototype.inspect = function(depth, opts) {
  return `Mutable { ${inspect(this.value, extend(opts, {depth}))} }`;
};

// e.g. `sig *x + 1` will print `[Signal]`
Signal.prototype.inspect = function(depth, opts) {
  // Consistent with how `inspect` shows a function (`'[Function]'` in colour)
  let str = '[Signal]';
  return opts.colors ? opts.stylize(str, 'special') : str;
};

// e.g. `<div#i.c {}> []` will print `<div#i.c/>`
VirtualElement.prototype.inspect = function(depth, opts) {
  if (depth < 0) { // When too nested, print `'[HTML]'` in colour
    let str = '[HTML]';
    return opts.colors ? opts.stylize(str, 'special') : str;
  }
  // New options to call inspect
  let attrsOpts = extend(opts, {depth, breakLength: Infinity}); // Inline attrs
  let childrenOpts = extend(opts, {depth});
  // Tag (possibly with qualifiers)
  let start = `<${this.tag()}`;
  let id = this.id() != null ? `#${this.id()}` : '';
  let classes = this.classes().length > 0 ? `.${this.classes().join('.')}` : '';
  // Don't show empty attributes
  let attrs = this.originalAttributes() == null ||
              Object.keys(this.originalAttributes()).length === 0 ?
              '' : ` ${inspect(this.originalAttributes(), attrsOpts)}`;
  // Don't show empty children
  let children = this.originalChildren() == null ||
    this.originalChildren().length === 0 ? '' :
      ` ${inspect(this.originalChildren(), childrenOpts)}`;
  let end = children === '' ? '/>' : '>';
  // Colourise id and classes (if they're not an empty string)
  if (opts.colors) {
    start = opts.stylize(start, 'html');
    id && (id = opts.stylize(id, 'qualifierId'));
    classes && (classes = opts.stylize(classes, 'qualifierClass'));
    end = opts.stylize(end, 'html');
  }
  return `${start}${id}${classes}${attrs}${end}${children}`;
};

// e.g. `css {"color": "red"}` will print `CSS {color: 'red'}`
CSS.prototype.inspect = function(depth, opts) {
  if (depth < 0) { // When too nested, print `'[CSS]'` in colour
    let str = '[CSS]';
    return opts.colors ? opts.stylize(str, 'special') : str;
  }
  // Transform an hyphenised property into its camel case equivalent
  let camelize = (property) => property
    .replace(/-([a-z])/g, (match) => match[1].toUpperCase())
    .replace(/^Ms([A-Z])/, 'ms$1');
  // Use a native object's `inspect`
  let obj = {};
  for (let [prop, value] of this.properties()) {
    obj[camelize(prop)] = value; // Camelise property
  }
  // TODO: Show rules
  // for (let {selectors, body} of this.rules()) {}

  let inspected = inspect(obj, extend(opts, {depth, innerCSS: true}));
  // Do not prefix with `'CSS'` when printing inner CSS values
  return opts.innerCSS ? inspected :
    `CSS${inspected.indexOf('\n') > 0 ? '\n' : ' '}${inspected}`;
};

SimpleSelector.prototype.inspect = function(depth, opts) {
  // TODO: Missing expression selectors
  let tag = this._tag !== '*' ? this._tag : '';
  let id = this._id != null ? `#${this._id}` : '';
  let classes = this._classes.length > 0 ? `.${this._classes.join('.')}` : '';
  let pseudos = this._pseudos.length > 0 ? `:${this._pseudos.join(':')}` : '';
  if (!tag && !id && !classes && !pseudos) {
    tag = '*';
  }
  if (opts.colors) {
    tag && (tag = opts.stylize(tag, 'html'));
    id && (id = opts.stylize(id, 'qualifierId'));
    classes && (classes = opts.stylize(classes, 'qualifierClass'));
    pseudos && (pseudos = opts.stylize(pseudos, 'qualifierPseudo'));
  }
  return `${tag}${id}${classes}${pseudos}`;
};

CompositeSelector.prototype.inspect = function(depth, opts) {
  let type = opts.colors ? opts.stylize(this._type, 'html') : this._type;
  return `${this._left.inspect(depth, opts)} ${type} ${
    this._right.inspect(depth, opts)}`;
};

/**
 * This module exposes a set of functions to build the AST nodes that support
 * the JavaScript grammar. Each function builds an AST node as specified in the
 * Babylon specification (https://github.com/babel/babylon).
 */

/**
 * Constant used to help create a function for each node defined in the spec.
 * Each node has a type and its list of properties.
 */
const NODES = [
  // Identifier ================================================================
  ['Identifier',               ['name']],

  // Literals ==================================================================
  ['RegExpLiteral',            ['pattern', 'flags']],
  ['NullLiteral',              []],
  ['StringLiteral',            ['value']],
  ['BooleanLiteral',           ['value']],
  ['NumericLiteral',           ['value']],

  // Programs ==================================================================
  ['Program',                  ['sourceType', 'body', 'directives']],

  // Statements ================================================================
  ['ExpressionStatement',      ['expression']],
  ['BlockStatement',           ['body', 'directives']],
  ['EmptyStatement',           []],
  ['DebuggerStatement',        []],
  ['WithStatement',            ['object', 'body']],

  // Control flow --------------------------------------------------------------
  ['ReturnStatement',          ['argument']],
  ['LabeledStatement',         ['label', 'body']],
  ['BreakStatement',           ['label']],
  ['ContinueStatement',        ['label']],

  // Choice --------------------------------------------------------------------
  ['IfStatement',              ['test', 'consequent', 'alternate']],
  ['SwitchStatement',          ['discriminant', 'cases']],
    ['SwitchCase',               ['test', 'consequent']],

  // Exceptions ----------------------------------------------------------------
  ['ThrowStatement',           ['argument']],
  ['TryStatement',             ['block', 'handler', 'finalizer']],
    ['CatchClause',              ['param', 'body']],

  // Loops ---------------------------------------------------------------------
  ['WhileStatement',           ['test', 'body']],
  ['DoWhileStatement',         ['body', 'test']],
  ['ForStatement',             ['init', 'test', 'update', 'body']],
  ['ForInStatement',           ['left', 'right', 'body']],
  ['ForOfStatement',           ['left', 'right', 'body']],

  // Declarations ==============================================================
  ['FunctionDeclaration',      ['id', 'params', 'body', 'generator', 'async']],
  ['VariableDeclaration',      ['kind', 'declarations']],
    ['VariableDeclarator',       ['id', 'init']],

  // Misc ======================================================================
  ['Decorator',                ['expression']],
  ['Directive',                ['value']],
  ['DirectiveLiteral',         ['value']],

  // Expressions ===============================================================
  ['Super',                    []],
  ['ThisExpression',           []],
  ['ArrowFunctionExpression',  ['params', 'body', 'expression']],
  ['YieldExpression',          ['argument', 'delegate']],
  ['AwaitExpression',          ['arguments']],
  ['ArrayExpression',          ['elements']],
  ['ObjectExpression',         ['properties']],
    ['ObjectProperty',           ['key', 'value', 'computed', 'shorthand',
                                  'decorators']],
    ['ObjectMethod',             ['key', 'value', 'computed', 'kind',
                                  'decorators']],
  ['RestProperty',             ['argument']],
  ['SpreadProperty',           ['argument']],
  ['FunctionExpression',       ['id', 'params', 'body', 'generator', 'async']],
  ['ConditionalExpression',    ['test', 'consequent', 'alternate']],
  ['CallExpression',           ['callee', 'arguments']],
  ['NewExpression',            ['callee', 'arguments']],
  ['SequenceExpression',       ['expressions']],

  // Unary operations ----------------------------------------------------------
  ['UnaryExpression',          ['operator', 'argument']],
  ['UpdateExpression',         ['operator', 'argument', 'prefix']],

  // Binary operations ---------------------------------------------------------
  ['BinaryExpression',         ['operator', 'left', 'right']],
  ['AssignmentExpression',     ['operator', 'left', 'right']],
  ['LogicalExpression',        ['operator', 'left', 'right']],
  ['SpreadElement',            ['argument']],
  ['MemberExpression',         ['object', 'property', 'computed']],
  ['BindExpression',           ['object', 'callee']],

  // Template literals =========================================================
  ['TemplateLiteral',          ['quasis', 'expressions']],
  ['TaggedTemplateExpression', ['tag', 'quasi']],
  ['TemplateElement',          ['value', 'tail']],

  // Patterns ==================================================================
  ['ObjectPattern',            ['properties']],
  ['ArrayPattern',             ['elements']],
  ['RestElement',              ['argument']],
  ['AssignmentPattern',        ['left', 'right']],

  // Classes ===================================================================
  ['ClassBody',                ['body']],
  ['ClassMethod',              ['key', 'value', 'kind', 'computed', 'static',
                                'decorators']],
  ['ClassProperty',            ['key', 'value']],
  ['ClassDeclaration',         ['id', 'superClass', 'body', 'decorators']],
  ['ClassExpression',          ['id', 'superClass', 'body', 'decorators']],
  ['MetaProperty',             ['meta', 'property']],

  // Modules ===================================================================
  // Imports -------------------------------------------------------------------
  ['ImportDeclaration',        ['specifiers', 'source']],
    ['ImportSpecifier',          ['local', 'imported']],
    ['ImportDefaultSpecifier',   ['local']],
    ['ImportNamespaceSpecifier', ['local']],
  // Exports -------------------------------------------------------------------
  ['ExportNamedDeclaration',   ['declaration', 'specifiers', 'source']],
    ['ExportSpecifier',          ['local', 'exported']],
  ['ExportDefaultDeclaration', ['declaration']],
  ['ExportAllDeclaration',     ['source']]
];

// Export a function per node
for (let [type, properties] of NODES) {
  // Export function with the first letter in lower case (i.e. the function to
  // create a `ForStatement` node is named `forStatement`)
  let lowerType = type.charAt(0).toLowerCase() + type.slice(1);

  /**
   * Function that, given the node's properties, returns its AST representation.
   * @param {...any} props Node's properties
   * @returns {Object} Object representation of node
   */
  exports[lowerType] = (...props) => {
    let obj = {type}; // Set type
    properties.forEach((prop, i) => obj[prop] = props[i]); // Set properties
    return obj;
  };
}

/**
 * Testing of control expressions (e.g. `if`, `for`).
 */

let {eq, eqArray, run} = require('./utils');

suite('Control expressions');

// Block expression
test('block expression', function() {
  eq(run('do{0;1;}'), 1);
  eq(run('do { 0 ; 1 ; }'), 1);
  eq(run('do {var x = 0; x + 1}'), 1);
});

// If expressions
test('if', function() {
  eq(run('if(true)1 else 2'), 1);
  eq(run('if ( true ) 1 else 2'), 1);
  eq(run('if (true) 1'), 1);
  eq(run('if ( true ) 1'), 1);
  eq(run('if (false) 1 else 2'), 2);
  eq(run('if (false) 1'), undefined);
  // Block expression syntactic sugar
  eq(run('if (1 + 1 == 2) {1} else {2}'), 1);
  // Equivalent to
  eq(run('if (1 + 1 == 2) do {1} else do {2}'), 1);
  // Object must be wrapped in `()` or `{}`
  eq(run('var o = if (true) ({x: 1}); o.x'), 1);
  eq(run('var o = if (true) {{x: 1}}; o.x'), 1);
  // Else is bound to last if
  eq(run('if (true) if (false) 1 else 2'), 2);
});

// Throw/try expressions
test('throw/try', function() {
  eq(run('try 1'), 1);
  eq(run('try 1 catch() 2 finally 3'), 1);
  eq(run('try 1 catch ( ) 2 finally 3'), 1);
  eq(run('try 1 catch(e) 2 finally 3'), 1);
  eq(run('try 1 catch ( e ) 2 finally 3'), 1);
  eq(run('try throw "error" catch(e) e'), 'error');
  // Block expression syntactic sugar
  eq(run('try {throw "error"} catch(e) {e}'), 'error');
  // `throw` can be used as an expression anywhere
  eq(run('try 1 + throw 2 catch (e) e'), 2);
});

// While and do while loops
test('while/do while loop', function() {
  eqArray(run('var i = 3; while(i--)i'), [2, 1, 0]);
  eqArray(run('var i = 3; while ( i-- ) i'), [2, 1, 0]);
  eqArray(run('var i = 3; do i while(i--)'), [3, 2, 1, 0]);
  eqArray(run('var i = 3; do i while ( i-- )'), [3, 2, 1, 0]);
});

// For in loops
test('for in loop', function() {
  eqArray(run('for(var e in [0,1,2])e'), [0, 1, 2]);
  eqArray(run('for ( var e in [ 0 , 1 , 2 ] ) e'), [0, 1, 2]);
  eqArray(run('for(var e,i in [0,1,2])e+i'), [0, 2, 4]);
  eqArray(run('for ( var e , i in [ 0 , 1 , 2 ] ) e + i'), [0, 2, 4]);
  eqArray(run('for (var [e], i in [[0], [1], [2]]) e + i'), [0, 2, 4]);
  eqArray(run('for (var {a}, i in [{a: 0}, {a: 1}]) a + i'), [0, 2]);
  // eqArray(run('var e; for (e in [0, 1, 2]) e'), [0, 1, 2]);
  // eqArray(run('var e, i; for (e, i in [0, 1, 2]) e + i'), [0, 2, 4]);
  // eqArray(run('var e, i; for ([e], i in [[0], [1], [2]]) e + i'), [0, 2, 4]);
});

// For of loops
test('for of loop', function() {
  eqArray(run('for(var k of {x:0,y:1,z:2})k'), ['x', 'y', 'z']);
  eqArray(run('for ( var k of { x : 0 , y : 1 , z : 2 } ) k'), ['x', 'y', 'z']);
  eqArray(run('for(var k,v of {x:0,y:1,z:2})v'), [0, 1, 2]);
  eqArray(run('for ( var k , v of { x : 0 , y : 1 , z : 2 } ) v'), [0, 1, 2]);
  eqArray(run('for (var k, [e] of {x: [0], y: [1]}) e'), [0, 1]);
  eqArray(run('for (var k, {a} of {x: {a: 0}, y: {a: 1}}) a'), [0, 1]);
  // eqArray(run('var k; for (k of {x: 0, y: 1, z: 2}) k'), ['x', 'y', 'z']);
  // eqArray(run('var k, v; for (k, v of {x: 0, y: 1, z: 2}) v'), [0, 1, 2]);
  // eqArray(run('var k, v; for (k, [v] of {x: [0], y: [1]}) v'), [0, 1]);
});

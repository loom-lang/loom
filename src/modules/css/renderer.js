// Dependencies
let Reactive = require('../reactive/reactive');
let {isUnitless, stringifyValue} = require('./utils');

/**
 * Attribute used to identify an element.
 */
const CSS_ATTRIBUTE_PREFIX = 'data-loom-css-';

const CSS_IF_PREFIX = 'data-loom-if-';

const CSS_CUSTOM_PROP_PREFIX = '--loom-prop-';

/**
 * Global counter for generating unique ids.
 * @type {number}
 */
let rendererCounter = 0;

/**
 * Class responsible for the rendering of CSS values in a `<style>` element.
 */
class Renderer {
  /**
   * Constructor of a renderer of CSS values.
   * @param {Element} elem DOM element against whom the CSS is being applied
   * @param {HTMLStyleElement} styleElem `<style>` element where the CSS value
   *   will be rendered.
   */
  constructor(elem, styleElem) {
    // Unique identifier for this CSS renderer
    this._id = rendererCounter++;
    // DOM element against whom the CSS will be applied
    this._elem = elem;
    // `<style>` element where the CSS is to be rendered
    this._styleElem = styleElem;

    // Listeners on reactive values used as property values; mapping from the
    // custom property name that is representing the reactive to a pair with the
    // reactive value as its first element and the listener as its second
    this._withUnitsListeners = new Map();
    this._unitlessListeners = new Map();
    // Set of attributes currently applied to the element on behalf of `if()`
    // qualifiers with truthy values
    this._ifAttributes = new Set();
    // Listeners on reactive values in `if()` qualifiers; mapping from the
    // reactive value to a pair `[attribute, listener]`, where `attribute` is
    // the name of the attribute representing the `if()` qualifier
    this._ifListeners = new Map();
    // Listeners for reactive bodies; mapping from the reactive value to its
    // listener
    this._bodyListeners = new Map();
  }

  /**
   * Transforms the properties of a CSS value into their string representation.
   * Reactive property values are turned into CSS variables.
   * Example:
   *   `Map([["margin", 1], ["color": someReactive]])`
   * becomes:
   *   `'margin:1px;color:var(--loom-prop-0)'`
   * @param {Map<string, any>} properties Map of attribute to value
   * @param {Object} o Options used when stringifying a CSS value
   * @returns {string} String representation of the properties
   */
  _propertiesToString(properties, o) {
    let str = '';
    // Map properties to their respective string representation
    for (let [prop, value] of properties) {
      let normalizedValue; // Normalised property value
      if (value instanceof Reactive) { // Handle reactive properties
        // Transform the value into a CSS variable; the same reactive value is
        // mapped to the same CSS custom property, unless it is used in both
        // unitless properties and properties with units
        let propType = isUnitless(prop) ? 'unitless' : 'withUnits';
        let propName = o.customProps[propType].get(value);
        if (propName == null) {
          // Custom CSS property is named: prefix + renderer id + counter
          o.customProps[propType].set(value, propName =
            `${CSS_CUSTOM_PROP_PREFIX}${this._id}-${o.counters.customProps++}`);
        }
        normalizedValue = `var(${propName})`;
      } else { // Handle normal properties
        normalizedValue = stringifyValue(prop, value);
      }
      // Stringified property
      str += `;${prop}:${normalizedValue}`;
    }
    return str.slice(1); // Remove initial semi-colon
  }

  /**
   * Transforms the rules of a CSS value into their string representation.
   * @param {Array<Object>} rules List of rules of a CSS value
   * @param {Object} o Options used when stringifying a CSS value
   * @returns {string} String representation of the rules
   */
  _rulesToString(rules, o) {
    let str = '';
    // Map rules to their respective string representation
    for (let {selectors, body} of rules) {
      // Selectors that will be the parents of the rule being processed
      let newParentSelectors = [];
      // New options to used when processing this rule
      let oNew = Object.assign({}, o, {parentSelectors: newParentSelectors});
      // Build the `newParentSelectors` array. Example (with simplified syntax):
      // If the current parent selectors (`o.parentSelectors`) are:
      //   `a, :hover`
      // and the rule being processed has selectors:
      //   `x, &.y, :if(val).z`
      // then, `newParentSelectors` must become:
      //   `a x, a.y, :hover x, :hover.y, [ATTR]a .z, [ATTR]:hover .z`
      // where `ATTR` is an attribute that will be set on `this._elem` when
      // `val` (or its reactive value) is truthy
      for (let parentSelector of o.parentSelectors) {
        for (let selector of selectors) {
          // The `toString` method of the selectors will push the values in
          // their `:if()` qualifiers to this array
          let ifValues = [];
          // The new parent selector becomes a descendent of the current parent
          // selector unless the selector being processed references its parent
          let newParentSelector = selector.referencesParent() ?
            selector.toString(ifValues, parentSelector) :
            `${parentSelector} ${selector.toString(ifValues)}`;

          // If the selector being processed has `:if()` qualifiers, we need to
          // prepend the new parent selector with the attribute qualifiers that
          // represent the values in the `:if()` qualifiers
          let attrQualifiers = '';
          // Map each value of the `:if()` qualifiers to the attribute qualifier
          // that represents it
          for (let ifValue of ifValues) {
            let attr; // Attribute representing the value in the `:if()`
            if (ifValue instanceof Reactive) { // Handle reactive values
              // If the reactive value has already appeared, reuse its attribute
              attr = o.reactiveIfs.get(ifValue);
              if (attr == null) { // Otherwise create a new attribute
                o.reactiveIfs.set(ifValue,
                  attr = `${CSS_IF_PREFIX}${this._id}-${o.counters.ifs++}`);
              }
            } else {
              // Non-reactive values in `:if()` qualifiers always map to a new
              // attribute qualifier
              o.nonReactiveIfs.set(
                attr = `${CSS_IF_PREFIX}${this._id}-${o.counters.ifs++}`,
                ifValue);
            }
            attrQualifiers += `[${attr}]`; // Add the new attribute qualifier
          }
          newParentSelectors.push(attrQualifiers + newParentSelector);
        }
        if (body instanceof Reactive) { // Resolve the body when it is reactive
          o.reactiveBodies.add(body);
          body = body.value;
        }
        str += this._cssToString(body, oNew); // Append the rule
      }
    }
    return str;
  }

  /**
   * Transforms a given CSS value into its string representation.
   * @param {CSS} css CSS value to stringify
   * @param {Object} o Options used when stringifying a CSS value
   * @returns {string} String representation of the CSS value
   */
  _cssToString(css, o) {
    return o.parentSelectors.join(',') +
           `{${this._propertiesToString(css.properties(), o)}}` +
           this._rulesToString(css.rules(), o);
  }

  _updateCustomPropListeners(curListeners, customProps, stringifyValue, api) {
    // Remove listeners for old reactive values no longer present in the new
    // properties
    for (let [reactive, [oldCustomProp, listener]] of curListeners) {
      let customProp = customProps.get(reactive);
      if (oldCustomProp !== customProp) {
        reactive.removeListener(listener);
        curListeners.delete(reactive);
        api.removeStyleProperty(this._elem, oldCustomProp);
      }
    }
    // Set listeners for new custom properties that differ from the old ones
    for (let [reactive, customProp] of customProps) {
      let [oldCustomProp] = curListeners.get(reactive) || [];
      if (oldCustomProp !== customProp) {
        curListeners.set(reactive, [customProp,
          reactive.onInvalidate((r) => api.setStyleProperty(this._elem,
            customProp, stringifyValue(r.value)))]);
        // Set the initial value
        api.setStyleProperty(this._elem, customProp,
                             stringifyValue(reactive.value));
      }
    }
  }

  _updateNonReactiveIfs(curAttributes, nonReactiveIfs, api) {
    // Remove attributes that should no longer be persent in the element
    for (let attr of curAttributes) {
      let val = nonReactiveIfs.get(attr);
      if (!val) {
        curAttributes.delete(attr);
        api.removeAttribute(this._elem, attr);
      }
    }
    // Add new attributes to the element
    for (let [attr, val] of nonReactiveIfs) {
      if (val && !curAttributes.has(attr)) {
        curAttributes.add(attr);
        api.setAttribute(this._elem, attr, '');
      }
    }
  }

  _updateIfListeners(curListeners, reactiveIfs, api) {
    // Remove listeners for old reactive values no longer present in the new
    // `:if()` qualifiers
    for (let [reactive, [oldAttr, listener]] of curListeners) {
      let attr = reactiveIfs.get(reactive);
      if (oldAttr !== attr) {
        reactive.removeListener(listener);
        curListeners.delete(reactive);
        api.removeAttribute(this._elem, oldAttr);
      }
    }
    // Set listeners for new reactive values inside `:if()` qualifiers
    for (let [reactive, attr] of reactiveIfs) {
      let [oldAttr] = curListeners.get(reactive) || [];
      if (oldAttr !== attr) {
        curListeners.set(reactive, [attr, reactive.onInvalidate((r) => {
          if (r.value) {
            api.setAttribute(this._elem, attr, '');
          } else {
            api.removeAttribute(this._elem, attr);
          }
        })]);
        if (reactive.value) { // Set attribute initially, if necessary
          api.setAttribute(this._elem, attr, '');
        }
      }
    }
  }

  _updateReactiveBodyListeners(curListeners, reactiveBodies, css, api) {
    // Remove reactive body listeners when the new CSS no longer has such bodies
    for (let [reactive, listener] of curListeners) {
      if (!reactiveBodies.has(reactive)) {
        reactive.removeListener(listener);
        curListeners.delete(reactive);
      }
    }
    // Create listeners for the new reactive rule bodies
    for (let reactive of reactiveBodies) {
      if (!curListeners.has(reactive)) {
        curListeners.set(reactive, reactive.onInvalidate(() =>
          this.render(css, api))); // Rerender whole CSS
      }
    }
  }

  _updateState(css, api, o) {
    // Update the listeners on custom properties with units
    this._updateCustomPropListeners(this._withUnitsListeners,
      o.customProps.withUnits, (val) => stringifyValue('margin', val), api);
    // Update the unitless custom properties listeners
    this._updateCustomPropListeners(this._unitlessListeners,
      o.customProps.unitless, (val) => stringifyValue('z-index', val), api);
    // Update element attributes associated with non-reactive values in `:if()`s
    this._updateNonReactiveIfs(this._ifAttributes, o.nonReactiveIfs, api);
    // Update listeners associated with reactive values in `:if()` qualifiers
    this._updateIfListeners(this._ifListeners, o.reactiveIfs, api);
    // Update the listeners on reactive bodies
    this._updateReactiveBodyListeners(this._bodyListeners, o.reactiveBodies,
                                      css, api);
  }

  /**
   * Starting options used when transforming a CSS value into its string
   * representation.
   * @returns {Object} Options used when stringifying a CSS value
   */
  _startingOptions() {
    return {
      // Selectors that are parents of the rule being currently processed,
      // initially the "namespace" attribute selector unique to this renderer
      parentSelectors: [`[${CSS_ATTRIBUTE_PREFIX}${this._id}]`],
      // Reactive property values are turned into CSS variables. Multiple
      // propertiy values may have the same reactive value, in which case they
      // should be mapped to the same CSS variable **unless** one of the
      // properties is "unitless" and the other isn't. Example:
      // `var x = mut 1; var c = css {margin: x, zIndex: x}`; `margin` and
      // `zIndex` cannot share the same CSS variable since one must have value
      // `1px` while the other must have value `1`. Each of these `Map`s map the
      // reactive value to the custom property representing said reactive
      customProps: {withUnits: new Map(), unitless: new Map()},
      // Reactive values inside `:if()` qualifiers; map from the reactive value
      // to the attribute name that represents said qualifier
      reactiveIfs: new Map(),
      // Non-reactive values inside `:if()` qualifiers; map from the attribute
      // name representing the value to the value itself
      nonReactiveIfs: new Map(),
      counters: {
        // Counter of custom CSS properties, this makes sure each new custom
        // property has a unique identifier
        customProps: 0,
        // Counter of attribute qualifiers associated to each `:if()` qualifier
        ifs: 0
      },
      // Set of reactive body values
      reactiveBodies: new Set()
    };
  }

  render(css, api) {
    api.setAttribute(this._elem, `${CSS_ATTRIBUTE_PREFIX}${this._id}`, '');
    let o = this._startingOptions();
    let str = this._cssToString(css, o);
    this._updateState(css, api, o);
    // Update CSS text only if it changed
    if (api.getTextContent(this._styleElem) !== str) {
      api.setTextContent(this._styleElem, str);
    }
  }

  unrender() {
    // Remove listeners on reactive properties
    for (let [reactive, [, listener]] of this._withUnitsListeners) {
      reactive.removeListener(listener);
    }
    this._withUnitsListeners.clear();
    for (let [reactive, [, listener]] of this._unitlessListeners) {
      reactive.removeListener(listener);
    }
    this._unitlessListeners.clear();
    // Remove listeners on reactive values in `:if()` qualifiers
    for (let [reactive, [, listener]] of this._ifListeners) {
      reactive.removeListener(listener);
    }
    this._ifListeners.clear();
    // Remove listeners on reactive bodies
    for (let [reactive, listener] of this._bodyListeners) {
      reactive.removeListener(listener);
    }
    this._bodyListeners.clear();
  }

  /**
   * Returns the `<style>` element associated with the renderer.
   * @returns {HTMLStyleElement} `<style>` element where the CSS value is
   * rendered.
   */
  style() {
    return this._styleElem;
  }
}

// Exported class
module.exports = Renderer;

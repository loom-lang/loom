/**
 * Testing of arrays.
 */

let {eq, eqArray, ok, run, testSyntax} = require('./utils');

suite('Arrays');

// Syntax
test('syntax', function() {
  let pass = ['[]', '[ ]', '[0]', '[ 0 ]', '[0,1]', '[ 0 , 1 ]', '[0,]',
              '[ 0 , ]', '[...[]]', '[ ... [ ] ]', '[ ,, , ,]', '[[],[[]]]',
              `[0 // Comment in automatic comma insertion
                ...[1]
                2]`];
  testSyntax(pass);
});

// Accessing elements
test('elements access', function() {
  eq(run('var a = [1, [2, 3], 4]; a[0] +  a [ 1 ] [ 0 ] + a[1][1] + a[2]'), 10);
});

// Trailing comma supported and doesn't count as a new element
test('trailing comma', function() {
  let a = run('[0,]');
  ok(a.length === 1);
});

// Support array elision
test('elision', function() {
  eqArray(run('[0,,2,]'), [0, undefined, 2]);
});

// Support the spread operator on arrays
test('spread operator', function() {
  eqArray(run('var a = [1, 2]; [...a, 3]'), [1, 2, 3]);
});

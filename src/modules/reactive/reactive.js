/**
 * Reactive is the class common to both mutables and signals.
 */
class Reactive {
  /**
   * Constructor of a reactive value.
   */
  constructor() {
    // Signals subscribed to this reactive value
    this._subscribers = new Set();
    // Functions to run whenever the value is invalidated; a map from a Symbol
    // to the function to run
    this._onInvalidate = new Map();
  }

  /**
   * Returns the value associated to the reactive value while adding the signal
   * that accessed it as a subscriber.
   * @param {Signal} signal The given signal will become subscribed to this
   *   reactive value, invalidating whenever the reactive value is invalidated
   * @returns {any} The reactive value's associated value
   */
  subscribe(signal) {
    // Only set dependencies when the signal's expression is currently being
    // computed
    if (signal._isComputing()) {
      if (this === signal) {
        throw new Error('Reactive.subscribe: cannot add self as a dependency');
      }
      this._subscribers.add(signal);
      signal._addSubscription(this);
    }
    return this.value;
  }

  /**
   * Adds a listener that runs whenever the reactive value is invalidated.
   * @param {Function} fun Function to run on invalidate, receives the
   *   invalidated reactive value as an argument
   * @returns {Symbol} Symbol that represents the listener, used to stop
   *   listening to `onInvalidate` events by calling `removeListener`
   */
  onInvalidate(fun) {
    let sym = Symbol(fun.name);
    this._onInvalidate.set(sym, fun);
    return sym;
  }

  /**
   * Stop listening to a specific `onInvalidate` event.
   * @param {Symbol} sym Symbol returned by the `onInvalidate` function that
   *   represents the listener to remove
   * @returns {void}
   */
  removeListener(sym) {
    this._onInvalidate.delete(sym);
  }

  /**
   * Remove a subscriber from the list of subscribers.
   * @param {Signal} signal Signal to remove as a subscriber
   * @returns {void}
   */
  _removeSubscriber(signal) {
    this._subscribers.delete(signal);
  }
}

// Exported class
module.exports = Reactive;

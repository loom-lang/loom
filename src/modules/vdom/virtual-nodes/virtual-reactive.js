// Dependencies
let VirtualNode = require('./virtual-node');
let {nodeify} = require('./utils');
let {patch} = require('../patch');

/**
 * Virtual node that contains a reactive value. It is used to handle children of
 * virtual nodes that are reactive. This class acts as a container for the
 * virtual node the reactive value produces; if the reactive value doesn't
 * produce a virtual node, a virtual text node is created from the string
 * obtained from such value.
 */
class VirtualReactive extends VirtualNode {
  /**
   * Virtual reactive node's constructor.
   * @param {Reactive} reactive Reactive value
   */
  constructor(reactive) {
    super();
    // Reactive value that caused the existence of this virtual node
    this._reactive = reactive;
    // Virtual node obtained from the value of the reactive value
    this._vNode = null;
    // Listener associated with the reactive value
    this._listener = null;
    // Namespace of the parent element that should be passed to the child
    // virtual node obtained from the reactive
    this._namespace = null;
  }

  /**
   * Method to run before this virtual node is used in the patching algorithm.
   * This allows virtual elements to be lazy, in the sense that the reactive
   * value is only accessed when the node is about to be rendered.
   * @param {string?} parentNamespace Namespace obtained from the parent
   *   element
   * @returns {void}
   */
  beforeRender(parentNamespace) {
    // Store the parent namespace to be able to pass to the reactive-generated
    // virtual node whenever the reactive changes
    this._namespace = parentNamespace;
    // Make sure the reactive's value is a virtual node (`nodeify` will
    // transform non-`VirtualNode`s into a `VirtualText`)
    let vNode = this._vNode = nodeify(this._reactive.value);
    // Call `beforeRender` on the virtual node since it is about to be rendered
    vNode.beforeRender && vNode.beforeRender(parentNamespace);
  }

  /**
   * Method called when an actual DOM element is associated with this virtual
   * node. Associate a listener with this node's reactive value and update the
   * DOM whenever it changes.
   * @param {Object} hooks Hooks to call during patching
   * @param {Object} api API to interact with the DOM
   * @returns {void}
   */
  onRender(hooks, api) {
    // Create a listener on the node to update the DOM when the reactive changes
    this._listener = this._reactive.onInvalidate((reactive) => {
      let oldVNode = this._vNode; // Old virtual node
      let vNode = this._vNode = nodeify(reactive.value); // New virtual node
      // The new virtual node is about to be rendered, so call its
      // `beforeRender` method
      vNode.beforeRender && vNode.beforeRender(this._namespace);
      // Patch the new virtual node against the old one
      patch(oldVNode, vNode, hooks, api);
    });
    // Call `onRender` on the virtual node obtained from the reactive value
    this._vNode.onRender && this._vNode.onRender(hooks, api);
  }

  /**
   * Method called when the DOM node is disassociated from this virtual node.
   * Used to remove the previously associated listener.
   * @param {Object} hooks Hooks to call during patching
   * @param {Object} api API to interact with the DOM
   * @returns {void}
   */
  onUnrender(hooks, api) {
    // Remove the listener on the reactive value
    this._reactive.removeListener(this._listener);
    this._listener = null;
    // Call `onUnrender` on the virtual node obtained from the reactive value
    this._vNode.onUnrender && this._vNode.onUnrender(hooks, api);
  }
}

// Methods implemented by virtual text and element nodes
const METHODS = ['type', 'text', 'tag', 'id', 'classes', 'originalAttributes',
                 'originalChildren', 'attributes', 'children', 'key',
                 'namespace', 'getNode', 'setNode'];

// Add the methods to `VirtualReactive`, each method simply calls the method
// defined in `this._vNode`
for (let method of METHODS) {
  VirtualReactive.prototype[method] = function() {
    let vNode = this._vNode;
    return vNode[method].apply(vNode, arguments);
  };
}

// Exported class
module.exports = VirtualReactive;

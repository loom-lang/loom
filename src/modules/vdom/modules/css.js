/**
 * This module handles the updating of the CSS of a DOM element. Reactive CSS
 * values, as well as a (possibly reactive) array of CSS or reactive CSS values,
 * are supported.
 */

// Dependencies
let SimpleSelector = require('../../css/selectors/simple-selector');
let CSS = require('../../css/css');
let Reactive = require('../../reactive/reactive');
let Renderer = require('../../css/renderer');
let {associateListener, disassociateListener} = require('./utils');

/**
 * Property to add to a DOM element to keep track of the renderer responsible
 * for its CSS.
 */
const ELEM_RENDERER_PROPERTY = '_renderer';

/**
 * Inserts a `<style scoped>` element as the first child of the given element.
 * @param {Element} elem Element where to insert the style element
 * @param {Object} api API to interact with the DOM and DOM elements
 * @returns {HTMLStyleElement} Inserted element
 */
function insertStyle(elem, api) {
  let style = api.createElement('style');
  api.setAttribute(style, 'scoped', '');
  api.insertBefore(elem, style, api.firstChild(elem));
  return style;
}

/**
 * Removes the `<style>` element from the given element.
 * @param {Element} elem Element from where to remove the style element
 * @param {Object} api API to interact with the DOM and DOM elements
 * @returns {void}
 */
function removeStyle(elem, style, api) {
  api.removeChild(elem, style);
}

/**
 * Normalises value of the non-reactive `css` property of the element.
 * @param {CSS | Array<CSS | Reactive<CSS>>} value Value of the `css` property
 *   of the element (non-reactive)
 * @returns {CSS} Normalised CSS value
 */
function normalizeCSS(value) {
  // Support an array of CSS values
  if (Array.isArray(value)) {
    // Transform an array of `CSS` or `Reactive<CSS>` values into a single `CSS`
    // value: (e.g. `[css {color: "red"}, css {background: "blue"}]` becomes:
    // `css {|&|: {color: "red"}, |&|: {background: "blue"}}`)
    let entries = [];
    for (let css of value) {
      entries.push({kind: 'rule',
        selectors: [new SimpleSelector({kind: 'parent'})], body: css});
    }
    return new CSS(...entries);
  }
  return value;
}

/**
 * Adds a listener on a reactive CSS value.
 * @param {Element} elem DOM element where listener will be stored
 * @param {Reactive} reactive Reactive value to listen to
 * @returns {void}
 */
function addReactiveListener(elem, reactive) {
  associateListener(elem, 'attribute', 'css', reactive.onInvalidate(
    (r) => elem[ELEM_RENDERER_PROPERTY].updateCSS(normalizeCSS(r.value))));
}

/**
 * Removes the listener on the reactive CSS value.
 * @param {Element} elem Element where listener was stored
 * @param {Reactive} reactive Reactive value to stop listening to
 * @returns {void}
 */
function removeReactiveListener(elem, reactive) {
  reactive.removeListener(disassociateListener(elem, 'attribute', 'css'));
}

/**
 * Updates the CSS of a DOM element given its old and new `css` attribute.
 * @param {Element} elem Element on which CSS is to be applied
 * @param {Object} oldCss Old CSS of the element
 * @param {Object} css New CSS to apply to the element
 * @param {Object} api API to interact with a DOM element
 * @returns {void}
 */
function updateCSS(elem, oldCss, css, api) {
  if (oldCss === css) { // Do nothing if the CSS value didn't change
    return;
  }

  // Remove old CSS
  if (oldCss != null) {
    let renderer = elem[ELEM_RENDERER_PROPERTY];
    // Remove listener from old reactive CSS
    if (oldCss instanceof Reactive) {
      removeReactiveListener(elem, oldCss);
    }
    // Call `unrender` on the renderer only when there is no new CSS
    if (css == null) {
      removeStyle(elem, renderer.style(), api);
      renderer.unrender(api);
    }
  }

  // Add/update new CSS
  if (css != null) {
    if (css instanceof Reactive) {
      addReactiveListener(elem, css);
      css = css.value;
    }
    let normalizedCss = normalizeCSS(css);
    let renderer;
    if (oldCss == null) { // New CSS
      // Insert a `<style scoped>` element
      let style = insertStyle(elem, api);
      // Create a new renderer and render the style
      renderer = elem[ELEM_RENDERER_PROPERTY] = new Renderer(elem, style);
    } else { // Update CSS
      renderer = elem[ELEM_RENDERER_PROPERTY];
    }
    renderer.render(normalizedCss, api);
  }
}

// Exported module functions
module.exports = {
  create: (elem, attrs, api) => updateCSS(elem, null, attrs.css || null, api),
  update: (elem, oldAttrs, attrs, api) =>
    updateCSS(elem, oldAttrs.css || null, attrs.css || null, api),
  destroy: (elem, attrs, api) => updateCSS(elem, attrs.css || null, null, api)
};

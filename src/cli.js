/**
 * Module responsible for handling the CLI.
 */

// Dependencies
let fs = require('fs');
let yargs = require('yargs'); // Options parser
let {inspect, resolveBabelPlugin} = require('./utils');
let {VERSION, parse, compile, run, register} = require('./main');

// CLI parsing =================================================================

let argv = yargs
  .usage('Usage: $0 [options] [files]') // Message on how to use the CLI
  .strict(true) // Do not allow unknown options
  // List of options
  .option('c', {alias: 'compile', type: 'boolean',
                desc: 'output compiled code'})
  .option('e', {alias: 'eval', type: 'boolean', desc: 'evaluate compiled code'})
  .option('h', {alias: 'help'})
    // Display built-in help message
    .help('h', 'display help message', false)
    // Do not display the help message when an error occurs
    .showHelpOnFail(false, 'Use --help to list all available options.')
  .option('i', {alias: 'input', type: 'string', normalize: true,
                desc: 'file used as input instead of STDIN'})
  .option('o', {alias: 'output', type: 'string', normalize: true,
                desc: 'file used for output instead of STDOUT'})
  .option('p', {alias: 'parse', type: 'boolean',
                desc: 'output parsed Loom AST'})
  .option('v', {alias: 'version'})
    // Use built-in version option
    .version('v', 'display version number', `Loom version ${VERSION}`)
  .option('cli', {type: 'string',
                  desc: 'pass string from the command line as input'})
  .option('js-ast', {type: 'boolean',
                     desc: 'output compiled JavaScript AST (Babylon)'})
  .option('repl', {type: 'boolean',
                   desc: 'run an interactive Loom REPL'})
  .option('babel-presets',
          {type: 'array', desc: 'specify Babel presets to use in compilation'})
  .option('babel-plugins',
          {type: 'array', desc: 'specify Babel plugins to use in compilation'})
  // Show at the end of the help menu
  .epilog(
`When none of --{cli,input,output} are given, Loom operates on STDIN/STDOUT.
When none of --{compile,eval,js-ast,parse,repl} are given:
  If an input source is given:
    * --eval is implied
    * positional arguments are passed as arguments to the script
  Otherwise if positional arguments were given:
    * --eval is implied
    * the first positional argument is the input filename
    * additional positional arguments are passed as arguments to the script
  Otherwise --repl is implied`
  )
  // Validate options
  .check((argv) => {
    /**
     * Counts the number of options in the `options` array being used.
     * @param {Array} options Array with options to count
     * @returns {number} Number of options used
     */
    function usedOptions(options) {
      return options.reduce((sum, opt) => {
        let val = argv[opt];
        return sum + (typeof val === 'string' ||
                      typeof val === 'boolean' && val ||
                      Array.isArray(val) && val.length > 0);
      }, 0);
    }

    // Mutually exclusive options
    let mutuallyExclusive = [
      ['compile', 'eval', 'js-ast', 'parse', 'repl'],
      ['cli', 'input', 'repl'],
      ['output', 'repl']
    ];
    for (let options of mutuallyExclusive) {
      if (usedOptions(options) > 1) {
        throw new Error(`Only one of --{${options.join()}} may be used.`);
      }
    }

    // Dependencies
    let dependencies = {
      'babel-presets': ['compile', 'eval', 'js-ast'],
      'babel-plugins': ['compile', 'eval', 'js-ast']
    };
    for (let option in dependencies) {
      // If option set, one of the dependencies must be too
      if (usedOptions([option]) === 1) {
        let options = dependencies[option];
        if (usedOptions(options) === 0) {
          throw new Error(
            `--${option} requires one of --{${options.join()}} to be set.`);
        }
      }
    }

    // Resolve Babel presets/plugins (this will throw when the modules aren't
    // installed)
    argv.babelPresets = argv['babel-presets'] = argv.babelPresets == null ? [] :
      argv.babelPresets.map((preset) => resolveBabelPlugin(preset, true));
    argv.babelPlugins = argv['babel-plugins'] = argv.babelPlugins == null ? [] :
      argv.babelPlugins.map((plugin) => resolveBabelPlugin(plugin, false));

    // Logic described in the epilogue
    if (!argv.compile && !argv.eval && !argv.jsAst && !argv.parse &&
        !argv.repl) {
      if (argv.cli != null || argv.input != null) {
        argv.eval = true;
      } else if (argv._.length > 0) {
        argv.eval = true;
        argv.input = argv._.shift();
      } else {
        argv.repl = true;
      }
    }
    return true;
  })
  .argv;

// CLI logic ===================================================================

if (argv.repl) { // Start REPL
  require('./repl/repl')();
} else if (argv.input != null) { // If input comes from file
  fs.readFile(argv.input, (err, content) => {
    if (err) {
      throw err;
    }
    processInput(content.toString(), fs.realpathSync(argv.input));
  });
} else if (argv.cli != null) { // If input comes from CLI
  processInput(argv.cli, '[cli]');
} else { // If input comes from `stdin`
  let input = '';
  process.stdin.on('data', (data) => input += data);
  process.stdin.on('end', () => processInput(input, '[stdin]'));
  process.stdin.setEncoding('utf8');
  process.stdin.resume();
}

/**
 * Processes the input source code according to the options.
 * @param {string} input Input source code to process
 * @param {string} filename Name of file from where input is coming (or
 *   something such as `'[cli]'`)
 * @returns {void}
 */
function processInput(input, filename) {
  // Strip UTF BOM
  if (input.charCodeAt(0) === 0xFEFF) {
    input = input.slice(1);
  }

  // Parse input
  let parsedAst;
  try {
    parsedAst = parse(input, {filename});
  } catch(err) {
    console.error(err.toString());
    process.exit(1);
  }

  // `--parse` option
  if (argv.parse) {
    return writeOutput(inspect(parsedAst.toObject()));
  }

  // Compile parsed AST
  let compiled;
  try {
    compiled = compile(parsedAst, {filename, jsAst: argv.jsAst,
      js: !argv.jsAst, presets: argv.babelPresets, plugins: argv.babelPlugins});
  } catch(err) {
    console.error(err.toString());
    process.exit(1);
  }

  // `--js-ast` option
  if (argv.jsAst) {
    return writeOutput(inspect(compiled.jsAst));
  }

  // `--compile` option
  if (argv.compile) {
    return writeOutput(compiled.js);
  }

  // `--eval` option
  if (argv.eval) {
    register(); // Register Loom to become able to import `.loom` files
    process.argv = [process.argv[1], filename].concat(argv._);
    run(compiled.js, {filename});
  }
}

/**
 * Writes string to output stream (either a specified file or `stdout`).
 * @param {string} output String to write
 * @returns {void}
 */
function writeOutput(output) {
  output += '\n'; // Append newline to output string
  if (argv.output) { // Write to file
    fs.writeFile(argv.output, output, (err) => {
      if (err) {
        throw err;
      }
    });
  } else { // Write to `stdout`
    process.stdout.write(output);
  }
}

// Regular expressions used to hyphenate properties
const UPPERCASE_RE = /[A-Z]/g;
const MS_RE = /^ms-/;

// Cache of properties already hyphenated
let hyphenatedCache = new Map();

/**
 * Transforms a CSS property into its hyphenated version.
 * @param {string} prop CSS property to hyphenate
 * @returns {string} Hyphenated CSS property
 */
function hyphenate(prop) {
  let hyphenated = hyphenatedCache.get(prop);
  if (hyphenated == null) {
    hyphenatedCache.set(prop, hyphenated =
      prop.replace(UPPERCASE_RE, '-$&').toLowerCase().replace(MS_RE, '-ms-'));
  }
  return hyphenated;
}

/**
 * CSS properties which accept numbers but are not in units of "px".
 * Taken from React's `CSSProperty.js`
 */
const UNITLESS_PROPERTIES = new Set(['animation-iteration-count',
  'border-image-outset', 'border-image-slice', 'border-image-width', 'box-flex',
  'box-flex-group', 'box-ordinal-group', 'column-count', 'flex', 'flex-grow',
  'flex-positive', 'flex-shrink', 'flex-negative', 'flex-order', 'grid-row',
  'grid-column', 'font-weight', 'line-clamp', 'line-height', 'opacity', 'order',
  'orphans', 'tab-size', 'widows', 'z-index', 'zoom',
  // SVG-related ---------------------------------------------------------------
  'fill-opacity', 'flood-opacity', 'stop-opacity', 'stroke-dasharray',
  'stroke-dashoffset', 'stroke-miterlimit', 'stroke-opacity', 'stroke-width'
]);

// Add prefixed version of unitless properties to the set
for (let property of [...UNITLESS_PROPERTIES]) {
  for (let prefix of ['-webkit-', '-ms-', '-moz-', '-o-']) {
    UNITLESS_PROPERTIES.add(prefix + property);
  }
}

/**
 * Whether a given property accept numbers but not in units of "px".
 * @param {string} prop (Hyphenated) property name
 * @returns {boolean} Whether property is unitless
 */
function isUnitless(prop) {
  return UNITLESS_PROPERTIES.has(prop);
}

/**
 * Transforms the value of a CSS property into a string.
 * @param {string} prop (Hyphenated) property name
 * @param {any} value Value of the property
 * @returns {string} Stringified value
 */
function stringifyValue(prop, value) {
  if (Array.isArray(value)) {
    // Arrays are joined with spaces, unless they are nested, in which case they
    // are joined with commas
    return value.map((inner) => stringifyValue(prop, inner, true))
                .join(Array.isArray(value[0]) ? ',' : ' ');
  } if (typeof value === 'number' && !isUnitless(prop)) {
    // Append `'px'` to number values that are supposed to have units (no need
    // to add `'px'` to `0`)
    return value === 0 ? '0' : `${value}px`;
  }
  return String(value);
}

// Exported functions
module.exports = {hyphenate, isUnitless, stringifyValue};

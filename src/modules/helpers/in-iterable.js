/**
 * Whether a given element is a member of a given iterable collection.
 * @param {any} element Element to check
 * @param {Iterable} iterable Collection where to check for element
 * @returns {boolean} Whether element is a member of the collection
 */
function inIterable(element, iterable) {
  for (let e of iterable) {
    if (e === element) {
      return true;
    }
  }
  return false;
}

// Export function
module.exports = inIterable;

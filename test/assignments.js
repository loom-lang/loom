/**
 * Testing of assignments.
 */

let {eq, run} = require('./utils');

suite('Assignments');

// Normal assignment
test('normal assignment', function() {
  eq(run('var x = 1; x = 2; x=3'), 3);
});

// Arithmetic assignments
test('arithmetic', function() {
  eq(run('var x = 1; x += 1; x+=1'), 3);
  eq(run('var x = 1; x -= 1; x-=1'), -1);
  eq(run('var x = 2; x *= 2; x*=2'), 8);
  eq(run('var x = 4; x /= 2; x/=2'), 1);
  eq(run('var x = 4; x %= 3; x%=2'), 1);
  eq(run('var x = 2; x ^^= 2; x^^=2'), 16);
});

// Bit assignments
test('bit', function() {
  eq(run('var x = 10; x |= 3; x|=4'), 15);
  eq(run('var x = 10; x &= 3; x&=6'), 2);
  eq(run('var x = 10; x ^= 3; x^=1'), 8);
  eq(run('var x = 10; x <<= 3; x<<=1'), 160);
  eq(run('var x = -1; x >>= 1; x>>=1'), -1);
  eq(run('var x = -1; x >>>= 1; x>>>=1'), 1073741823);
});

// Logical assignments
test('logical', function() {
  eq(run('var x = true; x ||= false; x||=false'), true);
  eq(run('var x = true; x &&= false; x&&=false'), false);
});

// Assignments with member expressions
test('member', function() {
  eq(run(`var x = {a: [{b: 0}], c: 1}
          x.c = 2
          x.a[0].b = 3
          x.c + x.a[0].b`), 5);
  eq(run(`var x = {a: 0}, f = () -> ({b: () -> () -> [x]})
          do {var g = f; g}().b()()[0].a = 1
          x.a`), 1);
});

// Destructuring assignments
test('destructuring', function() {
  eq(run(`var a, b, c, x = [1, 2], y = [3];
          [a, b] = x;
          [c] = y;
          a + b + c`), 6);
  eq(run(`var a, b, c, x = {x: 1, y: 2}, y = {c: 3}
          {x: a, y: b} = x
          {c} = y
          a + b + c`), 6);
  eq(run('var a, x = [1, 2, 3]; [,, a] = x; a'), 3);
  eq(run('var a, b, x = {x: 1, y: 2}; {"x": a, ["y"]: b} = x; a + b'), 3);
  eq(run(`var a, b, c, d, x = [{a: 1, b: [2, {c: 3}]}, [4]];
          [{a, b: [b, {c}]}, [d]] = x;
          a + b + c + d
         `), 10);
  eq(run('var a, b, x = [1, 2, 3]; [a, ...b] = x; a + b[0] + b[1]'), 6);
  eq(run(`var a, b, x = {x: 1, y: 2, z: 3}
          {x: a, ...b} = x
          a + b.y + b.z`), 6);
  eq(run(`var a, b, c, x = [, [3]];
          [a = 1, [b = 2, c = 3]] = x;
          a + b + c`), 7);
  eq(run(`var a, b, c, x = {x: 3}
          {a = 1, x: b = 2, "c\${0}": c = 3} = x
          a + b + c`), 7);
  eq(run(`var x = {a: [{b: 0}], c: 1};
          [x.a[0].b, x.c] = [2, 3];
          x.a[0].b + x.c`), 5);
  eq(run(`var x = {a: 0}, f = () -> ({b: () -> () -> [x]});
          [do {var g = f; g}().b()()[0].a] = [1];
          x.a`), 1);
});

// Assigning to mutables
test('dereference assignment', function() {
  eq(run('var x = mut 0; *x = 1; *x'), 1);
  eq(run('var x = mut 1, y = mut 2; [*x, *y] = [3, 4]; *x + *y'), 7);
  eq(run(`var x = mut 1, y = mut 2
         {a: *x, b: *y} = {a: 3, b: 4}
         *x + *y`), 7);
});

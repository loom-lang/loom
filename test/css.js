/**
 * Testing of CSS.
 */

let {testSyntax} = require('./utils');

suite('CSS');

// Basic syntax (support for properties and imports)
test('basic syntax', function() {
  let pass = ['css{}', 'css { }', 'css{x: 0}', 'css {"x": 0}', 'css{["x"]: 0}',
    'css{x:0,y:0}', 'css { x : 0 , y : 0 }', 'css {x: 0,}', 'css{...css{x:0}}',
    'css { ... css { x : 0 } }',
    `css {
      x: 0 // Comment in automatic comma insertion
      ...css {y: 0}
      z: 0
     }`];
  testSyntax(pass);
});

// Test some basic selector (tag, universal, id, class)
test('basic selector', function() {
  let pass = ['css {|*| {}}', 'css {|div| {}}', 'css {|#i| {}}',
    'css {|.c| {}}', 'css {|div#i.a.b, *#i.a.b, div.a#i.b, *.a#i.b| {}}'];
  testSyntax(pass);
});

// Test selector combinators
test('combinator', function() {
  let pass = ['css {|i a| {}}', 'css {|i>a, i > a| {}}',
    'css {|i+a, i + a| {}}', 'css {|i~a, i ~ a| {}}',
    'css {|a#i.j.k#l b > c + d ~ e| {}}'];
  testSyntax(pass);
});

// Parent (`&`) selector
test('parent selector', function() {
  let pass = ['css {|&| {}}', 'css {|&#x.a| {}}', 'css {|&:hover| {}}',
    'css {|& > div| {}}', 'css {|&.x + div span| {}}'];
  // Parent selector can only appear at the beginning of composite selectors
  let fail = ['css {|i &| {}}', 'css {|i > &| {}}', 'css {|i > a &| {}}'];
  testSyntax(pass, fail);
});

// Syntax of the `if` pseudo-class
test(':if() selector', function() {
  let pass = ['css {|:if(true), :if( true )| {}}',
    'css {|i#x:if(true):hover| {}}', 'var x = true; css {|:if(x)| {}}'];
  testSyntax(pass);
});

// Syntax of attribute selectors
test('attribute selector', function() {
  let pass = ['css {|[href], [ href ]| {}}',
    'css {|[rel="x"], [ rel = "x" ]| {}}',
    'css {|[rel~="x"], [ rel ~= "x" ]| {}}',
    'css {|[rel|="x"], [ rel |= "x" ]| {}}',
    'css {|[rel^="x"], [ rel ^= "x" ]| {}}',
    'css {|[rel$="x"], [ rel $= "x" ]| {}}',
    'css {|[rel*="x"], [ rel *= "x" ]| {}}',
    // Support for arbitrary expression
    'var x = "x"; css {|[rel*=x], [ rel *= x ]| {}}'];
  testSyntax(pass);
});

// Syntax of pseudo-class selectors
test('pseudo-class selector', function() {
  let pass = ['css {|hover| {}}',
    'css {|:nth-child("odd"), :nth-child( "odd" )| {}}',
    'css {|div:hover:nth-child("odd"):nth-of-type("even")| {}}',
    // Support for arbitrary expression
    'var n = 2; css {|:nth-child(n), :nth-child( n )| {}}'];
  testSyntax(pass);
});

// Syntax of pseudo-element selectors
test('pseudo-element selector', function() {
  let pass = ['css {|:after| {}}', 'css {|div::after, #i::after| {}}',
    'css {|:function("arg"), ::function( "arg" )| {}}',
    // Support for arbitrary expression
    'var x = "arg"; css {|:function(x), ::function( x )| {}}'];
  // Pseudo-elements must be the last qualifier of a selector
  let fail = ['css {|div::after:hover| {}}'];
  testSyntax(pass, fail);
});

// Syntax of `:not` pseudo-class
test(':not() selector', function() {
  let pass = ['css {|:not(div), :not( div )| {}}',
    'css {|:not(*), :not( * )| {}}', 'css {|:not(#i), :not( #i )| {}}',
    'css {|:not(.c), :not( .c )| {}}',
    'css {|:not([false]), :not( [false] )| {}}',
    'css {|:not([href]), :not( [href] )| {}}',
    'css {|:not(:hover), :not( :hover )| {}}',
    'css {|:not(::placeholder), :not( ::placeholder )| {}}'];
  // Only simple selectors are accepted
  let fail = ['css {|:not(#x.c)| {}}', 'css {|:not(:not(div))| {}}',
    'css {|:not(div > span)| {}}', 'css {|:not(&)| {}}'];
  testSyntax(pass, fail);
});

/**
 * Virtual DOM patching algorithm based on (MIT licensed)
 * [Snabbdom](https://github.com/snabbdom/snabbdom).
 *
 * Besides stylistic changes, the following was changed from Snabbdom's original
 * algorithm:
 * - The notion of modules was removed. Instead, and to simplify the algorithm,
 *   a single `hooks` object must be passed with `onCreate`, `onInsert`,
 *   `onUpdate`, `onDestroy`, and `onRemove` defined. Note that the notion of
 *   modules may still exist, just not in here;
 * - The `onRemove` hook expects a promise to delay the removal of the DOM
 *   element instead of using a callback;
 * - The patching function can only receive virtual nodes and the "old" virtual
 *   node passed to the function must contain a DOM node. This means that the
 *   transformation of a given DOM node to its equivalent virtual representation
 *   must be done before using `patch`;
 * - Virtual nodes are interacted with through methods they must define, as
 *   opposed to the original algorithm where properties are used. This provides
 *   some extra flexibility;
 * - There is no handling of the virtual node's hooks. Note that it can still be
 *   done by using the hooks appropriately;
 * - There is no `init` function, `patch` is used directly by passing it the
 *   hooks and DOM API.
 */

// Dependencies
let {ELEMENT_NODE, TEXT_NODE} = require('./node-types');

/**
 * Whether two virtual nodes may be patched against one another.
 * @param {VirtualNode} vNode1 First virtual node to compare
 * @param {VirtualNode} vNode2 Second virtual node to compare
 * @returns {boolean} Whether the two nodes are considered the same
 */
function patchable(vNode1, vNode2) {
  return vNode1.type() === vNode2.type() &&
    (vNode1.type() !== ELEMENT_NODE || vNode1.tag() === vNode2.tag() &&
                                       vNode1.key() === vNode2.key());
}

/**
 * Creates a mapping from each child's key to its index in the array.
 * @param {Array<VirtualNode>} children Array of virtual children
 * @param {number} startIdx Index from witch to start building map
 * @param {number} endIdx Index until witch to build map
 * @returns {Object} Mapping from each child's key to its index
 */
function keyToIdxMap(children, startIdx, endIdx) {
  let map = {};
  for (; startIdx <= endIdx; ++startIdx) {
    let child = children[startIdx];
    if (child.type() === ELEMENT_NODE) {
      let key = child.key();
      if (key != null) {
        map[key] = startIdx;
      }
    }
  }
  return map;
}

/**
 * Creates an actual DOM node, provided its virtual representation. The created
 * DOM node is set into the provided virtual node.
 * @param {VirtualNode} vNode Virtual representation of the node to create
 * @param {Array<VirtualNode>} insertedVNodes Virtual nodes with newly inserted
 *   DOM elements
 * @param {Object} hooks Hooks to call during patching
 * @param {Object} api API to interact with the DOM
 * @returns {Node} Actual DOM node
 */
function createNode(vNode, insertedVNodes, hooks, api) {
  insertedVNodes.push(vNode); // Mark node as inserted

  let domNode, r; // `r` is a helper variable to pass around references
  if (vNode.type() === TEXT_NODE) { // Create a DOM text node
    domNode = api.createTextNode(vNode.text());
    vNode.setNode(domNode);
  } else { // Create a DOM element
    let tag = vNode.tag();
    domNode = (r = vNode.namespace()) != null ? api.createElementNS(r, tag) :
                                                api.createElement(tag);
    vNode.setNode(domNode);
    // Append children
    for (let child of vNode.children()) {
      api.appendChild(domNode, createNode(child, insertedVNodes, hooks, api));
    }
  }
  // Call the `onCreate` hook with the virtual node
  hooks.onCreate(vNode);

  return domNode;
}

/**
 * Removes nodes in `vNodes` between `startIdx` and `endIdx` from the DOM.
 * @param {Element} parentElem Parent element of all nodes to remove
 * @param {Array<VirtualNode>} vNodes Virtual nodes to remove from the DOM
 * @param {number} startIdx Index from which to remove from `vNodes`
 * @param {number} endIdx Index until which to remove from `vNodes`
 * @param {Object} hooks Hooks to call during patching
 * @param {Object} api API to interact with the DOM
 * @returns {void}
 */
function removeVNodes(parentElem, vNodes, startIdx, endIdx, hooks, api) {
  /**
   * Invokes the `onDestroy` hook with `vNode` and all of its children.
   * @param {VirtualNode} vNode Virtual node with which to call `onDestroy`
   * @returns {void}
   */
  function callOnDestroyHook(vNode) {
    // Call the `onDestroy` hook with the virtual node
    hooks.onDestroy(vNode);
    if (vNode.type() === ELEMENT_NODE) {
      for (let child of vNode.children()) { // Call on children
        callOnDestroyHook(child);
      }
    }
  }

  // Remove nodes from `startIdx` to `endIdx`
  for (; startIdx <= endIdx; ++startIdx) {
    let vNode = vNodes[startIdx];
    if (vNode != null) { // It can be `null` thanks to `updateChildren`
      // Call the `onDestroy` hook with the virtual node and all of its children
      callOnDestroyHook(vNode);
      // Function to remove the DOM node
      let rm = () => api.removeChild(parentElem, vNode.getNode());
      // Call the `onRemove` hook with the virtual node being removed, the hook
      // might return a promise, in which case the DOM node may only be removed
      // from the DOM after all such promises are resolved
      let result = hooks.onRemove(vNode);
      if (result instanceof Promise) {
        result.then(rm, rm);
      } else { // When there is no promise involved, remove immediately
        rm();
      }
    }
  }
}

/**
 * Algorithm to update the children of an element.
 * @param {Element} parentElem Parent DOM element that contains the children
 * @param {Array<VirtualNode>} oldChildren Children being updated
 * @param {Array<VirtualNode>} children Children that should belong to the node
 * @param {Array<VirtualNode>} insertedVNodes Virtual nodes with newly inserted
 *   DOM elements
 * @param {Object} hooks Hooks to call during patching
 * @param {Object} api API to interact with the DOM
 * @returns {void}
 */
function updateChildren(parentElem, oldChildren, children, insertedVNodes,
                        hooks, api) {
  let oldStartIdx = 0;
  let oldEndIdx = oldChildren.length - 1;
  let oldStartVNode = oldChildren[0];
  let oldEndVNode = oldChildren[oldEndIdx];
  let startIdx = 0;
  let endIdx = children.length - 1;
  let startVNode = children[0];
  let endVNode = children[endIdx];

  // Mapping from keys of the old children to their respective indices in the
  // `oldChildren` array (computed when needed for the first time)
  let oldKeyToIdxMap;

  while (oldStartIdx <= oldEndIdx && startIdx <= endIdx) {
    if (oldStartVNode == null) { // VNode has been moved left
      oldStartVNode = oldChildren[++oldStartIdx];
    } else if (oldEndVNode == null) {
      oldEndVNode = oldChildren[--oldEndIdx];
    } else if (patchable(oldStartVNode, startVNode)) {
      patchVNode(oldStartVNode, startVNode, insertedVNodes, hooks, api);
      oldStartVNode = oldChildren[++oldStartIdx];
      startVNode = children[++startIdx];
    } else if (patchable(oldEndVNode, endVNode)) {
      patchVNode(oldEndVNode, endVNode, insertedVNodes, hooks, api);
      oldEndVNode = oldChildren[--oldEndIdx];
      endVNode = children[--endIdx];
    } else if (patchable(oldStartVNode, endVNode)) { // VNode moved right
      patchVNode(oldStartVNode, endVNode, insertedVNodes, hooks, api);
      api.insertBefore(parentElem, endVNode.getNode(),
                       api.nextSibling(oldEndVNode.getNode()));
      oldStartVNode = oldChildren[++oldStartIdx];
      endVNode = children[--endIdx];
    } else if (patchable(oldEndVNode, startVNode)) { // VNode moved left
      patchVNode(oldEndVNode, startVNode, insertedVNodes, hooks, api);
      api.insertBefore(parentElem, startVNode.getNode(),
                       oldStartVNode.getNode());
      oldEndVNode = oldChildren[--oldEndIdx];
      startVNode = children[++startIdx];
    } else {
      let startKey, idxInOld;
      if (startVNode.type() === ELEMENT_NODE) {
        if (oldKeyToIdxMap == null) { // Compute the map when it is first needed
          oldKeyToIdxMap = keyToIdxMap(oldChildren, oldStartIdx, oldEndIdx);
        }
        startKey = startVNode.key();
        idxInOld = oldKeyToIdxMap[startKey];
      }
      if (idxInOld == null) { // New node
        api.insertBefore(parentElem, createNode(startVNode, insertedVNodes,
          hooks, api), oldStartVNode.getNode());
      } else {
        let vNodeToMove = oldChildren[idxInOld];
        if (vNodeToMove == null) {
          throw new Error(`patch: duplicate keys [${startKey
                           }] found, children keys must be unique`);
        }

        // Tags changed, treat as a new node
        if (vNodeToMove.tag() !== startVNode.tag()) {
          api.insertBefore(parentElem, createNode(startVNode, insertedVNodes,
            hooks, api), oldStartVNode.getNode());
        } else {
          patchVNode(vNodeToMove, startVNode, insertedVNodes, hooks, api);
          oldChildren[idxInOld] = null; // Mark as moved
          api.insertBefore(parentElem, startVNode.getNode(),
                           oldStartVNode.getNode());
        }
      }
      startVNode = children[++startIdx];
    }
  }

  if (oldStartIdx > oldEndIdx) { // Insert the rest of the new nodes
    let before = children[endIdx + 1] == null ? null :
                                                children[endIdx + 1].getNode();
    for (; startIdx <= endIdx; ++startIdx) {
      api.insertBefore(parentElem, createNode(children[startIdx],
        insertedVNodes, hooks, api), before);
    }
  } else if (startIdx > endIdx) { // Remove the rest of the old nodes
    removeVNodes(parentElem, oldChildren, oldStartIdx, oldEndIdx, hooks, api);
  }
}

/**
 * Patches an old virtual node against its new representation (assumes that the
 * nodes are `patchable`).
 * @param {VirtualNode} oldVNode Virtual node being patched
 * @param {VirtualNode} vNode Virtual node representing what node should become
 * @param {Array<VirtualNode>} insertedVNodes Virtual nodes with newly inserted
 *   DOM elements
 * @param {Object} hooks Hooks to call during patching
 * @param {Object} api API to interact with the DOM
 * @returns {void}
 */
function patchVNode(oldVNode, vNode, insertedVNodes, hooks, api) {
  if (oldVNode === vNode) { // Virtual nodes are static
    return;
  }

  // Move the DOM node from the old node to the new one
  let domNode = oldVNode.getNode();
  vNode.setNode(domNode);

  // Call the `onUpdate` hook with the old and new virtual nodes
  hooks.onUpdate(oldVNode, vNode);

  if (vNode.type() === TEXT_NODE) {
    // Update the text of the DOM text node if the text changed
    let text = vNode.text();
    if (oldVNode.text() !== text) {
      api.setTextContent(domNode, text);
    }
  } else {
    let oldChildren = oldVNode.children();
    let children = vNode.children();
    if (oldChildren !== children) { // Children are static
      updateChildren(domNode, oldChildren, children, insertedVNodes, hooks,
                     api);
    }
  }
}

/**
 * Patches a given virtual node (`oldVNode`) with an associated DOM node to
 * become like `vNode`.
 * @param {VirtualNode} oldVNode Virtual node being patched
 * @param {VirtualNode} vNode Representation of what the node should become
 * @param {Object} hooks Hooks to call during patching
 * @param {Object} api API to interact with the DOM
 * @returns {VirtualNode} Virtual representation of final node
 */
function patch(oldVNode, vNode, hooks, api) {
  if (oldVNode == null || vNode == null) {
    throw new Error('patch: cannot handle null/undefined nodes');
  }

  let domNode = oldVNode.getNode(); // Get old node's DOM node
  if (domNode == null) {
    throw new Error(
      'patch: given virtual node does not have an associated DOM node');
  }

  // List of virtual nodes with newly inserted DOM elements. This is required
  // since `insert` should only be called after the patching ends
  let insertedVNodes = [];

  if (patchable(oldVNode, vNode)) { // Whether old virtual node can be patched
    patchVNode(oldVNode, vNode, insertedVNodes, hooks, api);
  } else { // Otherwise just create a new node
    let parentElem = api.parentElement(domNode);
    createNode(vNode, insertedVNodes, hooks, api);
    if (parentElem !== null) {
      api.insertBefore(parentElem, vNode.getNode(), api.nextSibling(domNode));
      removeVNodes(parentElem, [oldVNode], 0, 0, hooks, api);
    }
  }

  // Call the `onInsert` hook with each inserted virtual node
  for (let insertedVNode of insertedVNodes) {
    hooks.onInsert(insertedVNode);
  }

  return vNode;
}

/**
 * Patches the children of a virtual node against its old children.
 * @param {Element} parentElem Parent DOM element that contains the children
 * @param {Array<VirtualNode>} oldChildren Children being updated
 * @param {Array<VirtualNode>} children New children
 * @param {Object} hooks Hooks to call during patching
 * @param {Object} api API to interact with the DOM
 * @returns {void}
 */
function patchChildren(parentElem, oldChildren, children, hooks, api) {
  // List of virtual nodes with newly inserted DOM elements. This is required
  // since `insert` should only be called after the patching ends
  let insertedVNodes = [];

  // Patch the children
  updateChildren(parentElem, oldChildren, children, insertedVNodes, hooks, api);

  // Call the `onInsert` hook with each inserted virtual node
  for (let insertedVNode of insertedVNodes) {
    hooks.onInsert(insertedVNode);
  }
}

// Exported functions
module.exports = {patch, patchChildren};

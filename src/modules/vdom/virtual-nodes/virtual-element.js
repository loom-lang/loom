// Dependencies
let Reactive = require('../../reactive/reactive');
let Signal = require('../../reactive/signal');
let VirtualNode = require('./virtual-node');
let {ELEMENT_NODE} = require('../node-types');
let {nodeifyChild} = require('./utils');
let {onUpdate} = require('../patch-attributes');
let {patchChildren} = require('../patch');

/**
 * Namespace for SVG elements.
 */
const SVG_NS = 'http://www.w3.org/2000/svg';

/**
 * Virtual node that represents a DOM element. It supports reactive values to
 * represent its attributes and/or children; in which case the node is lazy in
 * the sense that such reactive values are only computed when the node is
 * about to be rendered.
 */
class VirtualElement extends VirtualNode {
  /**
   * Virtual element node's constructor.
   * @param {string} tag Element's tag
   * @param {string} id Element's id
   * @param {Array<string>} classes Element's classes
   * @param {any} attributes Element's attributes
   * @param {any} children Element's children
   * @param {Element} node DOM element this virtual element represents
   */
  constructor(tag, id, classes, attributes, children, node) {
    super();
    // Element's tag (e.g. `div`)
    this._tag = tag.toLowerCase();
    // Element's "static" id and classes; these may not be the element's final
    // qualifiers (e.g. if this virtual element was created via something such
    // as `<div#x.a.b {id: "y", class: "c"} />` then `this._id` is `'x'` and
    // `this._classes` is `['a', 'b']` even though the DOM element will have
    // `id` `'y'` and classes `['a', 'b', 'c']`)
    this._id = id;
    this._classes = classes;
    // The element's original attributes and children, as provided in the
    // constructor: note that they may be reactive values (whose value is only
    // accessed when this virtual element is to be rendered)
    this._originalAttributes = attributes;
    this._originalChildren = children;
    // "Normalised" attributes and children, in representations understood by
    // the DOM and attributes patching algorithms
    this._attributes = null;
    this._children = null;
    // Listeners associated with `this._originalAttributes` and
    // `this._originalChildren` when these are reactive values. They are needed
    // in order to repatch the children and update the attributes whenever the
    // reactive values change. They are set (if required) when the element is to
    // be rendered on the DOM, i.e. `onRender`
    this._attributesListener = null;
    this._childrenListener = null;
    // Key associated with this element. Keys are used to distinguish otherwise
    // similar siblings
    this._key = null;
    // Namespace of this element (e.g. the `SVG` namespace)
    this._namespace = null;
    // DOM element this virtual element is representing
    this._node = node;
  }

  /**
   * Type of DOM node (as defined in the HTML spec) this virtual node
   * represents. All instances of this class represent DOM element nodes,
   * defined in the DOM in `Node.ELEMENT_NODE` (number `1`).
   * @returns {number} `Node.ELEMENT_NODE`
   */
  type() {
    return ELEMENT_NODE;
  }

  /**
   * Gets the tag of the DOM element being represented.
   * @returns {string} Element's tag
   */
  tag() {
    return this._tag;
  }

  /**
   * Gets the element's "static" id (i.e. gets `'x'` in `<div#x {id: "y"} />`
   * even though the element's actual `id` is `'y'`).
   * @returns {string?} Element's "static" id
   */
  id() {
    return this._id;
  }

  /**
   * Gets the element's "static" classes (i.e. gets `['a', 'b']` in
   * `<div.a.b {class: "c"} />` even though the element's actual classes are
   * `['a', 'b', 'c']`).
   * @returns {Array<string>} Element's "static" classes
   */
  classes() {
    return this._classes;
  }

  /**
   * The element's original attributes, as provided in the constructor.
   * @returns {any} Element's original attributes
   */
  originalAttributes() {
    return this._originalAttributes;
  }

  /**
   * The element's original children, as provided in the constructor.
   * @returns {any} Element's original children
   */
  originalChildren() {
    return this._originalChildren;
  }

  /**
   * Gets the attributes of the DOM element being represented, normalised so
   * that they may be used by the attributes-patching algorithm; the original
   * attributes may be obtained via the `originalAttributes` method.
   * @returns {Object} Element's attributes
   */
  attributes() {
    return this._attributes;
  }

  /**
   * Gets the children of the DOM element being represented. The children are
   * normalised as an array of `VirtualNode`s (in a representation that the
   * patching algorithm understands); the original children may be obtained via
   * the `originalChildren` method.
   * @returns {Array<VirtualNode>} Normalised element's children
   */
  children() {
    return this._children;
  }

  /**
   * Gets the element's key. The key should be specified in the original
   * attributes and is set in the `_normalizeAttributes` method. Keys are used
   * by the patching algorithm to distinguish siblings that would otherwise be
   * similar.
   * @returns {any} Element's key as provided in the original attributes
   */
  key() {
    return this._key;
  }

  /**
   * Gets the element's namespace. Namespaces are set in the `beforeRender`
   * method.
   * @returns {string?} Element's namespace
   */
  namespace() {
    return this._namespace;
  }

  /**
   * Gets the DOM element being represented by this virtual element.
   * @returns {Element} DOM element being represented
   */
  getNode() {
    return this._node;
  }

  /**
   * Sets the DOM element this virtual element will represent.
   * @param {Element} node DOM element to represent
   * @returns {void}
   */
  setNode(node) {
    this._node = node;
  }

  /**
   * Method to run before this virtual element is used in the patching
   * algorithm. It is used to initialise some of the content required by the
   * algorithm. This allows virtual elements to be lazy, in the sense that
   * reactive values are only accessed when the element is about to be rendered.
   * @param {string?} parentNamespace Namespace obtained from the parent
   *   element
   * @returns {void}
   */
  beforeRender(parentNamespace) {
    // Set the SVG namespace when necessary; `foreignObject`s mustn't inherit
    // the namespace
    if (this._tag === 'svg') {
      this._namespace = SVG_NS;
    } else if (parentNamespace !== SVG_NS || this._tag !== 'foreignobject') {
      this._namespace = parentNamespace;
    }

    // Normalise the attributes to be used by the attributes-patching algorithm
    let attributes = this._originalAttributes;
    if (attributes instanceof Reactive) {
      attributes = attributes.value;
    }
    this._attributes = this._normalizeAttributes(attributes);
    // Normalise the children so they can be accessed in the patching algorithm
    let children = this._originalChildren;
    if (children instanceof Reactive) {
      children = children.value;
    }
    this._children = this._normalizeChildren(children);
  }

  /**
   * Normalises the attributes to be used by the attributes-patching algorithm.
   * In specific it normalises all attribute names; handles the `key` that may
   * be defined in the original attributes; sets `id` and `class` in the
   * attributes, taking into consideration the "static" id and classes of the
   * virtual element.
   * @param {Object} attributes Attributes to normalise
   * @returns {Object} Normalised attributes
   */
  _normalizeAttributes(attributes) {
    let attrs = this._attributes = {};
    for (let attr of Object.getOwnPropertyNames(attributes || {})) {
      // Attributes should be case insensitive
      attrs[attr.toLowerCase()] = attributes[attr];
    }
    this._key = attrs.key; // Set the key in the virtual element
    delete attrs.key; // The key is not meant to be passed to modules
    this._mergeIds(attrs);
    this._mergeClasses(attrs);
    return attrs;
  }

  /**
   * The `id` may be specified in two ways, directly in the element (`<i#x />`)
   * or in the attributes (`<i {id: "x"} />`), the one in the attributes has
   * precedence (i.e. the element `<i#x {id: "y"} />` should have `id` `"y"`).
   * This function merges both `id`s (note that the `id` in the attributes may
   * be a reactive value).
   * @param {Object} attributes Attributes with (possibly) defined id, where to
   *   set merged id
   * @returns {void}
   */
  _mergeIds(attributes) {
    // Don't touch the attributes' id when there is none defined in the element
    if (this._id != null) {
      let id = attributes.id;
      if (id == null) {
        // If no `id` defined in attributes use the one in the element
        attributes.id = this._id;
      } else if (id instanceof Reactive) {
        // When there is an `id` defined in attributes and it is a reactive
        // value, we must only use the element's `id` when the reactive
        // evaluates to `null`/`undefined`
        attributes.id = new Signal((sig) => {
          let idValue = id.subscribe(sig); // Depend on the reactive `id`
          return idValue != null ? idValue : this._id;
        });
      }
    }
  }

  /**
   * Similarly to the `id`, classes may be specified in two ways
   * (`<i.a.b {class: "c"} />`); these classes should be merged.
   * @param {Object} attributes Attributes where classes will be merged
   * @returns {void}
   */
  _mergeClasses(attributes) {
    // Transform classes string into array of classes
    let classesToArray = (classes) =>
      typeof classes === 'string' ? classes.split(/\s+/) : classes;

    // Don't touch the attributes' classes when there are none defined in the
    // element
    // NOTE: The used `concat`s may produce duplicates, but that should be fine
    if (this._classes.length > 0) {
      let classes = attributes.class;
      if (classes == null) {
        // If no classes were specified in the attributes, use the element's
        attributes.class = this._classes;
      } else if (classes instanceof Reactive) {
        // Assume that classes may come either as a string or an array
        if (classes instanceof Reactive) {
          attributes.class = new Signal((sig) => {
            let classesValue = classesToArray(classes.subscribe(sig));
            return classesValue != null ? this._classes.concat(classesValue) :
                                          this._classes;
          });
        }
      } else {
        attributes.class = this._classes.concat(classesToArray(classes));
      }
    }
  }

  /**
   * Normalises the children, making them an array of `VirtualNode`s. The
   * children are also prepared to be rendered (their `beforeRender` method is
   * called).
   * @param {any} children Children to normalise
   * @returns {Array<VirtualNode>} Normalised children (prepared to be rendered)
   */
  _normalizeChildren(children) {
    // Children must be an array of `VirtualNode`s
    return (Array.isArray(children) ? children : [children]).map((child) => {
      let vChild = nodeifyChild(child); // Transform child in a `VirtualNode`
      // Since the children are about to be rendered, call their `beforeRender`
      // method if defined
      vChild.beforeRender && vChild.beforeRender(this._namespace);
      return vChild;
    });
  }

  /**
   * Method called when an actual DOM element is associated with this virtual
   * element. Used to associate listeners with this node's reactive values and
   * act accordingly whenever they change.
   * @param {Object} hooks Hooks to call during patching
   * @param {Object} api API to interact with the DOM
   * @returns {void}
   */
  onRender(hooks, api) {
    // Create a listener on the reactive attributes and update them whenever the
    // reactive changes
    let attributes = this._originalAttributes;
    if (attributes instanceof Reactive) {
      this._attributesListener = attributes.onInvalidate((reactive) => {
        let oldAttributes = this._attributes;
        let attributes = this._attributes =
          this._normalizeAttributes(reactive.value);
        // Update the attributes
        onUpdate(this._node, oldAttributes, attributes, api);
      });
    }
    // Create a listener on the reactive children to update them whenever the
    // reactive changes
    let children = this._originalChildren;
    if (children instanceof Reactive) {
      this._childrenListener = children.onInvalidate((reactive) => {
        let oldChildren = this._children;
        let children = this._children = this._normalizeChildren(reactive.value);
        // Patch the new children against the old children
        patchChildren(this.getNode(), oldChildren, children, hooks, api);
      });
    }
  }

  /**
   * Method called when the DOM element is disassociated from this virtual
   * element. Used to remove the previously associated listeners.
   * @returns {void}
   */
  onUnrender() {
    // Remove listener on attributes when they're reactive
    let attributes = this._originalAttributes;
    if (attributes instanceof Reactive) {
      attributes.removeListener(this._attributesListener);
      this._attributesListener = null;
    }
    // Remove listener on children when reactive
    let children = this._originalChildren;
    if (children instanceof Reactive) {
      children.removeListener(this._childrenListener);
      this._childrenListener = null;
    }
  }
}

// Exported class
module.exports = VirtualElement;

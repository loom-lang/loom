/**
 * Module containing some utility functions.
 */

// Dependencies
let {inspect: nodeInspect} = require('util');

/**
 * Turns a JavaScript object into a colourised string to print.
 * @param {Object} o Object to inspect
 * @returns {string} Object as a string
 */
function inspect(o) {
  return nodeInspect(o, {depth: null, colors: colorsEnabled()});
}

/**
 * Creates a (shallow) clone of `object` with the properties of the `extension`.
 * @param {Object} object Object to clone and extend
 * @param {Object} extension Object with properties to copy
 * @returns {Object} Extended object
 */
function extend(object, extension) {
  return Object.assign({}, object, extension);
}

/**
 * Whether the current process has colours enabled.
 * @returns {boolean} Whether colours are enabled
 */
function colorsEnabled() {
  return typeof process !== undefined && process !== null &&
         process.stdout != null && process.stdout.isTTY &&
         (process.env == null || !process.env.NODE_DISABLE_COLORS);
}

/**
 * Returns the full name of a Babel plugin.
 * @param {string} plugin Name of the Babel plugin
 * @param {boolean} isPreset Whether the plugin a preset
 * @returns {string} Full name of plugin
 */
function fullBabelPluginName(plugin, isPreset) {
  let start = isPreset ? 'babel-preset-' : 'babel-plugin-';
  return plugin.startsWith(start) ? plugin : start + plugin;
}

/**
 * Resolves (requires) a Babel plugin or preset.
 * @param {any} plugin Plugin to resolve
 * @param {boolean} isPreset Whether we are resolving a preset
 * @returns {Object} Resolved Babel plugin
 */
function resolveBabelPlugin(plugin, isPreset) {
  return (typeof plugin !== 'string') ? plugin :
    require(fullBabelPluginName(plugin, isPreset));
}

/**
 * Makes an error appear pretty, overriding its `toString` and `stack`.
 * @param {Error} error Error to make pretty, should have a location, the input
 *   from which it was generated, and a message to show
 * @returns {void}
 */
function prettifyError(error) {
  /**
   * Makes an error pretty by showing its formatted location information as well
   * as the line where the error occurred and a pointer to said error.
   * @returns {string} Pretty error message
   */
  function errorToString() {
    let {start: {line: startLine, column: startCol},
         end: {line: endLine, column: endCol}} = this.location;
    // Increment final column when it is the same as the start column (EOF)
    startLine === endLine && startCol === endCol && ++endCol;
    // First line where error occurred
    let errorLine = this.input.split('\n')[startLine - 1];
    // Only show first line, so trim marker to end of first line
    let markerStart = startCol - 1;
    let markerEnd = startLine === endLine ? endCol - 1 : this.errorLine.length;
    // FIXME: Tabs are a problem
    let marker = ' '.repeat(markerStart) + '^'.repeat(markerEnd - markerStart);
    // Colourise output if colours are enabled
    if (colorsEnabled()) {
      let colorize = (str) => '\x1b[1;31m' + str + '\x1b[0m';
      errorLine = errorLine.slice(0, markerStart) +
        colorize(errorLine.slice(markerStart, markerEnd)) +
        errorLine.slice(markerEnd);
      marker = colorize(marker);
    }
    // Print "filename?:line:column: message" followed by the error line and a
    // pointer to the error location
    return `${this.filename ? `${this.filename}:` : ''}${startLine}:${
      startCol}: ${this.message}\n${errorLine}\n${marker}`;
  }
  // Override error `toString` and `stack`
  error.toString = errorToString;
  error.stack = error.toString();
}

// Exported utilities
module.exports = {inspect, extend, colorsEnabled, fullBabelPluginName,
  resolveBabelPlugin, prettifyError};

/**
 * Testing of objects.
 */

let {eq, run, testSyntax} = require('./utils');

suite('Objects');

// Syntax
test('syntax', function() {
  let pass = ['{}', '{ }', '{o: {o: {}}}', '{x: 0}', '{0: 0}', '{["x"]: 0}',
    '{x:0,y:0}', '{ x : 0 , y : 0 }', '{x: 0,}', '{...{x:0}}',
    '{ ... { x : 0 } }', '{0: 0, x: 0, "y": 0, ["z"]: 0, ...{a: 0, b: 0}}',
    `{a: 0 // Comment in automatic comma insertion
      ...{b: 0}
      c: 0}`];
  testSyntax(pass);
});

// Objects with identifiers as keys
test('id key', function() {
  let o = run('{x: 0, y: 1}');
  eq(o.x, 0);
  eq(o.y, 1);
});

// Objects with numbers as keys
test('number key', function() {
  let o = run('{0: 0, 1: 1}');
  eq(o[0], 0);
  eq(o[1], 1);
});

// Objects with strings as keys
test('string key', function() {
  let o = run('{"x": 0, "y": 1}');
  eq(o.x, 0);
  eq(o.y, 1);
});

// Objects with computed keys
test('computed key', function() {
  let o = run('{["x"]: 0, [2 * 2]: 1}');
  eq(o.x, 0);
  eq(o[4], 1);
});

// Objects with shorthand properties
test('shorthand property', function() {
  let o = run('var x = 0, y = 1; {x, y}');
  eq(o.x, 0);
  eq(o.y, 1);
});

// Objects with reserved keywords as keys
test('keyword key', function() {
  let o = run('{if: 0, for: 1, null: 2, true: 3}');
  eq(o.if, 0);
  eq(o.for, 1);
  eq(o.null, 2);
  eq(o.true, 3);
});

// Objects with spread properties
test('spread property', function() {
  let o = run(`
    var a = {x: 0, y: 1}
    var b = {w: 2, z: 3}
    {x: 4, y: 5, ...a, ...b, w: 6, z: 7}
  `);
  // `a` overrides `x` and `y` since it was added after
  eq(o.x, 0);
  eq(o.y, 1);
  // `b` does not override `w` and `z` since it was added before
  eq(o.w, 6);
  eq(o.z, 7);
});

// Objects with template strings as keys
test('string template key', function() {
  let o = run('{"x${0}": 0}');
  eq(o.x0, 0);
});

// Using the member operators
test('property access', function() {
  eq(run(`
      var o = {x: 1, y: {w: 2, z: 3}, if: 4};
      o.x + o . x + o.if + o . y . z + o["y"]["w"] + o [ "y" ] [ "w" ]
    `), 13);
});

// Using the `delete` operator
test('delete property', function() {
  eq(run('var o = {x: 0}; delete o.x'), true);
  eq(run('var o = {x: 0}; delete o.x; o.x'), undefined);
});

// Dependencies
let Reactive = require('../../reactive/reactive');
let {ELEMENT_NODE, TEXT_NODE} = require('../node-types');

/**
 * Shallowly transforms a DOM node into a virtual node.
 * @param {Node} node DOM node to transform
 * @param {Object} api API to interact with the DOM
 * @returns {VirtualNode} Virtual node representation of the DOM node
 */
function virtualize(node, api) {
  let nodeType = api.nodeType(node);
  // Runtime dependencies to avoid dependency cycles
  if (nodeType === ELEMENT_NODE) {
    let VirtualElement = require('./virtual-element');
    let tag = api.tagName(node); // `VirtualElement` turns into lower case
    return new VirtualElement(tag, null, [], {}, [], node);
  } else if (nodeType === TEXT_NODE) {
    let VirtualText = require('./virtual-text');
    let text = api.getTextContent(node);
    return new VirtualText(text, node);
  } else {
    throw new Error(`virtualize: cannot virtualize DOM node with type: ${
      nodeType}`);
  }
}

/**
 * Transforms a child (as found in the children array) into a virtual node.
 * Takes care of converting reactive children into `VirtualReactive` nodes.
 * @param {any} child Child element
 * @returns {VirtualNode} Nodeified child
 */
function nodeifyChild(child) {
  // Reactive children are transformed into `VirtualReactive`s (the runtime
  // dependency avoids dependency cycles)
  if (child instanceof Reactive) {
    let VirtualReactive = require('./virtual-reactive');
    return new VirtualReactive(child);
  }
  // If not a reactive value, transform an arbitrary value into a virtual node
  return nodeify(child);
}

/**
 * Returns the passed value as is if it is already a virtual node. Otherwise
 * create and return a `VirtualText` node with the stringified value as its text
 * (`null` and `undefined` values become an empty string).
 * @param {any} value Value to transform into a virtual node
 * @returns {VirtualNode} Virtual node representation of passed value
 */
function nodeify(value) {
  // Runtime dependencies to avoid dependency cycles
  let VirtualNode = require('./virtual-node');
  if (value instanceof VirtualNode) { // Already a virtual node
    return value;
  }
  let VirtualText = require('./virtual-text');
  // Transform `null`/`undefined` into an empty stringed virtual text node
  return new VirtualText(value == null ? '' : String(value));
}

// Exported utility functions
module.exports = {virtualize, nodeifyChild, nodeify};

/**
 * Testing of operators.
 */

let {eq, run} = require('./utils');

suite('Operators');

// Sequence of expressions
test('sequence', function() {
  eq(run('1 , 2,3'), 3);
});

// Arithmetic operations
test('arithmetic', function() {
  // Binary
  eq(run('1 + 1+1'), 3);
  eq(run('1 - 1-1'), -1);
  eq(run('2 * 2*2'), 8);
  eq(run('4 / 2/2'), 1);
  eq(run('4 % 3%2'), 1);
  eq(run('2 ^^ 2^^2'), 16);
  // Unary
  eq(run('+1'), 1);
  eq(run('+ 1'), 1);
  eq(run('-1'), -1);
  eq(run('- 1'), -1);
  eq(run('- - + -1'), -1);
  // Mix
  eq(run('1 + -1'), 0);
  eq(run('1 +- -1'), 2);
});

// Bit operations
test('bit', function() {
  // Binary
  eq(run('10 | 3|4'), 15);
  eq(run('10 & 3&6'), 2);
  eq(run('10 ^ 3^1'), 8);
  eq(run('10 << 3<<1'), 160);
  eq(run('-1 >> 1>>1'), -1);
  eq(run('-1 >>> 1>>>1'), 1073741823);
  // Unary
  eq(run('~10'), -11);
  eq(run('~ 10'), -11);
  eq(run('~~ ~10'), -11);
});

// Logical operations
test('logical', function() {
  // Binary
  eq(run('true || false||false'), true);
  eq(run('true && false&&false'), false);
  // Unary
  eq(run('!true'), false);
  eq(run('! false'), true);
  eq(run('!! !true'), false);
});

// Comparisons
test('comparison', function() {
  // Equality
  eq(run('1 == 1==true'), true);
  eq(run('1 != 1!=false'), false);
  // `==` compiles to `===`
  eq(run('true == 1'), false);
  // Relational
  eq(run('1 > 1'), false);
  eq(run('1>1'), false);
  eq(run('1 >= 1'), true);
  eq(run('1>=1'), true);
  eq(run('1 < 1'), false);
  eq(run('1<1'), false);
  eq(run('1 <= 1'), true);
  eq(run('1<=1'), true);
});

// Operator precedences
test('precedence', function() {
  eq(run('2 + 2 * 2 < (2 + 2) * 2'), true);
  eq(run('2 + 2 * 2 < 7 && 10 >> 3 >= 1 && false == 3 < 1 + 1'), true);
  eq(run('2 + 2 * 2 < 7 && 10 >> 3 > 1 || 2 * 1 == 4 / 2 && 1 >= 1'), true);
});

// `in` operator
test('in operator', function() {
  eq(run('1 in [0, 1, 2]'), true);
  eq(run('3 in [0, 1, 2]'), false);
});

// `of` operator
test('of operator', function() {
  eq(run('"x" of {x: 0, y: 1, z: 2}'), true);
  eq(run('"w" of {x: 0, y: 1, z: 2}'), false);
});

// `instanceof` operator
test('instanceof', function() {
  eq(run('new Number(0) instanceof Number'), true);
  eq(run('0 instanceof Number'), false);
});

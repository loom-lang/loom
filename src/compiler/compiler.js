/**
 * Loom's compiler. This module is responsible for the compilation of a Loom AST
 * into a (Babel compatible) JavaScript AST. Note that the compilation rules
 * assume that the provided Loom AST has already been type checked.
 */

// Dependencies
let ast = require('../parser/nodes');
let js = require('./js-nodes');
let {VOID_0, expressionOptions, statementOptions, statementize, blockify,
     iife, getModuleId, declareAuxiliaryIds, compileLoop, splitDeclaration,
     compileParameters, cacheUpdatable, jsToAST} = require('./utils');

/**
 * Compilation rules. Each rule receives the node being compiled and the
 * compilation options. The rule that is called for a given node is the most
 * "specific" one, i.e. if both `Not` and `UnaryOperation` rules exist, a node
 * that is an instance of `ast.Not` is called with the `Not` rule.
 * Each rule either returns the compiled node or a list of nodes (statements)
 * used to compile it.
 */
const RULES = {
  // Special node used to add location information to "names" in the language
  Name(node) {
    return js.identifier(node.name);
  },

  // Root node =================================================================
  Program(node, o) {
    // Options to compile all instructions as statements
    let oStat = statementOptions(o);
    // Compile the program's body
    let body = [];
    for (let i = 0, l = node.body.length; i < l; ++i) {
      // Last statement may need to end in a value to make the program seem like
      // an expression
      let stat = c(node.body[i], i !== node.body.length - 1 ? oStat : o);
      Array.isArray(stat) ? body.push(...stat) : body.push(stat);
    }

    // Declare auxiliary ids
    declareAuxiliaryIds(body, o.idsToDeclare);
    // Import auxiliary modules
    let imports = [];
    for (let name of o.modulesToImport) {
      let module = o.auxiliaryModules.get(name);
      let specifiers = [];
      if (module.type === 'Identifier') { // Default specifier
        specifiers.push(js.importDefaultSpecifier(module));
      } else {
        for (let name in module) { // Named specifiers
          specifiers.push(js.importSpecifier(module[name],
                                             js.identifier(name)));
        }
      }
      imports.push(js.importDeclaration(specifiers,
        js.stringLiteral(`${o.modulesPath}/${name}`)));
    }
    body.unshift(...imports);

    // Generate a name for all auxiliary ids (this is something that must be
    // done at the end of the compilation, once all declarations are known)
    for (let id of o.auxiliaryIds) {
      // Assign to the identifier a name that doesn't conflict: start with the
      // suggested name and append an increasing index until the name doesn't
      // conflict
      let name = id.name;
      for (let i = 1; o.declaredNames.has(name); i++) {
        name = id.name + i;
      }
      id.name = name; // Name doesn't conflict, assign it to identifier
      o.declaredNames.add(name); // Declare the name so it can't be used again
    }

    return js.program('module', body, []);
  },

  // Statements ================================================================
  // Import statement (e.g. `import x, {y, w as z} from "m";`)
  Import(node, o) {
    return js.importDeclaration(node.specifiers.map((specifier) => {
      o.declaredNames.add(specifier.local);
      let local = c(specifier.local, o);
      if (specifier.default) { // Default specifier
        return js.importDefaultSpecifier(local);
      } else if (specifier.namespace) { // Namespace specifier
        return js.importNamespaceSpecifier(local);
      } else {
        return js.importSpecifier(local, c(specifier.imported, o));
      }
    }), c(node.source, expressionOptions(o)));
  },
  // Export statement (e.g. `export var x;`)
  Export(node, o) {
    let oExp = expressionOptions(o);
    if (node.default) {
      return js.exportDefaultDeclaration(c(node.declaration,
        node.declaration instanceof ast.Expression ? oExp : o));
    } else if (node.all) {
      return js.exportAllDeclaration(c(node.source, oExp));
    } else {
      return js.exportNamedDeclaration(node.declaration &&
        c(node.declaration, o), node.specifiers.map((specifier) =>
          js.exportSpecifier(c(specifier.local, o),
                             c(specifier.exported, o))
        ), node.source && c(node.source, oExp));
    }
  },
  // Variable declaration (e.g. `var x = 1, y = 2;`, `const [x] = [1];`)
  VariableDeclaration(node, o) {
    // Note that `var`s are compiled to JS' `let`s (block scoped)
    let kind = node instanceof ast.VarDeclaration ? 'let' : 'const';
    let oExp = expressionOptions(o);
    let decl = js.variableDeclaration(kind, node.declarators.map(
    (declarator) => {
      let [pattern, names] = c(declarator.pattern, oExp);
      for (let name of names) { // Declare bound names
        o.declaredNames.add(name);
      }
      return js.variableDeclarator(pattern,
        declarator.initializer && c(declarator.initializer, oExp));
    }));
    // The value associated to a variable declaration should be `undefined`
    if (o.mustEndInValue) {
      return [decl, VOID_0];
    }
    return decl;
  },
  // Function declaration (e.g. `function x() y`)
  FunctionDeclaration(node, o) {
    let name = c(node.name, o);
    o.declaredNames.add(name.name);
    let params = compileParameters(node.parameters, o, c);
    let body = c(blockify(node.body), statementOptions(o, true));
    let fun = js.functionDeclaration(name, params, body, false, false);
    // When a function declaration is to be returned or pushed, we must return
    // two statements, the first declaring the function, the second returning or
    // pushing it
    if (o.mustReturn || o.pushTo != null || o.mustEndInValue) {
      return [fun, statementize(o, name)];
    }
    return fun;
  },
  // Return statement (e.g. `return x;`)
  Return(node, o) {
    return js.returnStatement(node.expression &&
                              c(node.expression, expressionOptions(o)));
  },
  // Break statement (e.g. `break label;`)
  Break(node, o) {
    return js.breakStatement(node.label && c(node.label, o));
  },
  // Continue statement (e.g. `continue label;`)
  Continue(node, o) {
    return js.continueStatement(node.label && c(node.label, o));
  },

  // Expressions ===============================================================
  // A block expression
  Block(node, o) {
    // If the block is being compiled as an expression and all statements in
    // the block are expressions, we can simply return them in a sequence (i.e.
    // `do {x, y}` becomes (in JS) `x, y`)
    if (o.expression && node.body.every((s) => s instanceof ast.Expression)) {
      return node.body.length === 0 ? VOID_0 :
        (node.body.length === 1 ? c(node.body[0], o) :
          js.sequenceExpression(node.body.map((exp) => c(exp, o))));
    }

    // When compiling the block as an expression and some of its statements
    // aren't expressions, we compile it as an IIFE (i.e. `do {var x = 0; x}`
    // becomes `(() => {let x = 0; return x;})()`)

    // Statements inside this block that require auxiliary ids will append them
    // to this array
    let toDeclare = [];
    // Options to compile all instructions as statements
    let oStat = statementOptions(o, false, null, false,
                                 {idsToDeclare: toDeclare});
    // The last statement may need to be returned or pushed
    let oLast = statementOptions(oStat, o.expression || o.mustReturn, o.pushTo,
                                 o.mustEndInValue);
    // Compile the block's body
    let body = [];
    for (let i = 0, l = node.body.length; i < l; ++i) {
      let stat = c(node.body[i], i !== node.body.length - 1 ? oStat : oLast);
      Array.isArray(stat) ? body.push(...stat) : body.push(stat);
    }
    // Declare (at the beginning of the block) all auxiliary ids
    declareAuxiliaryIds(body, toDeclare);
    // Return the block (possibly an IIFE)
    let block = js.blockStatement(body, []);
    return o.expression ? iife(block) : block;
  },
  // If expression (e.g. `if (x) y else z`)
  If(node, o) {
    let oExp = expressionOptions(o);
    if (o.expression) { // Compile as conditional expression
      return js.conditionalExpression(c(node.test, oExp),
        c(node.consequent, oExp),
        node.alternate ? c(node.alternate, oExp) : VOID_0);
    }
    // Compile as an if statement (with forced blocks to self-contain possible
    // auxiliary ids)
    return js.ifStatement(c(node.test, oExp), c(blockify(node.consequent), o),
      node.alternate && c(blockify(node.alternate), o));
  },
  // While expression (e.g. `while (x) y`)
  While(node, o) {
    return compileLoop(o, false, (oBody) => js.whileStatement(
      c(node.test, expressionOptions(o)), c(blockify(node.body), oBody)));
  },
  // Do while expression (e.g. `do x while (y)`)
  DoWhile(node, o) {
    return compileLoop(o, false, (oBody) => js.doWhileStatement(
      c(blockify(node.body), oBody), c(node.test, expressionOptions(o))));
  },
  // For in expression (e.g. `for (var x, i in y) x`)
  ForIn(node, o) {
    let oExp = expressionOptions(o);
    // Assignees can be a declaration or an expression
    let declaredAssignees = node.assignees instanceof ast.VariableDeclaration;
    let assignees = c(node.assignees, declaredAssignees ? statementOptions(o) :
                                                          oExp);
    // The loop requires an index when the node's assignees is a declaration
    // with more than one declarator or a sequence expression
    let needsIndex = declaredAssignees ? assignees.declarations.length > 1 :
                                         node.assignees instanceof ast.Sequence;
    return compileLoop(o, needsIndex, (oBody, indexId) => {
      let assignee, body; // Assignee and body of the `for of` JavaScript cycle
      if (needsIndex) {
        // Prepend the body with the declaration/assignment of the current index
        let bodyCode = [];
        if (declaredAssignees) {
          // Prepend `let|const i = _i;`
          let split = splitDeclaration(assignees);
          assignee = split[0];
          split[1].declarations[0].init = indexId;
          bodyCode.push(split[1]);
        } else { // `assignees` is a sequence expression
          // Prepend `i = _i;`
          assignee = assignees.expressions[0];
          bodyCode.push(js.expressionStatement(
            js.assignmentExpression('=', assignees.expressions[1], indexId)));
        }
        // Compile the actual body with the provided options and append an
        // increment of the current index (`++_i;`)
        bodyCode.push(c(node.body, oBody),
          js.expressionStatement(js.updateExpression('++', indexId, true)));
        body = js.blockStatement(bodyCode, []);
      } else {
        // Compile a block version of the actual body with the provided options
        assignee = assignees;
        body = c(blockify(node.body), oBody);
      }
      return js.forOfStatement(assignee, c(node.collection, oExp), body);
    });
  },
  // For of expression (e.g. `for (var k, v of x) v`)
  ForOf(node, o) {
    let oExp = expressionOptions(o);
    // Assignees can be a declaration or an expression
    let declaredAssignees = node.assignees instanceof ast.VariableDeclaration;
    let assignees = c(node.assignees, declaredAssignees ? statementOptions(o) :
                                                          oExp);
    // The loop requires a value when the node's assignees is a declaration with
    // more than one declarator or a sequence expression
    let needsValue = declaredAssignees ? assignees.declarations.length > 1 :
                                         node.assignees instanceof ast.Sequence;
    let objectRef; // Reference to the object being iterated
    // We need to store the object being iterated in an auxiliary variable when
    // the value is needed in the iteration and the object expression is complex
    if (needsValue && !(node.object instanceof ast.Identifier ||
                        node.object instanceof ast.Literal)) {
      objectRef = js.identifier('_ref');
      o.auxiliaryIds.push(objectRef);
      o.idsToDeclare.push([objectRef, c(node.object, oExp)]);
    } else {
      objectRef = c(node.object, oExp);
    }
    return compileLoop(o, false, (oBody) => {
      let assignee, body; // Assignee and body of the `for in` JavaScript cycle
      if (needsValue) {
        // Prepend the body with the declaration/assignment of the value
        let bodyCode = [];
        if (declaredAssignees) {
          // Prepend `let|const v = _ref[k];`
          let split = splitDeclaration(assignees);
          assignee = split[0];
          split[1].declarations[0].init = js.memberExpression(objectRef,
            assignee.declarations[0].id, true);
          bodyCode.push(split[1]);
        } else { // `assignees` is a sequence expression
          // Prepend `v = _ref[k];`
          assignee = assignees.expressions[0];
          bodyCode.push(js.expressionStatement(js.assignmentExpression('=',
            assignees.expressions[1], js.memberExpression(objectRef, assignee,
                                                          true))));
        }
        // Compile the actual body with the provided options
        bodyCode.push(c(node.body, oBody));
        body = js.blockStatement(bodyCode, []);
      } else {
        // Compile a block version of the actual body with the provided options
        assignee = assignees;
        body = c(blockify(node.body), oBody);
      }
      return js.forInStatement(assignee, objectRef, body);
    });
  },
  // Try expression (e.g. `try x() catch(e) e`)
  Try(node, o) {
    // Options for the `try`/`catch` blocks
    let oBlock = statementOptions(o, o.expression || o.mustReturn, o.pushTo,
                                  o.mustEndInValue);
    // `try` block
    let block = c(blockify(node.body), oBlock);
    // `catch` block
    let handler = null;
    // Create an empty `catch` block when the `try` expression has no `catch`
    // and no `finally` declared
    if (node.handler == null && node.finalizer == null) {
      handler = js.catchClause(js.identifier('_e'), js.blockStatement([], []));
    } else if (node.handler != null) {
      let pattern;
      // If no parameter was declared, generate a new one
      if (node.handler.param == null) {
        pattern = js.identifier('_e');
        o.auxiliaryIds.push(pattern);
      } else { // Compile the declared parameter
        let names;
        [pattern, names] = c(node.handler.param, expressionOptions(o));
        for (let name of names) { // Declare bound names
          o.declaredNames.add(name);
        }
      }
      handler = js.catchClause(pattern, c(blockify(node.handler.body), oBlock));
    }
    // `finally` block
    let finalizer = node.finalizer &&
                    c(blockify(node.finalizer), statementOptions(o));
    // Create a try statement and wrap it in an IIFE when compiling the node as
    // an expression
    let tryStatement = js.tryStatement(block, handler, finalizer);
    return o.expression ? iife(js.blockStatement([tryStatement], [])) :
                          tryStatement;
  },
  // Throw expression (e.g. `throw x;`)
  Throw(node, o) {
    // Create a throw statement and wrap it in an IIFE when compiling the node
    // as an expression
    let throwStat = js.throwStatement(c(node.expression, expressionOptions(o)));
    return o.expression ? iife(js.blockStatement([throwStat], [])) : throwStat;
  },
  // A sequence expression (e.g. `x, y`)
  Sequence(node, o) {
    let oExp = expressionOptions(o);
    return statementize(o,
      js.sequenceExpression(node.expressions.map((exp) => c(exp, oExp))));
  },
  // Logical assignment (e.g. `x &&= y`, `x ||= y`)
  LogicalAssignment(node, o) {
    let op = node.operator.slice(0, -1); // Convert `||=` to `||`; `&&=` to `&&`
    let oExp = expressionOptions(o);
    let [left, leftRef] = cacheUpdatable(node.assignee, oExp, c);
    return statementize(o,
      js.logicalExpression(op, left,
        js.assignmentExpression('=', leftRef, c(node.expression, oExp))));
  },
  // Variable assignment (e.g. `x = y`, `x += y`)
  Assignment(node, o) {
    let op = node instanceof ast.SimpleAssignment ? '=' : node.operator;
    if (op === '^^=') { // Convert `^^=` to `**=`
      op = '**=';
    }
    let oExp = expressionOptions(o);
    return statementize(o,
      js.assignmentExpression(op, c(node.assignee, oExp)[0],
                              c(node.expression, oExp)));
  },
  // Logical operation (e.g. `x || y`)
  LogicalOperation(node, o) {
    let oExp = expressionOptions(o);
    return statementize(o,
      js.logicalExpression(node.operator, c(node.left, oExp),
                           c(node.right, oExp)));
  },
  // In operation (e.g. `0 in [0, 1, 2]`)
  In(node, o) {
    // Get the id used to access the `inIterable` function
    let inId = getModuleId('helpers/in-iterable', '_in', o);
    let oExp = expressionOptions(o);
    // Compile `l in r` to `_in(l, r)`
    return statementize(o, js.callExpression(inId, [c(node.left, oExp),
                                                    c(node.right, oExp)]));
  },
  // Binary operation (e.g. `x + y * z >= a % b`, `"x" of {x: 0}`)
  BinaryOperation(node, o) {
    let op;
    if (node instanceof ast.InstanceOf) {
      op = 'instanceof';
    } else if (node instanceof ast.Of) {
      op = 'in';
    } else {
      op = node.operator;
      if (op === '==' || op === '!=') { // Convert `==` to `===`; `!=` to `!==`
        op += '=';
      } else if (op === '^^') { // Convert `^^` to `**`
        op = '**';
      }
    }
    let oExp = expressionOptions(o);
    return statementize(o,
      js.binaryExpression(op, c(node.left, oExp), c(node.right, oExp)));
  },
  // Unary operation (e.g. `-x`, `!x`)
  UnaryOperation(node, o) {
    let op;
    if (node instanceof ast.Not) {
      op = '!';
    } else if (node instanceof ast.BitNot) {
      op = '~';
    } else if (node instanceof ast.Delete) {
      op = 'delete';
    } else if (node instanceof ast.TypeOf) {
      op = 'typeof';
    } else {
      op = node.operator;
    }
    return statementize(o,
      js.unaryExpression(op, c(node.expression, expressionOptions(o))));
  },
  // Variable update (e.g. `x++`, `--x`)
  Update(node, o) {
    return statementize(o,
      js.updateExpression(node.operator,
        c(node.target, expressionOptions(o))[0], node.prefix));
  },
  // Dereference signal (e.g. `*x`)
  Dereference(node, o) {
    let reactive = c(node.expression, expressionOptions(o));
    // If the dereferencing is happening inside a signal expression, we must
    // add the signal as a subscriber to the value being dereferenced
    return statementize(o,
      o.signal ? js.callExpression(js.memberExpression(reactive,
          js.identifier('subscribe'), false), [o.signal]) :
        js.memberExpression(reactive, js.identifier('value'), false));
  },
  // Call operation (e.g. `x()`)
  Call(node, o) {
    let oExp = expressionOptions(o);
    return statementize(o,
      js.callExpression(c(node.callee, oExp), node.arguments.map(
        (argument) => {
          let exp = c(argument.expression, oExp);
          return argument.spread ? js.spreadElement(exp) : exp;
        }
      )));
  },
  // New operator (e.g. `new x()`)
  New(node, o) {
    let oExp = expressionOptions(o);
    return statementize(o,
      js.newExpression(c(node.callee, oExp), node.arguments.map(
        (argument) => {
          let exp = c(argument.expression, oExp);
          return argument.spread ? js.spreadElement(exp) : exp;
        }
      )));
  },
  // Object member access (e.g. `x.p`, `x[p]`)
  Member(node, o) {
    let oExp = expressionOptions(o);
    let property = c(node.property, oExp);
    return statementize(o,
      js.memberExpression(c(node.object, oExp), property, node.computed));
  },
  // Function expression (e.g. `(x, y) -> x + y`, `x => x`, `function x(y) {}`)
  FunctionExpression(node, o) {
    let params = compileParameters(node.parameters, o, c);
    let body = c(blockify(node.body), statementOptions(o, true));
    // Create a normal or arrow function
    let fun = node.bound ? js.arrowFunctionExpression(params, body, false) :
      js.functionExpression(node.name && c(node.name, o), params, body);
    return statementize(o, fun);
  },
  // Array (e.g. `[x,, y]`)
  Array(node, o) {
    let oExp = expressionOptions(o);
    return statementize(o,
      js.arrayExpression(node.elements.map((element) => {
        if (!element.expression) { // Elision
          return null;
        }
        let exp = c(element.expression, oExp);
        return element.spread ? js.spreadElement(exp) : exp;
      })));
  },
  // Object (e.g. `{a: x, [b]: y, c}`)
  Object(node, o) {
    let oExp = expressionOptions(o);
    return statementize(o,
      js.objectExpression(node.properties.map((property) => {
        // A spread property
        if (property.spread) {
          return js.spreadProperty(c(property.expression, oExp));
        }
        // Keys can be ids, strings, string templates, numbers, or arbitrary
        // expressions (computed keys). We compile string template keys to
        // computed keys
        let computed = property.computed ||
                       property.key instanceof ast.StringTemplate;
        return js.objectProperty(c(property.key, oExp),
          c(property.expression, oExp), computed, false, []);
      })));
  },
  // Regular expression (e.g. `/x/g`)
  RegularExpression(node, o) {
    return statementize(o, js.regExpLiteral(node.pattern, node.flags));
  },
  // HTML (e.g. `<div#id {x: y} />`)
  HTML(node, o) {
    // Example:
    //   `<div#i.c.d {a: 1}> ["Hello", "World"]`
    // compiles to:
    //   `new _VElem('div', 'i', ['c', 'd'], {a: 1}, ['Hello', 'World'])`
    let vElemId = getModuleId(
      'vdom/virtual-nodes/virtual-element', '_VElem', o);
    let oExp = expressionOptions(o);
    // HTML element type
    let tag = js.stringLiteral(node.tag.name);
    // Qualifiers
    let id = js.nullLiteral();
    let classes = js.arrayExpression([]);
    for (let qualifier of node.qualifiers) {
      let string = js.stringLiteral(qualifier.name.name);
      if (qualifier.kind === 'id') { // Qualifier is an id
        id = string;
      } else { // Qualifier is a class (push to existent array)
        classes.elements.push(string);
      }
    }
    // Attributes and children
    let attributes = node.attributes == null ? js.objectExpression([]) :
                                               c(node.attributes, oExp);
    let children = node.children == null ? js.arrayExpression([]) :
                                           c(node.children, oExp);
    return statementize(o, js.newExpression(vElemId,
                             [tag, id, classes, attributes, children]));
  },
  // CSS (e.g. `css {color: "red"}`)
  CSS(node, o) {
    // Example:
    //   `css {color: "red", margin: 1, |div|: {}, ...y, background: "pink"}`
    // compiles to:
    //   `new _CSS({kind: 'properties',
    //              properties: [['color', 'red'], ['margin', 1]]},
    //             {kind: 'rule',
    //              selectors: [new _SimpleSel({kind: 'tag', name: 'div'})],
    //              body: new _CSS()},
    //             {kind: 'import', imported: y},
    //             {kind: 'properties', properties: [['background', 'pink']]})`
    let cssId = getModuleId('css/css', '_CSS', o);
    let entries = []; // Entries to pass to the constructor
    let cssProps = null; // Current JS array for an entry of kind `properties`
    let oExp = expressionOptions(o);
    for (let prop of node.properties) {
      // When the property is an array of selectors or a spread
      if (prop.selector || prop.spread) {
        // Add `properties` entry before the incoming `rule` or `import`
        if (cssProps != null) {
          entries.push(jsToAST({kind: 'properties', properties: cssProps}, oExp,
                               c));
          cssProps = null;
        }

        if (prop.spread) { // Import another CSS, add an `import` entry
          entries.push(jsToAST({kind: 'import', imported: prop.expression},
                               oExp, c));
        } else { // Array of selectors, add a `rule` entry
          entries.push(jsToAST({kind: 'rule', selectors: prop.key,
                                body: prop.expression}, oExp, c));
        }
      } else { // Normal property (append to the object for `addProperties`)
        if (cssProps == null) { // Create new array if first property
          cssProps = [];
        }
        // Keys can be names, strings, string templates, or arbitrary
        // expressions (computed keys); we compile names to strings
        let key = prop.key instanceof ast.Name ? prop.key.name : prop.key;
        // Push `[key, value]`
        cssProps.push([key, prop.expression]);
      }
    }
    // Push final `properties` entry
    if (cssProps != null) {
      entries.push(jsToAST({kind: 'properties', properties: cssProps}, oExp,
                           c));
    }

    // Return `new _CSS(entries)`
    return statementize(o, js.newExpression(cssId, entries));
  },
  // Mutable (e.g. `mut x`)
  Mutable(node, o) {
    // Get the id used to access the Mutable module
    let mutableId = getModuleId('reactive/mutable', '_Mut', o);
    return statementize(o,
      js.newExpression(mutableId, [c(node.expression, expressionOptions(o))]));
  },
  // Signal (e.g. `sig *x + *y`)
  Signal(node, o) {
    // Get the id used to access the Signal module
    let signalId = getModuleId('reactive/signal', '_Sig', o);
    // The signal expression will be compiled to a function; such function
    // receives the signal itself as an argument, it is used to access the value
    // of other signals or mutables while providing itself as a dependency
    let paramId = js.identifier('_sig');
    o.auxiliaryIds.push(paramId);
    // Options for the signal expression (must return from the function and any
    // dereferencing must create a dependency with the signal)
    let oSig = statementOptions(o, true, null, false, {signal: paramId});
    // e.g. `sig *x` compiles to `new _Sig(_sig => {return x.value(_sig);})`
    return statementize(o,
      js.newExpression(signalId, [js.arrowFunctionExpression([paramId],
        c(blockify(node.expression), oSig), false)]));
  },
  // String value (e.g. `"x"`)
  String(node, o) {
    return statementize(o, js.stringLiteral(node.value));
  },
  // Strings with interpolation (e.g. `"x ${y}"`)
  StringTemplate(node, o) {
    let oExp = expressionOptions(o);
    return statementize(o,
      js.templateLiteral(
        node.stringParts.map((part, i) => js.templateElement(part,
          i === node.stringParts.length - 1)),
        node.expressions.map((exp) => c(exp, oExp))));
  },
  // Number value (e.g. `0`)
  Number(node, o) {
    return statementize(o, js.numericLiteral(node.value));
  },
  // Boolean value (`true` or `false`)
  Boolean(node, o) {
    return statementize(o, js.booleanLiteral(node.value));
  },
  // Identifier (e.g. `x`)
  Identifier(node, o) {
    return statementize(o, c(node.name, o));
  },
  // This expression (`this`)
  This(node, o) {
    return statementize(o, js.thisExpression());
  },
  // Null value (`null`)
  Null(node, o) {
    return statementize(o, js.nullLiteral());
  },
  // Undefined value (`undefined`)
  Undefined(node, o) {
    return statementize(o, VOID_0);
  },

  // CSS Selectors =============================================================
  // Simple selector (e.g. `div#i.c:hover`)
  SimpleSelector(node, o) {
    // Example compilation: `div#i.c:if(x):nth-child("odd")::after` compiles to:
    // `new _SimpSel({kind: 'tag', name: 'div'}, {kind: 'id', name: 'i'},
    //    {kind: 'class', name: 'c'}, {kind: 'if', expression: x},
    //    {kind: 'pseudoClass', name: 'nth-child', argument: 'odd'},
    //    {kind: 'pseudoElement', name: 'after'})`
    let oExp = expressionOptions(o);
    let selId = getModuleId('css/selectors/simple-selector', '_SimpSel', o);
    let entries = [];
    for (let qualifier of node.qualifiers) {
      switch (qualifier.kind) {
        // Examples: `div`, `#id`, `.class`
        case 'tag': case 'id': case 'class':
          entries.push(jsToAST({kind: qualifier.kind,
                                name: qualifier.name.name}));
          break;
        // `*` and `&` selectors
        case 'universal': case 'parent':
          entries.push(jsToAST({kind: qualifier.kind}));
          break;
        // Example: `[rel]`, `[href^="https://"]`
        case 'attribute':
          entries.push(jsToAST({kind: 'attribute',
            attribute: qualifier.attribute.name, operator: qualifier.operator,
            value: qualifier.expression}, oExp, c));
          break;
        // Example: `:if(x)`, `:if(sig *x + *y > 3)`
        case 'if':
          entries.push(jsToAST({kind: 'if',
                                value: qualifier.expression}, oExp, c));
          break;
        // Example: `:not(div)`, `:not(:hover)`
        case 'not':
          entries.push(jsToAST({kind: 'not', selector: qualifier.selector},
                               oExp, c));
          break;
        // Example: `:hover`, `:nth-of-type("2n+1")`, `::before`
        case 'pseudoClass': case 'pseudoElement':
          entries.push(jsToAST({kind: qualifier.kind, name: qualifier.name.name,
                                argument: qualifier.argument}, oExp, c));
          break;
      }
    }
    return js.newExpression(selId, entries);
  },
  // Composite selector (e.g. `div > span`)
  CompositeSelector(node, o) {
    // Example compilation: `a > i` compiles to:
    // `new _CompSel('>', new _SimpSel('a'), new _SimpSel('i'))`
    let selId = getModuleId('css/selectors/composite-selector', '_CompSel', o);
    return js.newExpression(selId, [js.stringLiteral(node.combinator),
                                    c(node.left, o), c(node.right, o)]);
  },

  // Patterns ==================================================================
  // Note that patterns return a pair of type (`JSNode`, `[string]`), i.e. each
  // rule outputs its respective JS AST representation, as well as the list of
  // names bound to the pattern (e.g. the pattern `[x, {a: y}]` would return
  // `[x, y]` in the second position of the pair)

  // Identifier pattern (e.g. `x`)
  IdentifierPattern(node, o) {
    let name = c(node.name, o);
    return [name, [name.name]];
  },
  // Dereference pattern (e.g. `*x`)
  DereferencePattern(node, o) {
    return [js.memberExpression(c(node.expression, expressionOptions(o)),
                                js.identifier('value'), false), []];
  },
  // Member pattern (e.g. `x.y`)
  MemberPattern(node, o) {
    let oExp = expressionOptions(o);
    return [js.memberExpression(c(node.object, oExp), c(node.property, oExp),
                                node.computed), []];
  },
  // Array pattern (e.g. `[x, y = 0, ...z]`)
  ArrayPattern(node, o) {
    let boundNames = []; // Names bound to the pattern
    let oExp = expressionOptions(o);
    let arrayPattern = js.arrayPattern(node.elements.map((element) => {
      if (!element.pattern) { // Elision
        return null;
      }
      let [pattern, names] = c(element.pattern, o);
      for (let name of names) { // Add bound names
        boundNames.push(name);
      }
      return element.rest ? js.restElement(pattern) :
        (element.initializer == null ? pattern :
          js.assignmentPattern(pattern, c(element.initializer, oExp)));
    }));
    return [arrayPattern, boundNames];
  },
  // Object pattern (e.g. `{x, y = 0, a: z = 0, ...w}`)
  ObjectPattern(node, o) {
    let boundNames = []; // Names bound to pattern
    let oExp = expressionOptions(o);
    let objectPattern = js.objectPattern(node.properties.map((property) => {
      let [pattern, names] = c(property.pattern, o);
      for (let name of names) { // Add bound names
        boundNames.push(name);
      }
      if (property.rest) { // A rest property
        return js.restProperty(pattern);
      }
      // Keys can be names, strings, string templates, numbers, or arbitrary
      // expressions (computed keys). We compile string template keys to
      // computed keys
      let computed = property.computed ||
                     property.key instanceof ast.StringTemplate;
      let value = property.initializer == null ? pattern :
        js.assignmentPattern(pattern, c(property.initializer, oExp));
      return js.objectProperty(c(property.key, oExp), value, computed, false,
                               []);
    }));
    return [objectPattern, boundNames];
  }
};

/**
 * Function responsible for the compilation of a Loom AST node. Compiles a given
 * node into its equivalent JavaScript AST representation.
 * @param {ASTNode} node Input Loom AST node to compile
 * @param {Object} o Compilation options
 * @returns {JSNode} Compiled JS AST node
 */
function c(node, o) {
  // Call the first available rule (e.g. if `node` is an instance of `ast.Null`
  // then, `node.types` will contain `['Null', 'Literal', 'ASTNode']` and the
  // called rule is the first one that is defined in `RULES`)
  for (let type of node.types) {
    if (RULES[type] !== undefined) {
      return RULES[type](node, o);
    }
  }
  // Throw error if no rule is available for node
  throw new Error('compile: No compilation rule for node ' +
                  node.constructor.name);
}

/**
 * Function that provides the options necessary to begin compilation.
 * @param {ASTNode} node Input Loom AST to compile
 * @param {Object} options Options for the compilation:
 *   - {Map} auxiliaryModules: helper modules already required
 *   - {Set} declaredNames: set of names already declared that we shouldn't use
 *       as auxiliary identifiers
 *   - {boolean} produceValue: whether to force the last statement of the
 *       program to produce a value (important for the REPL, for example)
 * @returns {Object} Object with the following properties:
 *   - {Map} auxiliaryModules: helper modules required by the compiled program
 *       (the map is the same as the one provided, if one was provided)
 *   - {Set} auxiliaryIds: set of names declared during the compilation of the
 *       program (the set is the same as the one provided, if one was provided)
 *   - {JSNode} jsAst: compiled JavaScript AST
 */
function compile(node, options = {}) {
  // Throw error if the node is not a Loom program
  if (!(node instanceof ast.Program)) {
    throw new Error('compile: Invalid argument, not a program AST node');
  }

  // Options passed to the `compile` function
  let compilationOptions = {
    // Path to Loom's modules
    modulesPath: options.modulesPath,
    // Set of all names declared during the compilation of a program
    declaredNames: options.declaredNames || new Set(),
    // Helper modules required by the compiled program, a map from `string`
    // (name/location of the module) to one of:
    // - `JSIdentifier`: id used when accessing the module via a default
    //   specifier (e.g. the `_x` in `import _x from 'm'`)
    // - `Object`: object mapping specifiers to their respective id, (e.g. if
    //   the object is `{Signal: _Sig, Mutable: _Mut}` then the import would be
    //   `import {Signal as _Sig, Mutable as _Mut} from 'm'`)
    auxiliaryModules: options.auxiliaryModules || new Map(),
    // Modules in `auxiliaryModules` that need to be imported
    modulesToImport: [],
    // List of all auxiliary ids generated during the compilation of a program;
    // each id has its suggested name set, such name may change at the end of
    // the compilation if it conflicts with a name in `declaredNames` (our
    // algorithm is "dumb", in the sense that it may change the name of some
    // auxiliary id that doesn't really conflict with any declared id because
    // they were defined in completely separate scopes)
    auxiliaryIds: [],
    // List of auxiliary ids that have not yet been declared; when some
    // compilation rule requires some auxiliary id, it should create it, append
    // it to this list and use it (it will later be declared at the top of the
    // program or of a block), each element of the list should be a pair of type
    // (`JSIdentifier`, `JSNode?`), i.e. the identifier and its initial value
    idsToDeclare: [],
    // Whether the current node is to be compiled as an expression
    expression: false,
    // Whether the expression being compiled must be returned
    mustReturn: false,
    // Identifier where the expression being compiled must be pushed to, `null`
    // means that the expression should not be pushed; this is used to compile
    // loops as expressions
    pushTo: null,
    // Whether the expression being compiled must produce a value (important for
    // the REPL)
    mustEndInValue: options.produceValue || false,
    // Whether the current node being compiled is inside a signal; this affects
    // the behaviour of dereferencing (if inside a signal, there must be created
    // a dependency with the signal)
    signal: null
  };

  // Compile program and return useful information to outside
  let compiled = c(node, compilationOptions);
  return {jsAst: compiled,
          auxiliaryModules: compilationOptions.auxiliaryModules};
}

// Exported function
module.exports = compile;

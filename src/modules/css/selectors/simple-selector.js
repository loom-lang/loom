// Dependencies
let Selector = require('./selector');
let {hyphenate} = require('../utils');

/**
 * Class that represents a simple selector used inside a CSS value.
 * Example: `a#b` would be a simple selector with tag `'a'` and id `'b'`.
 */
class SimpleSelector extends Selector {
  /**
   * Constructor of a simple selector.
   * @param {...Object} qualifiers Qualifiers of the selecot; each qualifier is
   *   an object with a `kind` property (one of: `'tag'`; `'universal'`,
   *   `'parent'`; `'id'`; `'class'`; `'attribute'`; `'if'`; `'not'`;
   *   `'pseudoClass'`; `'pseudoElement'`) and specific properties depending on
   *   the kind
   */
  constructor(...qualifiers) {
    super();
    this._qualifiers = qualifiers;
  }

  /**
   * Whether this selector references its parent.
   * @returns {boolean} Whether selector references its parent
   */
  referencesParent() {
    return this._qualifiers[0].kind === 'parent';
  }

  /**
   * String representation of the selector (as expected in CSS).
   * @param {Array<any>} ifValues Values appearing inside `:if()` qualifiers
   * @param {string} parent String representation of the parent element
   * @returns {string} String representation of the selector
   */
  toString(ifValues, parent) {
    let str = '';
    for (let qualifier of this._qualifiers) {
      switch (qualifier.kind) {
        // A qualifier of kind `'universal'` or `'parent'` has no properties
        case 'universal': str += '*';                  break;
        case 'parent':    str += parent;               break;
        // A qualifier of kind `'tag'`, `'id'`, or `'class'` has the following
        // property:
        // - {string} name: Name of the tag, id, or class
        case 'tag':       str += qualifier.name;       break;
        case 'id':        str += `#${qualifier.name}`; break;
        case 'class':     str += `.${qualifier.name}`; break;
        // A qualifier of kind `'attribute'` has the following properties:
        // - {string} attribute: Name of the attribute
        // - {string?} operator: Operator of the attribute selector
        // - {any?} value: Value being matched in the attribute selector
        case 'attribute': {
          // `JSON.stringify` forces the string to be wrapped in quotes
          str += qualifier.operator == null ? `[${qualifier.attribute}]` :
            `[${qualifier.attribute}${qualifier.operator}${
              JSON.stringify(String(qualifier.value))}]`;
          break;
        }
        // A qualifier of kind `'if'` has the following property:
        // - {any} value: Value of the `if` qualifier; possibly a reactive value
        case 'if':
          ifValues.push(qualifier.value);
          // `if` values are dealt with outside
          str === '' && (str = '*');
          break;
        // A qualifier of kind `'not'` has the following property:
        // - {SimpleSelector} selector: Selector in the `:not()`
        case 'not':
          str += `:not(${qualifier.selector.toString(parent, ifValues)})`;
          break;
        // A qualifier of kind `'pseudoClass'` or `'pseudoElement'` has the
        // following properties:
        // - {string} name: Name of the pseudo-class/element
        // - {any} argument: Argument of the pseudo-class/element
        case 'pseudoClass': {
          let name = hyphenate(qualifier.name);
          str += qualifier.argument == null ? `:${name}` :
            `:${name}(${qualifier.argument})`;
          break;
        }
        case 'pseudoElement': {
          let name = hyphenate(qualifier.name);
          str += qualifier.argument == null ? `::${name}` :
            `::${name}(${qualifier.argument})`;
          break;
        }
      }
    }
    return str;
  }
}

// Exported class
module.exports = SimpleSelector;

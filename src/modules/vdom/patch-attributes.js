/**
 * List of modules to use when patching element attributes.
 */
const MODULES = [
  require('./modules/attributes'),
  require('./modules/events'),
  require('./modules/css'),
  require('./modules/hooks')
];

/**
 * Calls a given hook in all modules when such hook is defined.
 * @param {string} hook Hook to call
 * @param {Array<any>} args Arguments to pass to the hook
 * @returns {Array<any>} Array with the result of calling the hook in each
 *   module
 */
function callModulesHook(hook, args) {
  let results = []; // Store the results of each call
  for (let module of MODULES) {
    if (module[hook] != null) {
      results.push(module[hook].apply(null, args));
    }
  }
  return results;
}

/**
 * Function to run when `elem` was just created and has certain attributes to
 * set.
 * @param {Element} elem Created DOM element
 * @param {Object} attributes Attributes that should become associated with the
 *   DOM element
 * @param {Object} api API to interact with the DOM element
 * @returns {void}
 */
function onCreate(elem, attributes, api) {
  callModulesHook('create', [elem, attributes, api]);
}

/**
 * Function to run when `elem` was inserted into the DOM and some of the
 * attributes in `attributes` should only be applied after that happens.
 * @param {Element} elem Inserted DOM element
 * @param {Object} attributes Attributes associated with the DOM element
 * @param {Object} api API to interact with the DOM element
 * @returns {void}
 */
function onInsert(elem, attributes, api) {
  callModulesHook('insert', [elem, attributes, api]);
}

/**
 * Function to run when `elem` was updated; it used to have attributes
 * `oldAttributes` and should now have `attributes`.
 * @param {Element} elem Updated DOM element
 * @param {Object} oldAttributes Attributes currently associated with the DOM
 *   element
 * @param {Object} attributes Attributes that should become associated with the
 *   DOM element
 * @param {Object} api API to interact with the DOM element
 * @returns {void}
 */
function onUpdate(elem, oldAttributes, attributes, api) {
  callModulesHook('update', [elem, oldAttributes, attributes, api]);
}

/**
 * Function to run when `elem` is to be removed from the DOM (either directly or
 * indirectly because one of its ancestors was removed).
 * @param {Element} elem Destroyed DOM element
 * @param {Object} attributes Attributes associated with the DOM element
 * @param {Object} api API to interact with the DOM element
 * @returns {void}
 */
function onDestroy(elem, attributes, api) {
  callModulesHook('destroy', [elem, attributes, api]);
}

/**
 * Function to run when `elem` is to be directly removed from the DOM. This
 * function might return a promise, in which case the DOM element may only be
 * effectively removed once the promise resolves.
 * @param {Element} elem Removed DOM element
 * @param {Object} attributes Attributes associated with the DOM element
 * @param {Object} api API to interact with the DOM element
 * @returns {Promise?} Promise that resolves once the DOM element may be removed
 */
function onRemove(elem, attributes, api) {
  let promises = callModulesHook('remove', [elem, attributes, api])
                 .filter((result) => result instanceof Promise);
  if (promises.length > 0) {
    return Promise.all(promises);
  }
}

// Exported functions
module.exports = {onCreate, onInsert, onUpdate, onDestroy, onRemove};

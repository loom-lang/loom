/**
 * Module that handles the REPL.
 */

// Dependencies
require('./inspects');
let path = require('path');
let repl = require('repl');
let vm = require('vm');
let {extend, colorsEnabled} = require('../utils');
let {parse, compile, register} = require('../main');

/**
 * Styled and unstyled prompt.
 */
const STYLED_PROMPT = '\x1b[4mloom\x1b[24m> '; // Underlined "loom"
const UNSTYLED_PROMPT = 'loom> ';

/**
 * Default name for the file storing the Loom REPL history.
 */
const REPL_HISTORY = '.loom_repl_history';

/**
 * Filename to use when reporting errors from the REPL.
 */
const REPL_FILENAME = '[repl]';

/**
 * Absolute path to Loom's modules.
 */
const MODULES_PATH = path.join(__dirname, '..', 'modules');

/**
 * Babel presets and plugins used when compiling the code.
 */
const BABEL_PRESETS = [[require('babel-preset-env'),
                        {targets: {node: 'current'}}]];

/**
 * Default options to pass to the Node REPL.
 */
const REPL_OPTIONS = {
  // Styled prompt if colours are enabled
  prompt: colorsEnabled() ? STYLED_PROMPT : UNSTYLED_PROMPT,
  historyFile: process.env.HOME && path.join(process.env.HOME, REPL_HISTORY),
  historyMaxInputSize: 10240, // 10KiB
  eval(input, context, filename, cb) {
    if (input.trim() === '') { // Do nothing on empty input
      return cb(null);
    }
    try {
      /**
       * FIXME: The compiled code generates auxiliary variables that shouldn't
       *   be accessible by the user. How to fix this taking into consideration
       *   that Babel also creates auxiliary variables that we don't even know
       *   about?
       */
      let compiled = compile(parse(input, {filename: REPL_FILENAME}),
        {modulesPath: MODULES_PATH, filename: REPL_FILENAME, produceValue: true,
         presets: BABEL_PRESETS});
      cb(null, vm.runInContext(compiled.js, context, REPL_FILENAME));
    } catch(e) {
      // Recoverable error, i.e. support for multi-line input
      if (e instanceof SyntaxError && e.filename === REPL_FILENAME &&
          /unexpected end of input/.test(e.message)) {
        return cb(new repl.Recoverable(e));
      }
      cb(e);
    }
  }
};

/**
 * Starts the REPL.
 * @param {Object} options Node REPL options used to override the default
 *   options
 * @returns {void}
 */
function start(options) {
  options = options ? extend(REPL_OPTIONS, options) : REPL_OPTIONS;
  register(); // Register Loom to be able to import `.loom` files
  return repl.start(options);
}

// Exported function
module.exports = start;

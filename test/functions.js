/**
 * Testing of functions.
 */

let {eq, ok, run} = require('./utils');

suite('Functions');

// `function` syntax
test('function syntax', function() {
  let funs = ['function()0', 'function ( ) 0', 'function f()0',
    'function f ( ) 0', 'function(x)0', 'function ( x ) 0', 'function(x,y)0',
    'function ( x , y ) 0', 'function(x,)0', 'function ( x , ) 0',
    'function(){0}', 'function ( ) { 0 }',
    `function(a // Comment in automatic comma insertion
              b
              c) 0`];
  for (let fun of funs) {
    eq(run(`(${fun})()`), 0);
  }
});

// Arrow functions syntax
test('arrow syntax', function() {
  let funs = ['()->0', '( ) -> 0', 'x->0', 'x -> 0', '(x,y)->0',
              '( x , y ) -> 0', '(x,)->0', '( x , ) -> 0', '()->{0}',
              '()=>0', '( ) => 0', 'x=>0', 'x => 0', '(x,y)=>0',
              '( x , y ) => 0', '(x,)=>0', '( x , ) => 0', '()=>{0}'];
  for (let fun of funs) {
    eq(run(`(${fun})()`), 0);
  }
});

// Returning objects from a function
test('return object', function() {
  ok(run('function() {{x: 0}}().x == (function() ({x: 0}))().x'));
});

// Parameter destructuring
test('destructuring', function() {
  eq(run('function([a, b], [c]) {a + b + c}([1, 2], [3])'), 6);
  eq(run('function({x: a, y: b}, {c}) {a + b + c}({x: 1, y: 2}, {c: 3})'), 6);
  eq(run('function([,, x]) {x}([1, 2, 3])'), 3);
  eq(run('function({"x": a, ["y"]: b}) {a + b}({x: 1, y: 2})'), 3);
  eq(run(`
    var f = ([{a, b: [b, {c}]}, [d]]) -> a + b + c + d
    f([{a: 1, b: [2, {c: 3}]}, [4]])
  `), 10);
  eq(run('function([a, ...b]) {a + b[0] + b[1]}([1, 2, 3])'), 6);
  eq(run('function({x: a, ...b}) {a + b.y + b.z}({x: 1, y: 2, z: 3})'), 6);
  eq(run('function([a = 1, [b = 2, c = 3]]) {a + b + c}([, [3]])'), 7);
  eq(run('function({a = 1, x: b = 2, "c${0}": c = 3}) {a + b + c}({x: 3})'), 7);
});

// Rest of parameters
test('rest', function() {
  eq(run('function(a, ...b) {a + b[0] + b[1]}(1, 2, 3)'), 6);
});

// Default parameters
test('defaults', function() {
  eq(run('function(a = 1, [b] = [2], {c} = {c: 3}) {a + b + c}()'), 6);
});

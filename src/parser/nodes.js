/**
 * This module exposes a set of classes that represent the Loom AST nodes.
 * "Abstract" nodes are also exported that group "actual" nodes in a way that
 * makes the compilation of the language simpler (explained below).
 */

/**
 * Constant used to help create a class for each of Loom's AST nodes. It
 * supports the creation of "abstract" nodes (e.g. both "ArithmeticOperation"
 * and "LogicalOperation" are "BinaryOperation"s), these "abstract" nodes are
 * helpful for the type-checking and compilation.
 * Each node has a type and an array: when the node is "abstract", the array
 * will contain other nodes, when it is not, it contains strings representing
 * the node's properties.
 *
 * The comments next to the nodes' properties represent the expected type for
 * each property. Note that an expression may appear wherever a statement is
 * expected (an expression may be seen as a "special" statement, in JavaScript
 * it would be an "expression statement") - however, we keep them separated for
 * simplicity.
 */
const NODES = [
  // Special node used to add location information to "names" in the language
  ['Name',                     ['name']], /* `string` */

  // Root node =================================================================
  ['Program',                  ['body']], /* `[Statement]` */

  // Statements ================================================================
  ['Statement', [
    ['Import',                 ['specifiers', 'source']],
                               /* `[{local: Name, imported: Name?,
                                     default: boolean, namespace: boolean}],
                                   String` */
    ['Export',                 ['declaration', 'specifiers', 'source',
                                'default', 'all']],
                               /* `Statement?, [{local: Name, exported: Name}],
                                   String?, boolean, boolean` */
    ['Declaration', [
      ['VariableDeclaration', [
        ['VarDeclaration',     ['declarators']],
        ['ConstDeclaration',   ['declarators']]
                               /* `[{pattern: Pattern,
                                     initializer: Expression?}]` */
      ]],
      ['FunctionDeclaration',  ['name', 'parameters', 'body']]
                               /* `Name, [Pattern], Expression` */
    ]],
    ['Return',                 ['expression']],
    ['Break',                  ['label']],
    ['Continue',               ['label']]
                               /* `StringIdentifier` */
  ]],

  // Expressions ===============================================================
  ['Expression', [
    ['Block',                  ['body']], /* `[Statement]` */
    ['If',                     ['test', 'consequent', 'alternate']],
                               /* `Expression, Statement, Statement?` */
    ['While',                  ['test', 'body']],
                               /* `Expression, Statement` */
    ['DoWhile',                ['body', 'test']],
                               /* `Statement, Expression` */
    ['ForIn',                  ['assignees', 'collection', 'body']],
    ['ForOf',                  ['assignees', 'object', 'body']],
                               /* `Statement, Expression, Statement` */
    ['Try',                    ['body', 'handler', 'finalizer']],
                               /* `Statement, {param: Pattern?,
                                               body: Statement}?, Statement?` */
    ['Throw',                  ['expression']],
                               /* `Expression` */
    ['Sequence',               ['expressions']], /* `[Expression]` */
    ['Assignment', [
      ['SimpleAssignment',     ['assignee', 'expression']],
                               /* `Pattern, Expression` */
      ['LogicalAssignment',    ['operator', 'assignee', 'expression']],
      ['BitAssignment',        ['operator', 'assignee', 'expression']],
      ['ArithmeticAssignment', ['operator', 'assignee', 'expression']]
                               /* `string, Pattern, Expression` */
    ]],
    ['BinaryOperation', [
      ['LogicalOperation',     ['operator', 'left', 'right']],
      ['BitOperation',         ['operator', 'left', 'right']],
      ['Comparison',           ['operator', 'left', 'right']],
      ['ArithmeticOperation',  ['operator', 'left', 'right']],
                               /* `string, Expression, Expression` */
      ['InstanceOf',           ['left', 'right']],
      ['In',                   ['left', 'right']],
      ['Of',                   ['left', 'right']]
                               /* `Expression, Expression` */
    ]],
    ['UnaryOperation', [
      ['UnaryArithmetic',      ['operator', 'expression']],
                               /* `string, Expression` */
      ['Not',                  ['expression']],
      ['BitNot',               ['expression']],
      ['Delete',               ['expression']],
      ['TypeOf',               ['expression']]
                               /* `Expression` */
    ]],
    ['Update',                 ['operator', 'target', 'prefix']],
                               /* `string, Pattern, boolean` */
    ['Dereference',            ['expression']], /* `Expression` */
    ['Call',                   ['callee', 'arguments']],
    ['New',                    ['callee', 'arguments']],
                               /* `Expression, [{expression: Expression,
                                                 spread: boolean}]` */
    ['Member',                 ['object', 'property', 'computed']],
                               /* `Expression, Name | Expression, boolean` */
    ['FunctionExpression',     ['name', 'parameters', 'body', 'bound']],
                               /* `Name?, [{pattern: Pattern,
                                     initializer: Expression?, rest: boolean}],
                                   Expression, boolean` */
    ['Array',                  ['elements']],
                               /* `[{expression: Expression?,
                                     spread: boolean}]` */
    ['Object',                 ['properties']],
                               /* `[{key: (Name | Expression)?,
                                     expression: Expression, computed: boolean,
                                     spread: boolean}]` */
    ['RegularExpression',      ['pattern', 'flags']], /* `string, string` */
    ['HTML',                   ['tag', 'qualifiers', 'attributes', 'children']],
                               /* `Name, [{kind: 'id' | 'class', name: Name}],
                                   Expression?, Expression?` */
    ['CSS',                    ['properties']],
                               /* `[{key: (Name | Expression | [Selector])?,
                                     expression: Expression, selector: boolean,
                                     computed: boolean, spread: boolean}]` */
    ['Mutable',                ['expression']],
    ['Signal',                 ['expression']],
                               /* `Expression` */
    ['This',                   []],
    ['StringTemplate',         ['stringParts', 'expressions']],
                               /* `[{cooked: string, raw: string}],
                                   [Expression]` */
    ['Identifier',             ['name']], /* `Name` */
    ['Literal', [
      ['String',               ['value']], /* `string` */
      ['Number',               ['value']], /* `number` */
      ['Boolean',              ['value']], /* `boolean` */
      ['Null',                 []],
      ['Undefined',            []]
    ]]
  ]],

  // CSS selectors =============================================================
  ['Selector', [
    ['SimpleSelector',         ['qualifiers']],
                               /* `[{kind: 'id' | 'class' | 'tag',
                                     name: Name} |
                                    {kind: 'universal' | 'parent'} |
                                    {kind: 'attribute', attribute: Name,
                                     operator: string?,
                                     expression: Expression?} |
                                    {kind: 'if', expression: Expression} |
                                    {kind: 'not', selector: SimpleSelector} |
                                    {kind: 'pseudoClass' | 'pseudoElement',
                                     name: Name, argument: Expression?}]` */
    ['CompositeSelector',      ['combinator', 'left', 'right']]
                               /* `' ' | '>' | '+' | '~', SimpleSelector,
                                   Selector` */
  ]],

  // Patterns ==================================================================
  ['Pattern', [
    ['IdentifierPattern',      ['name']], /* `Name` */
    ['MemberPattern',          ['object', 'property', 'computed']],
                               /* `Expression, Name | Expression, boolean` */
    ['DereferencePattern',     ['expression']], /* `Expression` */
    ['ArrayPattern',           ['elements']],
                               /* `[{pattern: Pattern?,
                                     initializer: Expression?,
                                     rest: boolean}]` */
    ['ObjectPattern',          ['properties']]
                               /* `[{key: (Name | Expression)?,
                                     pattern: Pattern, computed: boolean,
                                     initializer: Expression?,
                                     rest: boolean}]` */
  ]]
];

/**
 * Class that represents a generic Loom AST node. Every other node (abstract or
 * not) extends this class.
 */
class ASTNode {
  /**
   * Constructor of the generic Loom AST node (sets the location information).
   * @param {Object} location Information about the node's source code location
   *   (compatible with the Babylon's `SourceLocation` interface)
   */
  constructor(location) {
    this.location = location;
  }

  /**
   * Converts the node into a simple JavaScript object.
   * @returns {Object} Object representation of the node
   */
  toObject() {
    /**
     * Recursive function to turn the node into a JavaScript object.
     * @param {any} value Value to transform
     * @returns {any} Transformed value
     */
    function valueToObject(value) {
      if (Array.isArray(value)) { // Transform each element of the array
        return value.map(valueToObject);
      } else if (value !== null && typeof value === 'object') {
        // Create a new object with every property of the one being transformed,
        // this includes `ASTNode`s
        let obj = {};
        if (value instanceof ASTNode) { // Add type when the value is a node
          obj.type = value.constructor.name;
        }
        for (let prop in value) { // Add properties that aren't in the prototype
          if (value.hasOwnProperty(prop)) {
            obj[prop] = valueToObject(value[prop]);
          }
        }
        return obj;
      }
      return value; // Simple value (e.g. `null`, `string`, `number`, etc.)
    }
    return valueToObject(this);
  }
}

/**
 * Function that exports a node class as a subclass of the class `superNode`.
 * The class can then be accessed outside of the module via a property with the
 * type of the node (e.g. `new ast.Number(42)`, where `ast` is the name of the
 * variable that represents this module). When the node to export is "abstract",
 * the function recursively exports its child nodes (with the current "abstract"
 * node as their super class, i.e. `ast.Number instanceof ast.Literal` will
 * hold). When the node to export is not "abstract", a class is created (and
 * exported) with a constructor that allows the definition of the node's
 * properties.
 * @param {Array} node "Node array" from which to create and export a class, of
 *   type `nodeArray = [string, [nodeArray] | [string]]`
 * @param {Class} superNode Class to become the super class of the exported node
 * @param {[string]} superTypes Array with the types of all the super nodes in
 *  the hierarchy (e.g. when exporting the `Null` node, `superTypes` will
 *  contain `['Literal', 'ASTNode']`)
 * @returns {void}
 */
function exportASTNode(node, superNode, superTypes) {
  let [type, array] = node;
  let Node;

  // Case when node is not "abstract"
  if (array.length === 0 || typeof array[0] === 'string') {
    // Create a new class that extends `superNode`
    Node = class extends superNode {
      /**
       * Constructor of an AST node, sets the node's source and its properties.
       * @param {Object} location Information about the node's source code
       *   location (compatible with the ESTree's `SourceLocation` interface)
       * @param {any} properties Properties of the node
       */
      constructor(location, ...properties) {
        super(location);
        for (let i = 0, l = array.length; i < l; ++i) {
          this[array[i]] = properties[i];
        }
      }
    };
    // Set property with all of the node's types
    Node.prototype.types = [type, ...superTypes];
  } else { // Case when node is "abstract"
    // Create a new "abstract" class that extends `superNode`
    Node = class extends superNode {};
    let types = [type, ...superTypes]; // Inherited types of a child node
    for (let child of array) { // Recursively export child nodes
      exportASTNode(child, Node, types);
    }
  }

  // Overwrite name property for pretty printing
  Object.defineProperty(Node, 'name', {value: type});
  // Export node
  exports[type] = Node;
}

// Export each and every node as a class
exports.ASTNode = ASTNode;
for (let node of NODES) {
  exportASTNode(node, ASTNode, ['ASTNode']);
}

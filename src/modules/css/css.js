// Dependencies
let {hyphenate} = require('./utils');

/**
 * Class that represents a CSS value.
 */
class CSS {
  /**
   * Constructor of a CSS value.
   * @param {...Object} entries Possible entries of a CSS value; each entry is
   *   an object with a `kind` property (one of: `'properties'`, `'rule'`, and
   *   `'import'`) and specific properties depending on the kind
   */
  constructor(...entries) {
    // Mapping of properties of the CSS value to their values, note that a `Map`
    // maintains insertion order, which is important in CSS; the added
    // properties are all hyphenised in order to avoid having hyphenised and
    // non-hyphenised properties representing the same property (e.g. in order
    // to avoid having both `backgroundColor` and `background-color`)
    this._properties = new Map();
    // Rules present in the CSS value: each rule is an object of type:
    // `{selector: Selector, body: any}`
    this._rules = [];

    // Each entry is an object containing its kind and respective properties
    for (let entry of entries) {
      switch (entry.kind) {
        // An entry of kind `'properties'` has the following property:
        // - {Array<[string, any]>} properties: list of key/value pairs (e.g.
        //     `[['color', 'red'], ['border', 0]]`)
        case 'properties': {
          for (let [property, value] of entry.properties) {
            this._properties.set(hyphenate(String(property)), value);
          }
          break;
        }
        // An entry of kind `'rule'` has the following properties:
        // - {Array<Selector>} selectors: list of selectors associated to the
        //     rule
        // - {any} body: body of the rule (arbitrary expression)
        case 'rule': {
          this._rules.push(entry);
          break;
        }
        // An entry of kind `'import'` has the following property:
        // - {any} imported: imported CSS value/arbitrary object
        case 'import': {
          let imported = entry.imported;
          if (imported instanceof CSS) { // Merge with the CSS value
            for (let [property, value] of imported.properties()) {
              this._properties.set(property, value);
            }
            this._rules.push(...imported.rules());
          } else if (imported != null) { // Allow importing an arbitrary object
            for (let property of Object.keys(imported)) {
              this._properties.set(hyphenate(property), imported[property]);
            }
          }
          break;
        }
      }
    }
  }

  /**
   * Map with properties of this CSS value.
   * @returns {Map<string, any>} Properties of the CSS value
   */
  properties() {
    return this._properties;
  }

  /**
   * Rules of this CSS value.
   * @returns {Array<Object>} Array of rules of the CSS value, each rule is an
   *   object of type `{selectors: Array<Selector>, body: any}`.
   */
  rules() {
    return this._rules;
  }
}

// Exported class
module.exports = CSS;

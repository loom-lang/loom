/**
 * Testing of reactive values.
 */

let {eq, run} = require('./utils');

suite('Reactives');

// Mutables syntax
test('mutable syntax', function() {
  eq(run('mut 1').value, 1);
  // No syntax sugar for block expressions (needs `do`)
  eq(run('mut {a: 1}').value.a, 1);
});

// Signals syntax
test('signal syntax', function() {
  eq(run('sig 1').value, 1);
  // Syntax sugar for block expressions (no need for `do`)
  eq(run('sig {var x = mut 1; *x + 1}').value, 2);
});

// Dereference syntax
test('dereference syntax', function() {
  eq(run('*mut 1'), 1);
  eq(run('* mut 1'), 1);
  eq(run('var x = mut 1; *x + * x'), 2);
});

// Test signals reacting to mutables changing
test('reaction', function() {
  eq(run('var x = mut 1, y = sig *x + 1; *y'), 2); // No changes
  eq(run('var x = mut 1, y = sig *x + 1; *x = 2; *y'), 3);
  eq(run(`var x = mut 1
          var y = sig *x + 1
          var z = sig *x + *y
          *x = 2
          *x + *y + *z`), 10);
});

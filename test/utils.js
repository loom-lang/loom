/**
 * Module that exports some utility functions to be used by the test suits.
 */

// Dependencies
let path = require('path');
let vm = require('vm');
let {parse, compile} = require('../src/main');
let {ok} = require('assert');
let {inspect} = require('../src/utils');

/**
 * Absolute path to Loom's modules.
 */
const MODULES_PATH = path.join(__dirname, '..', 'src', 'modules');

/**
 * Babel presets and plugins used when compiling the code.
 */
const BABEL_PRESETS = [[require('babel-preset-env'),
                        {targets: {node: 'current'}}]];

/**
 * A function that deeply compares two values using `Object.is`.
 * @param {any} x First value to compare
 * @param {any} y Second value to compare
 * @returns {boolean} Whether two values are indistinguishable
 */
function deepIs(x, y) {
  return Object.is(x, y) || Array.isArray(x) && Array.isArray(y) &&
         x.length === y.length && x.every((e, i) => deepIs(e, y[i]));
}

/**
 * Evaluate the input Loom code as an expression and return its result.
 * @param {string} input Input Loom code
 * @returns {any} Evaluation result
 */
function run(input) {
  // Compile the program as an expression
  let js = compile(parse(input), {modulesPath: MODULES_PATH, produceValue: true,
                                  presets: BABEL_PRESETS}).js;
  // Allow requiring modules by adding the `require` function to the context
  // TODO: Is this the proper way of doing this? CoffeeScript does something
  //       more complex
  return vm.runInNewContext(js, {require});
}

/**
 * Function used to perform syntax testing. It assumes that if no error is
 * thrown then all went well, otherwise it failed.
 * @param {Array<string>} pass List of programs that should pass
 * @param {Array<string>?} fail List of programs that should fail
 * @returns {void}
 */
function testSyntax(pass, fail) {
  // Tests that should pass
  for (let test of pass) {
    run(test);
  }
  // Tests that should fail
  if (fail != null) {
    for (let test of fail) {
      try {
        run(test);
      } catch(e) {
        // We expect them to throw: if they did then all went well
        continue;
      }
      throw new Error(`${test} should fail`);
    }
  }
}

// Export utility functions to be used on test suites
module.exports = {
  ok,
  run,
  eq: (x, y, msg) =>
    ok(Object.is(x, y), msg || `${inspect(x)} == ${inspect(y)}`),
  eqArray: (x, y, msg) =>
    ok(deepIs(x, y), msg || `${inspect(x)} == ${inspect(y)}`),
  neq: (x, y, msg) =>
    ok(!Object.is(x, y), msg || `${inspect(x)} != ${inspect(y)}`),
  testSyntax
};

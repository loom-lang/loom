/**
 * DOM node types according to the HTML spec.
 * See: `https://developer.mozilla.org/en-US/docs/Web/API/Node/nodeType`
 */
const ELEMENT_NODE = 1;
const TEXT_NODE = 3;

// Exported constants
module.exports = {ELEMENT_NODE, TEXT_NODE};

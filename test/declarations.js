/**
 * Testing of declarations.
 */

let {eq, run} = require('./utils');

suite('Declarations');

// Declaration syntax
test('syntax', function() {
  let decls = ['var x=0', 'var x = 0', 'var x=0,y=1', 'var x = 0 , y = 1',
               'const x=0', 'const x = 0', 'const x=0,y=1',
               'const x = 0 , y = 1'];
  for (let decl of decls) {
    run(`${decl}`);
  }
});

// Declaration destructuring
test('destructuring', function() {
  eq(run('var [a, b] = [1, 2], [c] = [3]; a + b + c'), 6);
  eq(run('var {x: a, y: b} = {x: 1, y: 2}, {c} = {c: 3}; a + b + c'), 6);
  eq(run('var [,, x] = [1, 2, 3]; x'), 3);
  eq(run('var {"x": a, ["y"]: b} = {x: 1, y: 2}; a + b'), 3);
  eq(run(`var [{a, b: [b, {c}]}, [d]] = [{a: 1, b: [2, {c: 3}]}, [4]]
          a + b + c + d`), 10);
  eq(run('var [a, ...b] = [1, 2, 3]; a + b[0] + b[1]'), 6);
  eq(run('var {x: a, ...b} = {x: 1, y: 2, z: 3}; a + b.y + b.z'), 6);
  eq(run('var [a = 1, [b = 2, c = 3]] = [, [3]]; a + b + c'), 7);
  eq(run('var {a = 1, x: b = 2, "c${0}": c = 3} = {x: 3}; a + b + c'), 7);
});

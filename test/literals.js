/**
 * Testing of literal values.
 */

let {eq, run} = require('./utils');

suite('Literals');

// Number syntax
test('number', function() {
  // Decimal numbers
  eq(run('1'), 1);
  eq(run('1e1'), 10);
  eq(run('1e+1'), 10);
  eq(run('1e-1'), 0.1);
  eq(run('1.0'), 1);
  eq(run('1.0e1'), 10);
  eq(run('1.0e+1'), 10);
  eq(run('1.0e-1'), 0.1);
  eq(run('.1'), 0.1);
  eq(run('.1e1'), 1);
  eq(run('.1e+1'), 1);
  eq(run('.1e-1'), 0.01);
  // Hexadecimal numbers
  eq(run('0xFf'), 255);
  eq(run('0XFf'), 255);
  // Octal numbers
  eq(run('0o77'), 63);
  eq(run('0O77'), 63);
  // Binary numbers
  eq(run('0b11'), 3);
  eq(run('0B11'), 3);
});

// Boolean syntax
test('boolean', function() {
  eq(run('true'), true);
  eq(run('false'), false);
});

// String syntax
test('string', function() {
  // Normal strings with interpolation
  eq(run('"string"'), 'string');
  eq(run('"string ${"interpolation"}"'), 'string interpolation');
  eq(run('"string ${"interpolation ${2}"}"'), 'string interpolation 2');
  // Escaped dollar sign shouldn't trigger interpolation
  eq(run('"string $${"\\${1}"}"'), 'string $${1}');
  // Test special characters
  eq(run('"\\\\\\"\\b\\f\\n\\r\\t\\v\\0\\x00\\u2716\\c"'),
     '\\"\b\f\n\r\t\x0B\0\x00\u2716c');
  // Support multi-line with a `\` at the end (`\n` should not be output)
  eq(run('"a\\\nb"'), 'ab');
  // Support multi-line without a `\` at the end (`\n` should be output)
  eq(run('"a\nb"'), 'a\nb');
});

// Null and undefined syntax
test('null & undefined', function() {
  eq(run('null'), null);
  eq(run('undefined'), void 0);
});

// Regular expressions syntax
test('regular expression', function() {
  eq(run('/string/.exec("string").index'), 0);
  eq(run('/string/.exec("some string").index'), 5);
  eq(run('/string/i.exec("sTrInG").index'), 0);
  // TODO: More tests
});

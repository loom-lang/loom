// Dependencies
let Reactive = require('./reactive');

/**
 * Class used to represent a signal, i.e. a reactive value that may depend on
 * some other reactive values (mutables or signals), becoming invalidated
 * (requiring a recomputation of its expression) whenever one of its
 * subscriptions becomes invalid. Note that a signal is lazy, i.e. it's value is
 * only recomputed whenever accessed (via `signal.value`).
 */
class Signal extends Reactive {
  /**
   * Constructor of a signal.
   * @param {Function} exp Signal expression
   */
  constructor(exp) {
    super();
    // Signals this signal is subscribed to
    this._subscriptions = new Set();
    // Signal expression
    this._expression = exp;
    // Whether the signal is currently valid (its expression has been computed)
    this._invalid = true;
    // Whether the signal expression is currently being computed; subscribing is
    // only allowed while the expression is computing
    this._computing = false;
    // Signal's associated value
    this._value = undefined;
  }

  /**
   * Gets the signal's current value, recomputing it if it is currently invalid.
   */
  get value() {
    if (!this._invalid) {
      return this._value;
    }
    this._invalid = false;
    this._computing = true;
    this._value = this._expression(this);
    this._computing = false;
    return this._value;
  }

  /**
   * Setting the value of a signal throws an error.
   * @param {any} value Value attempting to be set
   */
  set value(value) {
    throw new Error('Signal: signals are immutable, cannot set value');
  }

  /**
   * Marks this signal as invalid, i.e. its expression needs to be recomputed
   * in order to access the signal's value.
   * @param {Array<Reactive>} invalids Array of reactive values with
   *  `onInvalidate` functions to run
   * @returns {void}
   */
  _invalidate(invalids) {
    if (this._invalid) { // Do nothing if already invalid
      return;
    }
    this._invalid = true;
    // Append self to run its `onInvalidate` function after
    if (this._onInvalidate.size > 0) {
      invalids.push(this);
    }
    // Remove subscriptions of previous expression
    for (let subscription of this._subscriptions) {
      subscription._removeSubscriber(this);
    }
    this._subscriptions.clear();
    // Invalidate all subscribers
    for (let subscriber of this._subscribers) {
      subscriber._invalidate(invalids);
    }
  }

  /**
   * Whether this signal's expression is currently being computed.
   * @returns {boolean} Whether signal expression is being computed
   */
  _isComputing() {
    return this._computing;
  }

  /**
   * Adds a subscription to the signal.
   * @param {Reactive} reactive Reactive value to add as a subscription
   * @returns {void}
   */
  _addSubscription(reactive) {
    this._subscriptions.add(reactive);
  }
}

// Exported class
module.exports = Signal;
